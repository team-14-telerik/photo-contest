# Photo Contest 
###A picture is worth a thousand words.

------------------

A team of aspiring **photographers** want an application that can allow them to easily manage online photo contests.

The application has two main parts:

**Organizational** – here, the application owners can organize photo contests.

**For photo junkies** – everyone is welcome to register and to participate in contests. Junkies with certain ranking can be invited to be jury.



### Database relations image

![database](https://res.cloudinary.com/photocontestapi/image/upload/v1651079299/Admin/Team14Database_zbwvj0.png)
### Swagger documentation
Link to Api Documentation
***http://localhost:8080/swagger-ui/*** </br>
Photo-contest is a platform powered by an amazing community that has uploaded hundreds of thousands
of their own photos to fuel creativity around the world. You can sign up for free.



### Instructions how to set up and run the project locally

- Clone the repository. </br>
- Create the Database with the existing SQL file (script.sql). </br>
- Fill the Database with the existing SQL file (photo_contest_data.sql). </br>
- ENJOY! </br>
