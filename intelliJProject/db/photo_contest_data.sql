-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.6.5-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for photo-contest
CREATE DATABASE IF NOT EXISTS `photo-contest` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `photo-contest`;

-- Dumping structure for table photo-contest.categories
CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.categories: ~5 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`id`, `name`) VALUES
	(1, 'Animals'),
	(2, 'Scenic '),
	(3, 'Nature'),
	(4, 'Food'),
	(5, 'Aviation');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping structure for table photo-contest.contests
CREATE TABLE IF NOT EXISTS `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `category_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `deadline_phase_1` datetime NOT NULL,
  `deadline_phase_2` datetime DEFAULT NULL,
  `photo_url` text NOT NULL,
  `finished` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contests_id_uindex` (`id`),
  KEY `contests_categories_id_fk` (`category_id`),
  KEY `contests_statuses_id_fk` (`status_id`),
  CONSTRAINT `contests_categories_id_fk` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`),
  CONSTRAINT `contests_statuses_id_fk` FOREIGN KEY (`status_id`) REFERENCES `statuses` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.contests: ~9 rows (approximately)
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` (`id`, `title`, `category_id`, `status_id`, `deadline_phase_1`, `deadline_phase_2`, `photo_url`, `finished`) VALUES
	(39, 'Funny Wild Life', 1, 1, '2022-04-25 00:00:00', '2022-04-25 14:00:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650795857/photocontestPhotos_albums/funny_wildlife/oh_boy.jpg', 1),
	(40, 'Is It Cake?', 4, 1, '2022-04-25 00:00:00', '2022-04-25 15:00:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650788916/photocontestPhotos_albums/is%20it%20cake/burger_cake_sa7ldm.png', 1),
	(41, 'Funny Dogs', 1, 1, '2022-05-01 22:00:00', '2022-05-05 22:00:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650789014/photocontestPhotos_albums/funny%20dogs/braydon-anderson-wOHH-NUTvVc-unsplash_drqfza.jpg', 0),
	(42, 'Winter Tale', 2, 1, '2022-05-01 22:00:00', '2022-05-05 22:00:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650788919/photocontestPhotos_albums/winter%20tale/winter-instagram-captions-1_xerz4z.jpg', 0),
	(44, 'Magnificent Volcanos', 3, 2, '2022-04-25 15:00:00', '2022-04-25 00:15:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650788919/photocontestPhotos_albums/volcanos/tanya-grypachevskaya-80x3QULJDN4-unsplash_ofbfwf.jpg', 1),
	(46, 'Commercial Airplanes', 5, 1, '2022-04-25 00:00:00', '2022-04-30 00:00:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650788914/photocontestPhotos_albums/airplanes/airplanes-noise-shape-memory-alloys-1600_fuj93d.jpg', 0),
	(47, 'Beautiful Bulgaria', 2, 1, '2022-05-01 00:00:00', '2022-05-02 00:00:00', 'https://res.cloudinary.com/photocontestapi/image/upload/v1650895030/photocontestPhotos_albums/Bulgaria/6e7b4226cca9d267e01721d050f95de8_dzy3rt.jpg', 0),
	(48, 'Beautiful Sunsets', 3, 1, '2022-04-24 18:00:00', '2022-05-02 00:00:00', 'http://res.cloudinary.com/photocontestapi/image/upload/v1650896975/PhotoContest/ContestPhotos/contestPhotoID:sean-oulashin-KMn4VEeEPR8-unsplash.jpg.jpg', 0),
	(49, 'Singapore Snaps', 2, 1, '2022-04-25 00:00:00', '2022-04-27 00:00:00', 'https://sumfinity.com/wp-content/uploads/2018/10/Helix-Bridge-Singapore-Asia-night.jpg', 0);
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;

-- Dumping structure for table photo-contest.contests_photos
CREATE TABLE IF NOT EXISTS `contests_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `contest_id` int(11) NOT NULL,
  `photo_url` text DEFAULT NULL,
  `title` varchar(100) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `story` varchar(2000) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contests_photos_id_uindex` (`id`),
  KEY `contests_photos_contests_id_fk` (`contest_id`),
  KEY `contests_photos_users_id_fk` (`user_id`),
  CONSTRAINT `contests_photos_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `contests_photos_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.contests_photos: ~64 rows (approximately)
/*!40000 ALTER TABLE `contests_photos` DISABLE KEYS */;
INSERT INTO `contests_photos` (`id`, `contest_id`, `photo_url`, `title`, `user_id`, `story`) VALUES
	(11, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650798993/PhotoContest/ContestPhotos/contestPhotoID:5f5a5f37e6ff30001d4e8163.webp.webp', 'Two Giraffes', 25, 'This picture was taken in the African savannah sporting Diego anfas, when a second giraffe crashed in the photo frame. "Look what long eyelash I\'ve got!"\r\n'),
	(12, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650799170/PhotoContest/ContestPhotos/contestPhotoID:I%20got%20you.png.png', 'I got you, buddy!', 28, 'I spent my days in my usual "gopher place" and yet again, these funny little animals haven\'t belied their true nature.'),
	(13, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650799369/PhotoContest/ContestPhotos/contestPhotoID:00001001_p.jpg.jpg', 'Tough negotiations', 29, 'I think I am having you for dinner, little bud!'),
	(14, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650799692/PhotoContest/ContestPhotos/contestPhotoID:no_fishing.png.png', 'It is a Mocking Bird', 21, 'I was hoping a Kingfisher would land on the "No Fishing" sign but I was over the moon when it landed for several seconds with a fish. It then flew off with it\'s catch. It appeared to be mocking the person who erected the sign!'),
	(15, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650800169/PhotoContest/ContestPhotos/contestPhotoID:o_sole_mio.png.png', 'O Sole Mio', 20, 'Who said squirrels were emotionless? O Sole Mio, which captures a praying ground squirrel.'),
	(16, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650800530/PhotoContest/ContestPhotos/contestPhotoID:dancing_like_patric_sauzey.png.png', 'Dance Moves', 34, 'This meerkat does Patrick Swayze\'s dance moves!'),
	(17, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650800654/PhotoContest/ContestPhotos/contestPhotoID:social_distance_please.png.png', 'Social Distance, Please!', 22, 'Yeah, yeah, show me your green certificate first!'),
	(18, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650800847/PhotoContest/ContestPhotos/contestPhotoID:wild_life_gossip.png.png', 'Wildlife Gossip', 18, 'These lions are true gossipmongers!'),
	(19, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650800949/PhotoContest/ContestPhotos/contestPhotoID:like_mother_like_daughter.png.png', 'Serious Rears', 17, 'Like mother, like daughter...'),
	(20, 39, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650801192/PhotoContest/ContestPhotos/contestPhotoID:00000362_p.jpg.jpg', 'Bear Photographer', 38, 'This curios polar bear is ready to take a scenic shot!'),
	(21, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650801757/PhotoContest/ContestPhotos/contestPhotoID:Cake-meme.jpg.jpg', 'Sweet Eggplant', 16, 'Is it an eggplant or is it a cake?'),
	(22, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650801959/PhotoContest/ContestPhotos/contestPhotoID:sirloin_stake.png.png', 'Tenderloin', 36, 'Is it tenderloin or is it cake?'),
	(23, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650802494/PhotoContest/ContestPhotos/contestPhotoID:onion_cake.jpg.jpg', 'Peel Me a Cake', 35, 'Is it an union or might it be a cake?'),
	(24, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650802652/PhotoContest/ContestPhotos/contestPhotoID:toilet_paper.jpg.jpg', 'A Cake Roll?', 9, 'Is it a toilet roll or is it a cake?'),
	(25, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650802786/PhotoContest/ContestPhotos/contestPhotoID:sirloin_stake.jpg.jpg', 'Sirloin Steak', 13, 'Sirloin steak with a garnish of peas, carrots and potatoes or ...a cake?'),
	(26, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650802936/PhotoContest/ContestPhotos/contestPhotoID:netflix-tiktok-will-it-cake.jpg.jpg', 'Seasonal Veggies', 14, 'Asparagus, cauliflower, something spikey or a cake?'),
	(27, 40, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650803032/PhotoContest/ContestPhotos/contestPhotoID:ducklings.jpg.jpg', 'Cute Ducklings', 15, 'Take them for a bath but they might melt! These cute ducklings are in fact a sugary delight!'),
	(28, 41, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650803511/PhotoContest/ContestPhotos/contestPhotoID:jane-almon-7rriIaBH6JY-unsplash.jpg.jpg', 'Big Glasses Dog', 7, 'Oh yes, now I can really see you!'),
	(29, 41, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650803669/PhotoContest/ContestPhotos/contestPhotoID:karsten-winegeart-qy0BHykaq0E-unsplash.jpg.jpg', 'Dressed To Impress', 35, 'G. is for Gangster, Bro!'),
	(30, 41, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650803837/PhotoContest/ContestPhotos/contestPhotoID:karsten-winegeart-stB6muBBXMc-unsplash.jpg.jpg', 'French Fries with a Dog', 13, 'Could I have some French fries as well? '),
	(31, 41, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650804063/PhotoContest/ContestPhotos/contestPhotoID:vidar-nordli-mathisen-cSsvUtTVr0Q-unsplash.jpg.jpg', 'Pug On A Walk', 18, 'Come on, let\'s go! Don\'t be stubborn.  '),
	(32, 41, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650804166/PhotoContest/ContestPhotos/contestPhotoID:karsten-winegeart-n3c5pSoD2Fc-unsplash.jpg.jpg', 'Rudolf The Dog', 24, 'Ho, ho, ho, Merry Xmas!'),
	(33, 41, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650804403/PhotoContest/ContestPhotos/contestPhotoID:cookie-the-pom-gySMaocSdqs-unsplash.jpg.jpg', 'Intelligent Dog', 27, 'Just quickly scanning the daily news...'),
	(34, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650805052/PhotoContest/ContestPhotos/contestPhotoID:bryce_canyon_utah.png.png', 'Bryce Canyon Utah', 21, 'Bryce Canyon National Park  is an American national park located in southwestern Utah. The major feature of the park is Bryce Canyon, which despite its name, is not a canyon, but a collection of giant natural amphitheaters along the eastern side of the Paunsaugunt Plateau. '),
	(35, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650805234/PhotoContest/ContestPhotos/contestPhotoID:nepalese%20winter.png.png', 'Nepalese Winter', 22, 'It\'s hard to find a region more remote and untouched than the Nepalese Himalayas. The sky-high country is defined as much by its rock formations and colorful stupas as by its unfathomable stretches of jagged mountains.'),
	(36, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650805330/PhotoContest/ContestPhotos/contestPhotoID:harbin_china.png.png', 'Harbin, China', 23, 'Every January in northern China, upwards of a million tourists flock to an attraction unlike anything else in the world: a temporary city made entirely out of ice. The Harbin International Ice & Snow Sculpture Festival has taken place every winter since 1963, covering some eight million square feet with frozen, LED-covered skyscrapers, palaces, and sculptures.'),
	(37, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650805444/PhotoContest/ContestPhotos/contestPhotoID:bulguksa%20temple%20korea.png.png', 'South Korea', 24, 'Built in 774, the Temple of Bulguksa is found in the incredibly scenic Gyeongju National Park on the slopes of Mount Tohamsan. The complex\'s stone terraces, bridges, and pagodas look especially lovely in winter, covered by a light layer of snow.'),
	(38, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650805532/PhotoContest/ContestPhotos/contestPhotoID:lofoten_islands_norway.png.png', 'Lofoten Islands, Norway', 25, 'Just off Norway\'s northwestern coast, the Lofoten Islands have clear blue waters that rival the Mediterranean, craggy mountains that rival Iceland, and enough edgy museums and fishing villages to rival...well, just pick any Scandinavian hot spot. Yet while Lofoten\'s sites may draw some comparisons, the beauty here is truly unparalleled: think colorful fishing villages, majestic fjords, and tons of opportunities to see the Northern Lights in the winter.'),
	(39, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650805839/PhotoContest/ContestPhotos/contestPhotoID:Plitviche_Croatia.png.png', 'Croatian Winter', 29, 'The 16 terraced lakes of Croatia\'s Plitvice Lakes National Park are connected by waterfalls and vary in shades of blue, resulting in something that defies the imagination especially when the falls freeze over.'),
	(40, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650806347/PhotoContest/ContestPhotos/contestPhotoID:Screenshot%202022-04-24%20111447.png.png', 'Swiss Winter Tale', 31, 'Surrounded by the Alps, Switzerland is easily one of the most beautiful countries in the world, no matter when you\'re visiting. Come wintertime though, it becomes a veritable wonderland, with powdery ski slopes, frozen lakes, and the most luxurious resort towns you\'ll ever see.'),
	(41, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650806629/PhotoContest/ContestPhotos/contestPhotoID:Quebec_City.png.png', 'Quebec City', 34, 'Quebec City is one of the oldest cities in North America, and its colonial French architecture gives it an unmistakably European feel. \r\n'),
	(42, 42, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650807195/PhotoContest/ContestPhotos/contestPhotoID:lake_blaen_slovenia.png.png', 'Lake Bled, Slovenia', 16, 'With its church-dotted islet and waters begging for rowboat rides, it\'s no wonder Lake Bled is one of the most popular destinations in Slovenia. The site gets even more postcard-worthy when the surrounding Julian Alps become covered with snow, and a fog settles over the lake.'),
	(43, 44, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650808280/PhotoContest/ContestPhotos/contestPhotoID:cojo-rosales-FnwL3-PCl6E-unsplash.jpg.jpg', 'Kibo, Kenya', 25, 'Adventure - seekers from all over the globe head to the border of Tanzania and Kenya to tackle Kilimanjaro, Africa\'s tallest mountain. Fewer know that Kili is also the continent\'s tallest volcano, unique in the fact that it has three volcanic cones.  While the Mawenzi and Shira cones are extinct, the highest one, Kibo, is still active and lets off occasional steam and gases.'),
	(44, 44, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650808993/PhotoContest/ContestPhotos/contestPhotoID:marc-szeglat-Aduh0KXCI1w-unsplash.jpg.jpg', 'Mount Nyiragongo, Congo', 22, 'Mount Nyiragongo may not be one of the most explosive volcanoes in the world - the shield volcano instead expels long flowing rivers of lava like Hawaii\'s Kilauea volcano - but it has the potential to be one of the most destructive.'),
	(46, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650810483/PhotoContest/ContestPhotos/contestPhotoID:iStock-1199718816-scaled.jpg.jpg', 'B767 Cockpit', 20, 'Nothing like a night view from the flight deck on auto pilot!'),
	(47, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650810745/PhotoContest/ContestPhotos/contestPhotoID:qantas_pride.jpg.jpg', 'Boeing Jumbo 747', 34, 'Qantas Boeing 747 Dressed To Impress - the aircraft is covered in aboriginal art and craft design '),
	(48, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650810844/PhotoContest/ContestPhotos/contestPhotoID:flying_over_singapore.jpg.jpg', 'Singapore Harbor ', 22, 'A view of Singapore Harbor from the aircraft on approach'),
	(49, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650810946/PhotoContest/ContestPhotos/contestPhotoID:SIA1.jpg.jpg', 'Singapore Airlines 777', 29, 'The picture says it all - top international carrier sporting a 777 Boeing '),
	(50, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650811022/PhotoContest/ContestPhotos/contestPhotoID:Screenshot-496.png.png', 'Boeing Boeing', 36, 'If it ain\'t Boeing, I ain\'t going'),
	(51, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650811166/PhotoContest/ContestPhotos/contestPhotoID:Emirates-Boeing-777-200LR.jpg.jpg', 'Emirates Boeing 777', 7, 'Emirates 777 parked on ground'),
	(52, 46, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650811292/PhotoContest/ContestPhotos/contestPhotoID:emirates-b777-business-class.jpg.jpg', 'Compact Business ', 12, 'The new compact all Business class Boeing 777 sporting the face of Emirati hospitality. '),
	(53, 44, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650887960/PhotoContest/ContestPhotos/contestPhotoID:volcanic-cone-Japan-Mount-Fuji.jpg.jpg', 'Mount Fuji, Japan', 5, 'Japan\'s Mt. Fuji is an active volcano about 100 km southwest of Tokyo. Commonly called "Fuji-san," it\'s the country\'s tallest peak, at 3,776 meters. A pilgrimage site for centuries, it\'s considered one of Japan\'s 3 sacred mountains, and summit hikes remain a popular activity. Its iconic profile is the subject of numerous works of art, notably Edo Period prints by Hokusai and Hiroshige.'),
	(54, 44, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650809229/PhotoContest/ContestPhotos/contestPhotoID:piermanuele-sberni-V71v27xm5Kk-unsplash.jpg.jpg', 'Tomboro, Indonesia', 29, 'Mount Tambora, or Tomboro, is an active stratovolcano in West Nusa Tenggara, Indonesia. Located on Sumbawa in the Lesser Sunda Islands, it was formed by the active subduction zones beneath it. Before 1815, it was more than 4,300 metres high, making it one of the tallest peaks in the Indonesian archipelago.'),
	(55, 44, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650889716/PhotoContest/ContestPhotos/contestPhotoID:yellow%20stone.jpg.jpg', 'Yellowstone Caldera', 36, 'The Yellowstone Caldera, sometimes referred to as the Yellowstone Supervolcano, is a volcanic caldera and supervolcano in Yellowstone National Park in the Western United States. The caldera and most of the park are located in the northwest corner of Wyoming'),
	(56, 44, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650890074/PhotoContest/ContestPhotos/contestPhotoID:photogenic%20volcano.jpg.jpg', 'East Java, Indonesia', 19, 'A pristine turquoise lake crowns the crater of this spectacular 9,100-foot mountain in East Java. But while the tropical color of the water may look inviting, it contains sulfuric acid. Though Kawah Ijen is not currently erupting, its restlessness can be sensed from underfoot as steam hisses from unseen vents in the crater surface.'),
	(57, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650895690/PhotoContest/ContestPhotos/contestPhotoID:dimitry-anikin-rTKn2UWXrVs-unsplash.jpg.jpg', 'Rilski Monastery', 1, 'The Monastery of Saint Ivan of Rila, better known as Rila Monastery "Sveti Ivan Rilski" is the largest and most famous Eastern Orthodox monastery in Bulgaria. It is situated in the southwestern Rila Mountains, 117 km (73 mi) south of the capital Sofia in the deep valley of the Rilska River ("Rila River") at an elevation of 1,147 m (3,763 ft) above sea level, inside of Rila Monastery Nature Park. The monastery is named after its founder, the hermit Ivan of Rila (876 - 946 AD), and houses around 60 monks.'),
	(58, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650895837/PhotoContest/ContestPhotos/contestPhotoID:aerial-view-by-drone-alexander-nevsky-russian-orthodox-cathedral-sofia-bulgaria-europe-RHPLF13927.jpg.jpg', 'Alexander Nevsky Cathedral', 12, 'St. Alexander Nevsky Cathedral is a Bulgarian Orthodox cathedral in Sofia, the capital of Bulgaria. Built in Neo-Byzantine style, it serves as the cathedral church of the Patriarch of Bulgaria and it is believed to be one of the 50 largest Christian church buildings by volume in the world.'),
	(59, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650895956/PhotoContest/ContestPhotos/contestPhotoID:nikolay-hristov-7IGfv8Fr3X0-unsplash.jpg.jpg', 'Endless Fields of Lavender', 13, 'Bulgaria is home to the largest growing area in the world for lavender oil, producing each year between 100 MT-130 MT (depending mainly on climatic conditions). Distilled from the plant flower spikes, lavender oil is one of the world\'s most traditional essential oils frequently used in fragrances and aromatherapy, despite little evidence proving its therapeutic values. '),
	(60, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650896056/PhotoContest/ContestPhotos/contestPhotoID:dorothea-oldani-rwN2Iv0k5Gg-unsplash.jpg.jpg', 'Belogradchik', 14, 'The Belogradchik Rocks are a group of strangely shaped sandstone and conglomerate rock formations located on the western slopes of the Balkan Mountains near the town of Belogradchik in northwest Bulgaria. The rocks vary in color from primarily red to yellow; some of the rocks reach up to 200 m in height.'),
	(61, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650896281/PhotoContest/ContestPhotos/contestPhotoID:valeri-terziyski-Tgpm1NYe8bQ-unsplash.jpg.jpg', 'Naroden Teatar Ivan Vazov', 24, 'The Ivan Vazov National Theatre is Bulgaria\'s national theatre, as well as the oldest and most authoritative theatre in the country and one of the important landmarks of Sofia, the capital of Bulgaria. It is located in the centre of the city, with the facade facing the City Garden.'),
	(62, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650896447/PhotoContest/ContestPhotos/contestPhotoID:stefan-vladimirov-Q_Moi2xjieU-unsplash.jpg.jpg', 'Tasty Bulgarian Food', 21, 'Bulgarian cuisine is a representative of the cuisine of Southeast Europe. It shares characteristics with other Balkan cuisines. Bulgarian cooking traditions are diverse because of geographical factors such as climatic conditions suitable for a variety of vegetables, herbs, and fruit. Aside from the vast variety of local Bulgarian dishes, Bulgarian cuisine shares a number of dishes with Persian, Turkish, and Greek cuisine. Bulgarian food often incorporates salads as appetizers and is also noted for the prominence of dairy products, wines and other alcoholic drinks such as rakia.'),
	(64, 48, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650897363/PhotoContest/ContestPhotos/contestPhotoID:sebastien-gabriel--IMlv9Jlb24-unsplash.jpg.jpg', 'Croation Sunset', 1, '"Zadar has the most beautiful sunset in the world, more beautiful than the one in Key West, Florida." These words were uttered by Alfred Hitchcock in 1964. Of course, one can safely assume Hitchcock never witnessed the sun set in Bulgaria.'),
	(65, 48, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650897761/PhotoContest/ContestPhotos/contestPhotoID:melvin-sra2BjMOREo-unsplash.jpg.jpg', 'Magic Ball Sunset', 30, 'Can you capture the Sun, I mean really capture it, not in a shot but may be in a container?'),
	(66, 47, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650898118/PhotoContest/ContestPhotos/contestPhotoID:pietra-schwarzler-FqdfVIdgR98-unsplash.jpg.jpg', 'Beautiful Sunset', 35, 'Any sunset would be stunning if you are there to witness it!'),
	(67, 48, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650898259/PhotoContest/ContestPhotos/contestPhotoID:artem-sapegin-8c6eS43iq1o-unsplash.jpg.jpg', 'Bulgarian Sunset', 31, 'The sun setting behind the Bulgarian mountains is always a breathtaking scene!'),
	(68, 48, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650898437/PhotoContest/ContestPhotos/contestPhotoID:african-sunset-paintings.jpg.jpg', 'African Sunset', 34, 'The Sun is biggest and reddest in Africa!'),
	(69, 48, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650898627/PhotoContest/ContestPhotos/contestPhotoID:frank-mckenna-OD9EOzfSOh0-unsplash.jpg.jpg', 'Marine Sunset', 5, 'The Sun setting over the Sea is a pleasure to observe.'),
	(70, 48, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650898841/PhotoContest/ContestPhotos/contestPhotoID:ryan-young-0U2Ze4QcbB0-unsplash.jpg.jpg', 'Romantic Sunset', 22, 'You, me, the sun, the sea ...'),
	(71, 49, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650906955/PhotoContest/ContestPhotos/contestPhotoID:gardens%20by%20the%20bay.jpg.jpg', 'Super Trees @ Gardens by the Bay', 22, 'Gardens by the Bay was part of the nation\'s plans to transform its "Garden City" to a "City in a Garden", with the aim of raising the quality of life by enhancing greenery and flora in the city. First announced by Prime Minister Lee Hsien Loong at Singapore\'s National Day Rally in 2005, Gardens by the Bay was intended to be Singapore\'s premier urban outdoor recreation space and a national icon.'),
	(72, 49, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650907279/PhotoContest/ContestPhotos/contestPhotoID:meric-dagli-RG7jGL8wkCs-unsplash.jpg.jpg', 'Parkroyal on Pickering Street', 29, 'Parkroyal Collection Pickering, Singapore is a luxury hotel located in the Central Area of Singapore. It is a "hotel-in-a-garden" with 15,000 m2 (160,000 sq ft) of elevated terraced gardens.'),
	(73, 49, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650907459/PhotoContest/ContestPhotos/contestPhotoID:Chinese%20New%20Year%20Dragon%20Dance.webp.webp', 'Dragon Dance', 34, 'The dragon dance is often performed during Chinese New Year. Chinese dragons are a symbol of China\'s culture, and they are believed to bring good luck to people, therefore the longer the dragon is in the dance, the more luck it will bring to the community. The dragons are believed to possess qualities that include great power, dignity, fertility, wisdom and auspiciousness. '),
	(74, 49, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650907584/PhotoContest/ContestPhotos/contestPhotoID:little%20india.jpg.jpg', 'Little India', 21, 'Little India is distinct from the Chulia Kampong area, which, under the Raffles Plan of Singapore, was originally a division of colonial-era Singapore where ethnic Indian immigrants would reside under the plan\'s outline of the formation of ethnic enclaves. However, as Chulia Kampong became more crowded and competition for land escalated, many ethnic Indians emigrants moved into what is now known as Little India.'),
	(75, 49, 'http://res.cloudinary.com/photocontestapi/image/upload/v1650908030/PhotoContest/ContestPhotos/contestPhotoID:03-esplanade.jpg.jpg', 'Esplanade, Singapore', 24, 'The Esplanade is a waterfront location just north of the mouth of the Singapore River in downtown Singapore. It is primarily occupied by the Esplanade Park, and was the venue for one of Singapore\'s largest congregation of satay outlets until their relocation to Clarke Quay as a result of the construction of a major performance arts venue, the Esplanade - Theatres on the Bay, which took its name from this location.\r\n'),
	(76, 49, 'https://res.cloudinary.com/photocontestapi/image/upload/v1650908608/PhotoContest/ContestPhotos/01-unsplash-singapore_pd4cwo.jpg', 'Marina Bay Sands', 36, 'The resort is designed by Moshe Safdie, who says it was initially inspired by card decks. The prominent feature of the design is the three hotel towers, which has 2,500 rooms and suites, and a continuous lobby at the base links the three towers. The casino has a four-storey central atrium with four levels of gaming and entertainment in one space.');
/*!40000 ALTER TABLE `contests_photos` ENABLE KEYS */;

-- Dumping structure for table photo-contest.jury
CREATE TABLE IF NOT EXISTS `jury` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `jury_id_uindex` (`id`),
  KEY `jury_contests_id_fk` (`contest_id`),
  KEY `jury_users_id_fk` (`user_id`),
  CONSTRAINT `jury_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `jury_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.jury: ~39 rows (approximately)
/*!40000 ALTER TABLE `jury` DISABLE KEYS */;
INSERT INTO `jury` (`id`, `user_id`, `contest_id`) VALUES
	(62, 4, 39),
	(63, 115, 39),
	(64, 35, 39),
	(65, 6, 39),
	(66, 27, 39),
	(67, 30, 39),
	(68, 10, 40),
	(69, 4, 40),
	(70, 115, 40),
	(71, 32, 40),
	(72, 6, 40),
	(73, 10, 41),
	(74, 4, 41),
	(75, 115, 41),
	(76, 6, 41),
	(77, 23, 41),
	(78, 4, 42),
	(79, 115, 42),
	(80, 35, 42),
	(81, 6, 42),
	(82, 30, 42),
	(94, 4, 44),
	(95, 115, 44),
	(96, 6, 44),
	(97, 30, 44),
	(101, 4, 46),
	(102, 115, 46),
	(103, 6, 46),
	(104, 4, 47),
	(105, 115, 47),
	(106, 6, 47),
	(107, 29, 47),
	(108, 4, 48),
	(109, 115, 48),
	(110, 6, 48),
	(111, 29, 48),
	(112, 4, 49),
	(113, 115, 49),
	(114, 6, 49);
/*!40000 ALTER TABLE `jury` ENABLE KEYS */;

-- Dumping structure for table photo-contest.notifications
CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` text NOT NULL,
  `creation_date` datetime NOT NULL,
  `is_read` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `table_name_contests_id_fk` (`contest_id`),
  KEY `table_name_users_id_fk` (`user_id`),
  KEY `type` (`type`),
  CONSTRAINT `table_name_contests_id_fk` FOREIGN KEY (`contest_id`) REFERENCES `contests` (`id`),
  CONSTRAINT `table_name_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  CONSTRAINT `type` FOREIGN KEY (`type`) REFERENCES `notification_types` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.notifications: ~45 rows (approximately)
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
INSERT INTO `notifications` (`id`, `message`, `creation_date`, `is_read`, `user_id`, `contest_id`, `type`) VALUES
	(26, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 13:50:41', 1, 4, 39, 1),
	(27, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 13:50:41', 1, 115, 39, 1),
	(28, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 13:50:41', 0, 35, 39, 1),
	(29, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 13:50:41', 1, 6, 39, 1),
	(30, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 13:50:41', 1, 27, 39, 1),
	(31, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 13:50:41', 0, 30, 39, 1),
	(32, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:00:45', 0, 10, 40, 1),
	(33, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:00:45', 1, 4, 40, 1),
	(34, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:00:45', 1, 115, 40, 1),
	(35, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:00:45', 0, 32, 40, 1),
	(36, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:00:45', 1, 6, 40, 1),
	(37, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:28:05', 0, 10, 41, 1),
	(38, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:28:05', 1, 4, 41, 1),
	(39, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:28:05', 1, 115, 41, 1),
	(40, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:28:05', 0, 6, 41, 1),
	(41, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:28:05', 0, 23, 41, 1),
	(42, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:50:32', 1, 4, 42, 1),
	(43, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:50:32', 0, 115, 42, 1),
	(44, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:50:32', 0, 35, 42, 1),
	(45, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:50:32', 0, 6, 42, 1),
	(46, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 15:50:32', 0, 30, 42, 1),
	(65, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 1, 4, 44, 1),
	(66, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 0, 115, 44, 1),
	(67, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 0, 6, 44, 1),
	(68, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 0, 30, 44, 1),
	(69, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 1, 25, 44, 2),
	(70, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 1, 22, 44, 2),
	(71, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 1, 18, 44, 2),
	(72, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 0, 16, 44, 2),
	(73, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 1, 9, 44, 2),
	(74, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 16:47:49', 1, 28, 44, 2),
	(78, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 17:23:45', 0, 4, 46, 1),
	(79, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 17:23:45', 0, 115, 46, 1),
	(80, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-24 17:23:45', 0, 6, 46, 1),
	(81, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 16:53:02', 0, 4, 47, 1),
	(82, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 16:53:02', 0, 115, 47, 1),
	(83, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 16:53:02', 0, 6, 47, 1),
	(84, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 16:53:02', 0, 29, 47, 1),
	(85, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 17:29:35', 0, 4, 48, 1),
	(86, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 17:29:35', 0, 115, 48, 1),
	(87, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 17:29:35', 0, 6, 48, 1),
	(88, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 17:29:35', 0, 29, 48, 1),
	(89, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 19:16:39', 0, 4, 49, 1),
	(90, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 19:16:39', 0, 115, 49, 1),
	(91, 'You have been selected to be jury for this contest. Review wisely!', '2022-04-25 19:16:39', 0, 6, 49, 1);
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;

-- Dumping structure for table photo-contest.notification_types
CREATE TABLE IF NOT EXISTS `notification_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `notification_types_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.notification_types: ~2 rows (approximately)
/*!40000 ALTER TABLE `notification_types` DISABLE KEYS */;
INSERT INTO `notification_types` (`id`, `type`) VALUES
	(1, 'jury'),
	(2, 'contest_participation');
/*!40000 ALTER TABLE `notification_types` ENABLE KEYS */;

-- Dumping structure for table photo-contest.ranks
CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rank_name` varchar(30) NOT NULL,
  `min_score` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `ranks_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.ranks: ~4 rows (approximately)
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` (`id`, `rank_name`, `min_score`) VALUES
	(1, 'junkie', 0),
	(2, 'enthusiast', 51),
	(3, 'master', 151),
	(4, 'wise_and_benevolent_dictator', 1001);
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;

-- Dumping structure for table photo-contest.reviews
CREATE TABLE IF NOT EXISTS `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `comment` varchar(8192) NOT NULL,
  `photo_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reviews_id_uindex` (`id`),
  KEY `reviews_contests_photos_id_fk` (`photo_id`),
  KEY `reviews_users_id_fk` (`user_id`),
  CONSTRAINT `reviews_contests_photos_id_fk` FOREIGN KEY (`photo_id`) REFERENCES `contests_photos` (`id`),
  CONSTRAINT `reviews_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.reviews: ~78 rows (approximately)
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` (`id`, `user_id`, `score`, `comment`, `photo_id`) VALUES
	(6, 4, 7, 'Nice shot. Rather entertaining Well done!', 11),
	(8, 4, 9, 'Fantastic shot! Great camaraderie! What are these animals called?', 12),
	(10, 4, 5, 'I wouldn\'t want to be in this mouse shoes', 13),
	(11, 4, 9, 'What a petulant mocking bird. Awesome shot!', 14),
	(12, 4, 10, 'Haha, super cute! 10 out of 10 from me!', 15),
	(13, 4, 5, 'Meh, I bet he is not a better dancer than me!:)', 16),
	(14, 4, 9, 'Yepp, social distancing is key prevention measure. Great shot!', 17),
	(15, 4, 7, 'Hope they are in a good mood. Wouldn\'t want to come across those gossipmongers on an empty stomach.', 18),
	(16, 4, 6, 'How do you know they are not a father and a son?:)', 19),
	(17, 4, 6, 'That bear is curious indeed! Great work', 20),
	(18, 35, 6, 'These guys are really funny! Awesome shot!', 11),
	(19, 35, 10, 'Wow, so cute! Real buddies, real friendship:)', 12),
	(20, 35, 5, 'This looks grim in prospects for the mouse', 13),
	(21, 35, 6, 'Someone is a sucker for rules!:)', 14),
	(22, 35, 10, 'O soleeeee miooooooo! Can almost here it sing:)', 15),
	(23, 35, 4, 'Not my Saturday night moves! More like a waltz', 16),
	(24, 35, 6, 'Good shot, nice story line too...', 17),
	(25, 35, 7, 'What are these two kanoodling over?!:)', 18),
	(26, 35, 6, 'I like how their tails intertwine. It is obvious they share kinship:)', 19),
	(27, 35, 4, 'Did someone eat the photographer?', 20),
	(29, 6, 10, 'Super funny! Giving you top score for an excellent shot!', 11),
	(30, 6, 10, 'Yup! Caught in action alright...', 12),
	(31, 6, 5, 'I am rooting for the mouse. Good shot but kind of sad too', 13),
	(32, 6, 10, 'Someone is illiterate :)           ', 14),
	(33, 6, 10, 'Really funny! Well done                ', 15),
	(34, 6, 3, 'Looks weird                       ', 16),
	(35, 6, 4, 'Good context for your shot. Well captured', 17),
	(36, 6, 8, 'Wow. Wouldn\'t get close to these two! Cheers for the photographer', 18),
	(37, 6, 10, 'Very endearing shot. I like elephants!', 19),
	(38, 6, 5, 'Funny indeed                     ', 20),
	(39, 27, 8, 'Cool shot. Well done!                         ', 11),
	(40, 27, 10, 'A snap in the precise moment. Well done!', 12),
	(41, 27, 4, 'I don\'t think mice make for a tasty dinner', 13),
	(42, 27, 7, 'The sign looks freshly painted   ', 14),
	(43, 27, 8, 'Very cute shot. Spot on                     ', 15),
	(44, 27, 6, 'Not the most elegant dancer out there ', 16),
	(45, 27, 5, 'Nice subtitles. Well done         ', 17),
	(46, 27, 8, 'Great shot!                              ', 18),
	(47, 27, 6, 'Seen a few too many of these shots. Polar bears exploring tech kits', 20),
	(48, 10, 10, 'It really looks like an aubergine. Would not have guessed it a cake', 21),
	(49, 10, 10, 'Looks succulent and delicious! Shame it\'s a cake:)', 22),
	(50, 10, 10, 'Wow, almost stinks my eyes. Great shot!', 23),
	(51, 10, 10, 'Definitely a surprise cut:) Great shot', 24),
	(52, 10, 8, 'I think it is a side of mushrooms and not potatoes:) ', 25),
	(53, 10, 7, 'Very convincing!                   ', 26),
	(54, 10, 6, 'Cute duckies:)                            ', 27),
	(55, 4, 10, 'Awesome creation. Looks pretty real ', 21),
	(56, 4, 10, 'Who can say no to good steak!    ', 22),
	(57, 4, 9, 'The peal looks super real       ', 23),
	(58, 4, 9, 'It is probably not what I would like to put in my mouth but a very convincing appearance!', 24),
	(59, 4, 7, 'Looks super rare. Still bleeding and moving:_', 25),
	(60, 4, 7, 'Not a big fan of veggies but well constructed', 26),
	(61, 4, 5, 'They really look plastic           ', 27),
	(62, 6, 10, 'Fresh looking eggplant!            ', 21),
	(63, 6, 6, 'The marbeling is not very convincing ', 22),
	(64, 6, 10, 'Got me fooled! Great shot!      ', 23),
	(65, 6, 10, 'Looks triply and very convincing:)   ', 24),
	(66, 6, 8, 'Great dish and a great shot. Well done!  ', 25),
	(67, 6, 6, 'They look a little waxy to me     ', 26),
	(68, 6, 9, 'I would take them for a swim. Super convincing ', 27),
	(69, 4, 8, 'Beautiful rendition of Mount Kilimanjaro', 43),
	(70, 4, 10, 'Phenomenal shot! Up, close and personal with an active volcano! ', 44),
	(71, 4, 10, 'Fuji-san is truly majestic! Great shot! ', 53),
	(72, 4, 6, 'Very dramatic scene. There are some horizonal light lines reflected taking away from the intensity of the moment', 54),
	(73, 4, 8, 'Very steamy. Quite an enticing snap! ', 55),
	(74, 4, 7, 'A truly photogenic volcano, albeit a bit shy:)', 56),
	(75, 6, 5, 'Pretty picture, thanks for the submission!', 43),
	(76, 6, 9, 'What a shot! Truly heart-stopping!', 44),
	(77, 6, 7, 'Beautiful mount Fuji. There is not a bad angle for a picture of this iconic volcano', 53),
	(78, 6, 5, 'The picture is a bit fuzzy and unfocused but certainly captivating and dramatic ', 54),
	(79, 6, 6, 'Pretty and steamy. I like the richness of the colours', 55),
	(80, 6, 8, 'I\'ve never seen a shot like this before. Almost as if photo-shopped', 56),
	(81, 115, 6, 'Pretty but forgettable          ', 43),
	(82, 115, 10, 'Breathtaking photo!                       ', 44),
	(83, 115, 10, 'An iconic valcono captured so adeptly ', 53),
	(84, 115, 8, 'Looks a like a risky capture. Hope you used a drone to take this one!', 54),
	(85, 115, 10, 'I bet you can boil an egg in these waters. Fantastic capture!', 55),
	(86, 115, 10, 'Such a pretty shot! Love it!       ', 56);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;

-- Dumping structure for table photo-contest.roles
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(60) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_role_id_uindex` (`id`),
  UNIQUE KEY `roles_role_name_uindex` (`role_name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.roles: ~3 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`id`, `role_name`) VALUES
	(3, 'admin'),
	(2, 'organizer'),
	(1, 'photo_junkie');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping structure for table photo-contest.statuses
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `statuses_id_uindex` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.statuses: ~2 rows (approximately)
/*!40000 ALTER TABLE `statuses` DISABLE KEYS */;
INSERT INTO `statuses` (`id`, `name`) VALUES
	(1, 'Open'),
	(2, 'Invitational');
/*!40000 ALTER TABLE `statuses` ENABLE KEYS */;

-- Dumping structure for table photo-contest.tokens
CREATE TABLE IF NOT EXISTS `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` text NOT NULL,
  `expired_at` datetime NOT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `tokens_users_id_fk` (`user_id`),
  CONSTRAINT `tokens_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.tokens: ~9 rows (approximately)
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` (`id`, `token`, `expired_at`, `confirmed_at`, `created_at`, `user_id`) VALUES
	(1, '81aa0d89-90ea-45b1-adb0-9fc2f44c9872', '2022-04-06 13:36:38', NULL, '2022-04-06 13:21:38', 2),
	(2, 'a1823c32-0c6e-4783-b4bb-117821a20216', '2022-04-06 13:37:28', NULL, '2022-04-06 13:22:28', 2),
	(3, 'afc8344d-5aff-4cc7-a53b-2aa4997c1713', '2022-04-06 13:37:50', NULL, '2022-04-06 13:22:50', 2),
	(4, '569e8dc4-5cbf-4f0c-a766-35c60fd16521', '2022-04-06 14:59:31', NULL, '2022-04-06 14:44:31', 2),
	(5, 'bee5eac1-3d9b-4fdd-8d3d-c7c4d344f59e', '2022-04-18 19:01:41', NULL, '2022-04-18 18:46:41', 2),
	(6, '9bd6c432-4ad9-4218-99a3-3c6f8dcb94c8', '2022-04-18 19:34:09', NULL, '2022-04-18 19:19:09', 2),
	(7, '4d4ff551-1d71-4b51-a4e0-c2ddff6c919f', '2022-04-18 19:39:50', NULL, '2022-04-18 19:24:50', 2),
	(8, 'ebc7773b-0589-4fd3-ae78-42057aa40c73', '2022-04-19 09:24:33', NULL, '2022-04-19 09:09:33', 2),
	(9, 'f0a6df8b-8c98-42b0-9571-cfdb9a979ff9', '2022-04-22 07:58:00', NULL, '2022-04-22 07:43:00', 115);
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;

-- Dumping structure for table photo-contest.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `registration_date` datetime NOT NULL,
  `role_id` int(11) NOT NULL,
  `score` int(11) NOT NULL,
  `rank_id` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `is_blocked` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_uindex` (`email`),
  UNIQUE KEY `users_id_uindex` (`id`),
  UNIQUE KEY `users_username_uindex` (`username`),
  KEY `users_ranks_id_fk` (`rank_id`),
  KEY `users_roles_role_id_fk` (`role_id`),
  CONSTRAINT `users_ranks_id_fk` FOREIGN KEY (`rank_id`) REFERENCES `ranks` (`id`),
  CONSTRAINT `users_roles_role_id_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=116 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.users: ~38 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `email`, `registration_date`, `role_id`, `score`, `rank_id`, `is_deleted`, `is_blocked`, `is_active`) VALUES
	(1, 'p.pavlova', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Plamenna', 'Pavlova', 'p.pavlova@gmail.com', '2022-02-08 12:23:25', 3, 900, 3, 0, 0, 1),
	(2, 'sofi', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Sofi', 'Stoyanova', 'sofi.stoyanova2@gmail.com', '2022-02-08 15:28:45', 3, 1200, 4, 0, 0, 1),
	(3, 'ivo.dimiev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivo', 'Dimiev', 'ivo.dimiev@telerik.com', '2022-02-08 15:28:25', 3, 1229, 4, 0, 0, 1),
	(4, 'denislav.vuchkov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Denislav', 'Vuchkov', 'vy4kov@gmail.com', '2022-02-08 12:23:24', 2, 300, 3, 0, 0, 1),
	(5, 'chavdar.d', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Chavdar', 'Dimitrov', 'chavdar.d@telerik.com', '2022-02-08 12:23:28', 1, 176, 3, 0, 0, 1),
	(6, 'dimitar.petrov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Dimitar', 'Petrov', 'd.petrov@telerik.com', '2022-02-08 12:23:29', 2, 80, 2, 0, 0, 1),
	(7, 'alex.velikova', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Alexandra', 'Velikova', 'a.velikova@telerik.com', '2012-02-08 15:16:10', 1, 60, 2, 0, 0, 1),
	(8, 'georgi.krastev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Georgi', 'Krustev', 'g.krastev@telerik.com', '2002-05-12 15:16:40', 3, 50, 1, 0, 0, 1),
	(9, 'ivaylo.stavrev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivaylo', 'Stavrev', 'i.stavrev@telerik.com', '2022-02-21 15:17:24', 1, 243, 3, 0, 0, 1),
	(10, 'd.seizov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Damian', 'Seizov', 'd.seizov@telerik.com', '1995-06-04 15:18:01', 1, 180, 3, 0, 0, 1),
	(12, 'andrey.mitev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Andrey', 'Mitev', 'a.mitev@telerik.com', '2022-02-08 15:19:24', 1, 300, 3, 0, 0, 1),
	(13, 'niya.tsaneva', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Niya', 'Tsaneva', 'niya.tsaneva@telerik.com', '2022-02-08 15:26:15', 1, 903, 3, 0, 0, 1),
	(14, 'emi.kalinova', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Emiliyana', 'Kalinova', 'e.kalinova@telerik.com', '2022-02-08 15:20:07', 1, 553, 3, 0, 0, 1),
	(15, 'z.georgiev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Zhelyazko', 'Georgiev', 'z.georgiev@gmail.com', '2022-02-08 15:20:37', 1, 153, 3, 0, 0, 1),
	(16, 'ivan.nikolov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivan', 'Nikolov', 'ivan.nikolov@abv.bg', '2022-02-08 15:21:02', 1, 43, 1, 0, 0, 1),
	(17, 'stef.sirakov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Stefan', 'Sirakov', 's.sirakov@gmail.com', '2022-02-08 15:23:15', 1, 155, 3, 0, 0, 1),
	(18, 'ivan.chepilev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivan', 'Chepilev', 'i.chepilev@abv.bg', '2022-02-08 15:21:48', 1, 43, 1, 0, 0, 1),
	(19, 'georgi.dinkov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Georgi', 'Dinkov', 'g.dinkov@yahoo.com', '2022-02-08 15:21:55', 1, 13, 1, 0, 0, 1),
	(20, 'i.danchev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Iskren', 'Danchev', 'i.danchev@yahoo.com', '2022-02-08 15:22:41', 1, 28, 1, 0, 0, 1),
	(21, 'g.stukanyov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Georgi', 'Stukanyov', 'stukanyov@telerik.com', '2022-02-08 15:23:58', 1, 263, 3, 0, 0, 1),
	(22, 'ivan.malinov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivan', 'Malinov', 'i.malinov@telerik.com', '2022-02-08 15:24:15', 1, 208, 3, 0, 0, 1),
	(23, 'lilia.georgieva', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Lilia', 'Georgieva', 'l.georgieva@telerik.com', '2022-02-08 15:24:18', 1, 0, 1, 0, 0, 1),
	(24, 'ivo.bankov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivo', 'Bankov', 'i.bankov@telerik.com', '2022-02-08 15:25:05', 1, 30, 1, 0, 0, 1),
	(25, 'ivan.petkov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivan', 'Petkov', 'ivan.petkov@telerik.com', '2022-02-08 15:25:34', 1, 126, 2, 0, 0, 1),
	(26, 'yordan.bradushev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Yordan', 'Bradushev', 'yordan.b@telerik.com', '2022-02-08 15:25:56', 1, 70, 2, 0, 0, 1),
	(27, 'martin.karalev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Martin', 'Karalev', 'martin.karalev@telerik.com', '2022-02-08 15:26:34', 1, 80, 2, 0, 0, 1),
	(28, 'ivaylo.ivanov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Ivaylo', 'Ivanov', 'ivaylo.ivanov@telerik.com', '2022-02-08 15:27:08', 1, 38, 1, 0, 0, 1),
	(29, 'plam.gospodinova', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Plamena', 'Gospodinova', 'plam.gospodinova@telerik.com', '2022-02-08 15:27:25', 1, 266, 3, 0, 0, 1),
	(30, 'martina.dimova', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Martina', 'Dimova', 'martina.dimova@telerik.com', '2022-02-08 15:27:46', 1, 0, 1, 0, 0, 1),
	(31, 'vilislav.angelov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Vilislav', 'Angelov', 'vilislav.angelov@telerik.com', '2022-02-08 15:28:08', 1, 0, 1, 0, 0, 1),
	(32, 'tihomir.dimitrov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Tihomir', 'Dimitrov', 'tihomir.dimitrov.95@gmail.com', '2022-02-08 12:23:20', 1, 0, 1, 0, 0, 1),
	(34, 'angel.giurov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Angel', 'Guirov', 'angel.guirov@telerik.com', '2022-02-08 15:29:11', 1, 13, 1, 0, 0, 1),
	(35, 'martin.todorov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Martin', 'Todorov', 'martin.todorov@telerik.com', '2022-02-08 15:29:34', 1, 43, 1, 0, 0, 1),
	(36, 'yoanna.stoeva', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Yoanna', 'Stoeva', 'yoanna.stoeva@telerik.com', '2022-02-08 15:29:50', 1, 38, 1, 0, 0, 1),
	(37, 'todor.andonov', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Todor', 'Andonov', 'todor@learn.com', '2015-02-09 11:17:52', 1, 0, 1, 1, 0, 1),
	(38, 'ti_bonev', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Todor', 'Bonev', 'ti_bonev@abv.bg', '2022-02-08 15:21:26', 1, 13, 1, 0, 0, 1),
	(112, 'testing', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'tester', 'testov', 'tester@gmail.com', '2022-04-05 18:42:57', 1, 0, 1, 1, 0, 1),
	(115, 'sofi4', '$2a$11$9MIgnpZpFaAtUl3BF3iF4.NGZQEh9yIPndaw5VOczyOXCz3vk.l1K', 'Sofi', 'Stoyanova', 'sofi.stoyanova4@gmail.com', '2022-04-22 07:42:59', 2, 0, 1, 0, 0, 1);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping structure for table photo-contest.users_photos
CREATE TABLE IF NOT EXISTS `users_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `photo_url` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_photos_id_uindex` (`id`),
  KEY `users_photos_users_id_fk` (`user_id`),
  CONSTRAINT `users_photos_users_id_fk` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- Dumping data for table photo-contest.users_photos: ~3 rows (approximately)
/*!40000 ALTER TABLE `users_photos` DISABLE KEYS */;
INSERT INTO `users_photos` (`id`, `user_id`, `photo_url`) VALUES
	(1, 2, 'https://res.cloudinary.com/photocontestapi/image/upload/v1650964132/PhotoContest/ProfilePhotos/sofi_wv4ehe.jpg'),
	(2, 3, 'https://res.cloudinary.com/photocontestapi/image/upload/v1650964087/PhotoContest/ProfilePhotos/ivo_y9omxa.jpg'),
	(3, 1, 'https://res.cloudinary.com/photocontestapi/image/upload/v1650964378/PhotoContest/ProfilePhotos/Passport_Photo_pb9bqm.jpg');
/*!40000 ALTER TABLE `users_photos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
