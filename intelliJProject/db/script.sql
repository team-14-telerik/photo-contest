create table categories
(
    id   int auto_increment
        primary key,
    name varchar(100) not null,
    constraint categories_id_uindex
        unique (id)
);

create table notification_types
(
    id   int auto_increment
        primary key,
    type varchar(40) not null,
    constraint notification_types_id_uindex
        unique (id)
);

create table ranks
(
    id        int auto_increment
        primary key,
    rank_name varchar(30) not null,
    min_score int         not null,
    constraint ranks_id_uindex
        unique (id)
);

create table roles
(
    id        int auto_increment
        primary key,
    role_name varchar(60) not null,
    constraint roles_role_id_uindex
        unique (id),
    constraint roles_role_name_uindex
        unique (role_name)
);

create table statuses
(
    id   int auto_increment
        primary key,
    name varchar(30) not null,
    constraint statuses_id_uindex
        unique (id)
);

create table contests
(
    id               int auto_increment
        primary key,
    title            varchar(150) not null,
    category_id      int          not null,
    status_id        int          not null,
    deadline_phase_1 datetime     not null,
    deadline_phase_2 datetime     null,
    photo_url        text         not null,
    finished         tinyint(1)   not null,
    constraint contests_id_uindex
        unique (id),
    constraint contests_categories_id_fk
        foreign key (category_id) references categories (id),
    constraint contests_statuses_id_fk
        foreign key (status_id) references statuses (id)
);

create table users
(
    id                int auto_increment
        primary key,
    username          varchar(60)          not null,
    password          varchar(60)          not null,
    first_name        varchar(50)          not null,
    last_name         varchar(50)          null,
    email             varchar(100)         not null,
    registration_date datetime             not null,
    role_id           int                  not null,
    score             int                  not null,
    rank_id           int                  not null,
    is_deleted        tinyint(1)           not null,
    is_blocked        tinyint(1)           not null,
    is_active         tinyint(1) default 0 not null,
    constraint users_email_uindex
        unique (email),
    constraint users_id_uindex
        unique (id),
    constraint users_username_uindex
        unique (username),
    constraint users_ranks_id_fk
        foreign key (rank_id) references ranks (id),
    constraint users_roles_role_id_fk
        foreign key (role_id) references roles (id)
);

create table contests_photos
(
    id         int auto_increment
        primary key,
    contest_id int           not null,
    photo_url  text          null,
    title      varchar(100)  null,
    user_id    int           not null,
    story      varchar(2000) not null,
    constraint contests_photos_id_uindex
        unique (id),
    constraint contests_photos_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint contests_photos_users_id_fk
        foreign key (user_id) references users (id)
);

create table jury
(
    id         int auto_increment
        primary key,
    user_id    int not null,
    contest_id int not null,
    constraint jury_id_uindex
        unique (id),
    constraint jury_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint jury_users_id_fk
        foreign key (user_id) references users (id)
);

create table notifications
(
    id            int auto_increment
        primary key,
    message       text                 not null,
    creation_date datetime             not null,
    is_read       tinyint(1) default 0 not null,
    user_id       int                  not null,
    contest_id    int                  not null,
    type          int                  not null,
    constraint table_name_contests_id_fk
        foreign key (contest_id) references contests (id),
    constraint table_name_users_id_fk
        foreign key (user_id) references users (id),
    constraint type
        foreign key (type) references notification_types (id)
);

create table reviews
(
    id       int auto_increment
        primary key,
    user_id  int           not null,
    score    int           not null,
    comment  varchar(8192) not null,
    photo_id int           not null,
    constraint reviews_id_uindex
        unique (id),
    constraint reviews_contests_photos_id_fk
        foreign key (photo_id) references contests_photos (id),
    constraint reviews_users_id_fk
        foreign key (user_id) references users (id)
);

create table tokens
(
    id           int auto_increment
        primary key,
    token        text     not null,
    expired_at   datetime not null,
    confirmed_at datetime null,
    created_at   datetime not null,
    user_id      int      not null,
    constraint tokens_users_id_fk
        foreign key (user_id) references users (id)
);

create table users_photos
(
    id        int auto_increment
        primary key,
    user_id   int  not null,
    photo_url text not null,
    constraint users_photos_id_uindex
        unique (id),
    constraint users_photos_users_id_fk
        foreign key (user_id) references users (id)
);