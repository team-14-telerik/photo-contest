package com.example.intellijproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IntelliJProjectApplication {

    public static void main(String[] args) {
        SpringApplication.run(IntelliJProjectApplication.class, args);
    }

}
