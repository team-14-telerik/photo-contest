package com.example.intellijproject.config;

import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;
import java.util.List;

import static springfox.documentation.builders.PathSelectors.regex;

public class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .paths(PathSelectors.any())
                .apis(RequestHandlerSelectors.basePackage("com.example.intellijproject"))
                .paths(regex("/api.*"))
                .build()
                .apiInfo(apiDetails())
                .securityContexts(List.of(securityContext()))
                .securitySchemes(List.of(apiKey()));
    }

    private SecurityContext securityContext() {
        return SecurityContext
                .builder()
                .securityReferences(defaultAuth())
                .build();
    }

    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        return List.of(new SecurityReference("JWT", authorizationScopes));
    }

    private ApiInfo apiDetails() {
        return new ApiInfo(
                "Photo contest - Ivo, Plami and Sofi",
                "Photo contest app",
                "version: 1.0",
                "Terms of Service",
                new springfox.documentation.service.Contact("Sofi Stoyanova, Plamenna Pavlova and Ivo Dimiev",
                        "", "dimievivo@gmail.com, pppavlova@gmail.com, sofi.stoyanova2@gmail.com"),
                "Developed by: Sofi Stoyanova, Plamenna Pavlova and Ivo Dimiev",
                "",
                Collections.emptyList());

    }

    private ApiKey apiKey() {
        return new ApiKey("JWT", "Authorization", "header");
    }


}

