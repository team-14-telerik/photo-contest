package com.example.intellijproject.config.security;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.Rank;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.UserRepository;
import com.example.intellijproject.services.contracts.RankService;
import com.example.intellijproject.services.contracts.RoleService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {
    private final String ENCODED_PASSWORD = "$2a$11$85ohgf80aMIwYIsgua18zOATBra0aYeHg4zB0ln8VScBSUFWQJ0tC";
    private final UserRepository userRepository;
    private final RankService rankService;
    private final RoleService roleService;

    public AuthenticationSuccessHandlerImpl(UserRepository userRepository, RankService rankService,
                                            RoleService roleService) {
        this.userRepository = userRepository;
        this.rankService = rankService;
        this.roleService = roleService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException, ServletException {
        if(authentication instanceof OAuth2AuthenticationToken) {
            OAuth2AuthenticationToken auth2AuthenticationToken = (OAuth2AuthenticationToken) authentication;
            String username = auth2AuthenticationToken.getPrincipal().getName();

            if(auth2AuthenticationToken.getAuthorizedClientRegistrationId().equals("google")) {
                OAuth2User oAuth2User = auth2AuthenticationToken.getPrincipal();
                String email = oAuth2User.getAttribute("email");
                String fullName = oAuth2User.getAttribute("name");
                String firstName = fullName.split(" ")[0];
                String lastName = fullName.split(" ")[1];
                String password = oAuth2User.getAttribute("password");
                User user;
                try {
                    user = userRepository.getByField("email", email);
                } catch (EntityNotFoundException e) {
                    user = new User();
                    user.setEmail(email);
                    user.setUsername(email);
                    user.setFirstName(firstName);
                    user.setLastName(lastName);
                    user.setActive(true);
                    user.setRegistrationDate(LocalDateTime.now());
                    user.setRank(rankService.getByRankName(Rank.JUNKIE));
                    user.setRole(roleService.getByRoleName(Role.PHOTO_JUNKIE));
                    user.setPassword(ENCODED_PASSWORD);
                    user = userRepository.create(user);
                }
                SecurityUser securityUser = new SecurityUser(user);
                Authentication contextAuthentication = new UsernamePasswordAuthenticationToken(securityUser, securityUser.getUsername(), securityUser.getGrantedAuthorities());
                SecurityContextHolder.getContext().setAuthentication(contextAuthentication);
                response.sendRedirect("/home");
            }
        }
    }
}
