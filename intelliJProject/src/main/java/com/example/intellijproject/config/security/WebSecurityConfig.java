package com.example.intellijproject.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableGlobalMethodSecurity(securedEnabled = true, prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final AuthenticationSuccessHandlerImpl authenticationSuccessHandler;
    private final WebSecurityUserService userService;

    @Autowired
    public WebSecurityConfig(WebSecurityUserService userService, AuthenticationSuccessHandlerImpl authenticationSuccessHandler) {
        this.userService = userService;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.userDetailsService(userService);
        http.authorizeRequests()
                .antMatchers("/api/users", "/api/passwords/**", "/auth/login", "/auth/logout", "/api/users/login"
                        , "/public/home", "/auth/register", "/users/**", "/api/users/activation", "/auth/**", "/swagger-ui/").permitAll()
                .anyRequest().authenticated()
                .and().formLogin().loginPage("/public/home").permitAll()
                .defaultSuccessUrl("/home").failureUrl("/public/home")
                .and().httpBasic()
                .and()
                .oauth2Login()
                .loginPage("/public/home")
                .successHandler(this.authenticationSuccessHandler)
                .and().logout().logoutUrl("/logout")
                .logoutSuccessUrl("/public/home").deleteCookies("JSESSIONID")
                .and().csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring().mvcMatchers("/styles/**", "/scripts/**", "/images/**", "/videos/**");
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }
}