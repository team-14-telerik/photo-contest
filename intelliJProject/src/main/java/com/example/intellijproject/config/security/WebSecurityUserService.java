package com.example.intellijproject.config.security;


import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class WebSecurityUserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Autowired
    public WebSecurityUserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        com.example.intellijproject.models.User user = userRepository.getByField("username", username);
        if (user == null || user.isDeleted()) {
            throw new UsernameNotFoundException("This username does not exist");
        }
        if (!user.isActive()) {
            throw new UnauthorizedOperationException("You need to activate your profile first");
        }
        return new SecurityUser(user);
    }
}
