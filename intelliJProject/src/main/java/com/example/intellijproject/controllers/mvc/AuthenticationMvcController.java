package com.example.intellijproject.controllers.mvc;

import com.example.intellijproject.controllers.rest.AuthenticationHelper;
import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.ChangePasswordDTO;
import com.example.intellijproject.models.dtos.ResetPasswordDTO;
import com.example.intellijproject.models.dtos.UserDTOIn;
import com.example.intellijproject.services.contracts.ResetPasswordService;
import com.example.intellijproject.services.contracts.TokenService;
import com.example.intellijproject.services.contracts.UserPhotoService;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;

@Controller
@RequestMapping("/auth")
public class AuthenticationMvcController {

    public static final String EMAIL_SUCCESS_MSG = "You will receive an email shortly at the address you provided.";
    public static final String PASSWORD_CONFIRMATION_ERR_MSG = "Password confirmation does not match password!";
    private final UserMapper userMapper;
    private final UserService userService;
    private final UserPhotoService userPhotoService;
    private final ResetPasswordService resetPasswordService;
    private final TokenService tokenService;


    @Autowired
    public AuthenticationMvcController(UserMapper userMapper,
                                       UserService userService, UserPhotoService userPhotoService,
                                       ResetPasswordService resetPasswordService, TokenService tokenService) {
        this.userMapper = userMapper;
        this.userService = userService;
        this.userPhotoService = userPhotoService;
        this.resetPasswordService = resetPasswordService;
        this.tokenService = tokenService;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userDTOIn", new UserDTOIn());
        model.addAttribute("sent", false);
        return "register";
    }

    @RequestMapping(path = "/register", method = RequestMethod.POST,
            consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public String createNewUser(@Valid @ModelAttribute("userDTOIn") UserDTOIn userDTOIn,
                                BindingResult bindingResult,
                                Model model) throws IOException {
        if (bindingResult.hasErrors()) {
            return "register";
        }

        if (!userDTOIn.getPassword().equals(userDTOIn.getPasswordConfirmation())) {
            bindingResult.rejectValue("passwordConfirmation", "password_error",
                    PASSWORD_CONFIRMATION_ERR_MSG);
        }

        User user = userMapper.createFromDto(userDTOIn);

        try {
            userService.create(user);
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("username", "username_error", e.getMessage());
            return "register";
        }

        if (userDTOIn.getMultipartPhoto() != null && !userDTOIn.getMultipartPhoto().isEmpty()) {
            user = userPhotoService.save(userDTOIn.getMultipartPhoto(), user.getId());
        }

        model.addAttribute("user", userMapper.UserToDTOOut(user));

        model.addAttribute("sent", true);
        return "register";
    }

    @GetMapping("/password-reset")
    public String showPasswordResetForm(@RequestParam(required = true) String token, Model model) {
        model.addAttribute("changePasswordDto", new ChangePasswordDTO());
        model.addAttribute("token", token);
        return "password-reset";
    }

    @PostMapping("/password-reset")
    public String showPasswordResetForm(@Valid @ModelAttribute("changePasswordDto") ChangePasswordDTO
                                                changePasswordDto, BindingResult errors, Model model) {
        ConfirmationToken confirmationToken = tokenService.findByToken(changePasswordDto.getToken());
        User user = confirmationToken.getUser();

        if (errors.hasErrors()) {
            model.addAttribute("user", user);
            model.addAttribute("token", confirmationToken.getToken());
            return "password-reset";
        }

        String newPassword = changePasswordDto.getNewPassword();
        String confirmedPassword = changePasswordDto.getConfirmedPassword();
        userService.changeUserPassword(confirmedPassword, newPassword, user);
        return "redirect:/public/home";
    }


    @GetMapping("/forgotten-password")
    public String showForgottenPasswordFrom(Model model) {
        model.addAttribute("emailDTO", new ResetPasswordDTO());
        model.addAttribute("successMsg", "");
        return "forgotten-password";
    }

    @PostMapping("/forgotten-password")
    public String requestForgottenPasswordToken(@ModelAttribute("emailDTO") ResetPasswordDTO resetPasswordDTO,
                                                Model model) {
        try {
            User user = userService.getByEmail(resetPasswordDTO.getEmail());
            resetPasswordService.generateResetToken(user);
            model.addAttribute("successMsg", EMAIL_SUCCESS_MSG);
            return "forgotten-password";
        } catch (EntityNotFoundException e) {
            model.addAttribute("successMsg", EMAIL_SUCCESS_MSG);
            return "forgotten-password";
        }
    }
}

