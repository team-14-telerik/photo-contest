package com.example.intellijproject.controllers.mvc;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.controllers.rest.AuthenticationHelper;
import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.*;
import com.example.intellijproject.models.dtos.ContestFilterDto;
import com.example.intellijproject.models.dtos.ContestMvcDTO;
import com.example.intellijproject.models.dtos.ContestPhotoMvcDTO;
import com.example.intellijproject.models.dtos.ReviewDTO;
import com.example.intellijproject.services.contracts.*;
import com.example.intellijproject.utils.ContestMapper;
import com.example.intellijproject.utils.ContestPhotoMapper;
import com.example.intellijproject.utils.NotificationMapper;
import com.example.intellijproject.utils.ReviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    public static final String[] STARS = {"1", "1", "1", "1", "1", "1", "1", "1", "1", "1"};
    public static final String INVITATIONAL_STATUS = "Invitational";
    public static final String FINISHED = "Finished";
    public static final String TWO = "Two";
    public static final String ONE = "One";
    private final ContestService contestService;
    private final ContestPhotoMapper contestPhotoMapper;
    private final AuthenticationHelper authenticationHelper;
    private final ContestPhotoService contestPhotoService;
    private final CategoryService categoryService;
    private final StatusService statusService;
    private final ContestMapper contestMapper;
    private final NotificationMapper notificationMapper;
    private final NotificationService notificationService;
    private final ReviewMapper reviewMapper;
    private final ReviewService reviewService;
    private final UserService userService;

    @Autowired
    public ContestMvcController(ContestService contestService, ContestPhotoMapper contestPhotoMapper,
                                AuthenticationHelper authenticationHelper, ContestPhotoService contestPhotoService,
                                CategoryService categoryService, StatusService statusService, ContestMapper contestMapper,
                                NotificationMapper notificationMapper, NotificationService notificationService,
                                ReviewMapper reviewMapper, ReviewService reviewService, UserService userService) {
        this.contestService = contestService;
        this.contestPhotoMapper = contestPhotoMapper;
        this.authenticationHelper = authenticationHelper;
        this.contestPhotoService = contestPhotoService;
        this.categoryService = categoryService;
        this.statusService = statusService;
        this.contestMapper = contestMapper;
        this.notificationMapper = notificationMapper;
        this.notificationService = notificationService;
        this.reviewMapper = reviewMapper;
        this.reviewService = reviewService;
        this.userService = userService;
    }

    @GetMapping("/{id}")
    public String showContestPage(@PathVariable int id, Model model) {
        try {
            SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
            User currentUser = authenticationHelper.tryGetUser(securityUser);
            String userPhoto = currentUser.getUserPhotoUrl();
            Contest contest = contestService.getById(id);
            List<ContestPhoto> allPhotos = contest.getContestPhotos();
            List<ContestPhoto> sortedPhotos = contestPhotoService.getAllByContestIdSorted(id);
            model.addAttribute("finished", contest.isFinished());
            model.addAttribute("phase", checkPhase(contest));
            model.addAttribute("contest", contest);
            model.addAttribute("allPhotos", allPhotos);
            model.addAttribute("sortedPhotos", sortedPhotos);
            model.addAttribute("userPhoto", userPhoto);
            model.addAttribute("votes", reviewService.getAllByContestId(id).size());
            model.addAttribute("finished", contest.isFinished());
            return "contest";
        } catch (EntityNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "404_not-found";
        }
    }

    private String checkPhase(Contest contest) {
        Date input = contest.getDeadlinePhaseOne();
        LocalDate date = LocalDate.ofInstant(input.toInstant(), ZoneId.systemDefault());
        if (contest.isFinished()) {
            return FINISHED;
        } else if (LocalDateTime.now().isBefore(contest.getDeadlinePhaseTwo()) &&
                LocalDate.now().isAfter(date)) {
            return TWO;
        } else return ONE;
    }

    @GetMapping
    public String showAllContests(Model model) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        String userPhoto = currentUser.getUserPhotoUrl();
        List<Contest> allContest = contestService.getAllFiltered(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty());
        model.addAttribute("userPhoto", userPhoto);
        model.addAttribute("contests", allContest);
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("contestFilterDto", new ContestFilterDto());
        model.addAttribute("title", "");
        model.addAttribute("categoryFilter", "");
        model.addAttribute("phase", "");
        model.addAttribute("sort", "");
        return "contests";
    }

    @PostMapping()
    public String filterAllContests(@Valid @ModelAttribute("contestFilterDto") ContestFilterDto contestFilterDto,
                                    BindingResult errors, Model model) throws IOException {
        List<Contest> filteredContests = contestService.getAllFiltered(
                Optional.ofNullable(contestFilterDto.getTitle().isBlank() ? null : contestFilterDto.getTitle()),
                Optional.ofNullable(contestFilterDto.getCategory() == null ? null : contestFilterDto.getCategory()),
                Optional.empty(),
                Optional.ofNullable(contestFilterDto.getPhase() == null ? null : contestFilterDto.getPhase()),
                Optional.ofNullable(contestFilterDto.getSort() == null ? null : contestFilterDto.getSort()),
                Optional.empty(),
                Optional.empty()
        );
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        String userPhoto = currentUser.getUserPhotoUrl();
        model.addAttribute("userPhoto", userPhoto);
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("contests", filteredContests);
        model.addAttribute("contestFilterDto", contestFilterDto);
        model.addAttribute("title", contestFilterDto.getTitle() == null ? "" : contestFilterDto.getTitle());
        model.addAttribute("categoryFilter", contestFilterDto.getCategory() == null ? "" : contestFilterDto.getCategory());
        model.addAttribute("phase", contestFilterDto.getPhase() == null ? "" : contestFilterDto.getPhase());
        model.addAttribute("sort", contestFilterDto.getSort() == null ? "" : contestFilterDto.getSort());
        return "contests";
    }

    @GetMapping("/creation-form")
    public String showContestCreationPage(Model model) {
        model.addAttribute("contestMvcDTO", new ContestMvcDTO());
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("statuses", statusService.getAll());
        model.addAttribute("contests", contestService.getAllFinished());
        return "contest-creation";
    }

    @PostMapping("/creation-form")
    public String uploadNewContest(@Valid @ModelAttribute("contestMvcDTO") ContestMvcDTO contestMvcDTO,
                                   BindingResult errors, Model model) throws IOException {
        if (errors.hasErrors()) {
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("statuses", statusService.getAll());
            model.addAttribute("contests", contestService.getAllFinished());
            return "contest-creation";
        }
        try {
            SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
            User user = authenticationHelper.tryGetUser(securityUser);
            Contest contest = contestMapper.mvcDTOToObject(contestMvcDTO);
            contestService.create(contest, user);
            List<Notification> juryNotifications = notificationMapper.createJuryNotifications(contest.getJuries(), contest);
            notificationService.createBulk(juryNotifications);

            if (contest.getStatus().getName().equals(INVITATIONAL_STATUS)) {
                Set<User> invitedUsers = findInvitedUsers(contestMvcDTO.getUsers());
                List<Notification> notifications = notificationMapper.createContestInvitationNotification(invitedUsers, contest);
                notificationService.createBulk(notifications);
            }
            return "redirect:/contests/" + contest.getId();
        } catch (DuplicateEntityException e) {
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("statuses", statusService.getAll());
            model.addAttribute("contests", contestService.getAllFinished());
            errors.rejectValue("title", "duplicate_Title", e.getMessage());
            return "contest-creation";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    private Set<User> findInvitedUsers(String users) {
        String[] usernames = users.split(";");
        Set<User> invitedUsers = new HashSet<>();
        for (String username : usernames) {
            User user = new User();
            try {
                user = userService.getByUsername(username);
                invitedUsers.add(user);
            } catch (EntityNotFoundException ignored) {

            }
        }
        return invitedUsers;
    }

    @GetMapping("/{contestId}/contestPhoto/{contestPhotoId}")
    public String showContestPhoto(@PathVariable int contestId, @PathVariable int contestPhotoId, Model model) {
        Contest contest = contestService.getById(contestId);
        ContestPhoto contestPhoto = contestPhotoService.getById(contestPhotoId);
        model.addAttribute("stars", STARS);
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        model.addAttribute("phaseForReview", checkReviewPhase(contest));
        model.addAttribute("isNotReviewed", contestPhotoService.isNotReviewed(contestPhoto, user));
        model.addAttribute("contestPhoto", contestPhoto);
        model.addAttribute("reviews", contestPhoto.getReviews());
        model.addAttribute("reviewDTO", new ReviewDTO());
        model.addAttribute("isLoggedUserJury", userService.isUserJury(contestId, user.getId()));
        model.addAttribute("isLoggedUserOwner", contestPhoto.getUser().getId() == user.getId());
        return "contest-photo";
    }

    public Boolean checkReviewPhase(Contest contest) {
        Date input = contest.getDeadlinePhaseOne();
        LocalDate date = LocalDate.ofInstant(input.toInstant(), ZoneId.systemDefault());
        return LocalDateTime.now().isBefore(contest.getDeadlinePhaseTwo()) &&
                (LocalDate.now().isAfter(date) || LocalDate.now().isEqual(date));
    }

    @PostMapping("/{contestId}/contestPhoto/{contestPhotoId}")
    public String submitReview(@Valid @ModelAttribute("reviewDTO") ReviewDTO reviewDTO,
                               BindingResult errors, @PathVariable int contestId, @PathVariable int contestPhotoId,
                               Model model) {
        Contest contest = contestService.getById(contestId);
        ContestPhoto contestPhoto = contestPhotoService.getById(contestPhotoId);
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Set<Review> reviews = contestPhoto.getReviews();
        if (errors.hasErrors()) {
            model.addAttribute("contestPhoto", contestPhoto);
            model.addAttribute("reviews", reviews);
            model.addAttribute("phaseForReview", checkReviewPhase(contest));
            model.addAttribute("isNotReviewed", contestPhotoService.isNotReviewed(contestPhoto, user));
            model.addAttribute("reviewDTO", new ReviewDTO());
            model.addAttribute("stars", STARS);
            model.addAttribute("isLoggedUserJury", userService.isUserJury(contestId, user.getId()));
            model.addAttribute("isLoggedUserOwner", contestPhoto.getUser().getId() == user.getId());
            return "contest-photo";
        }
        Review review = reviewMapper.fromDto(reviewDTO, user, contestPhotoId);
        reviewService.create(review, user);
        reviews.add(review);
        model.addAttribute("contestPhoto", contestPhoto);
        model.addAttribute("stars", STARS);
        model.addAttribute("reviews", reviews);
        model.addAttribute("phaseForReview", checkReviewPhase(contest));
        model.addAttribute("isNotReviewed", contestPhotoService.isNotReviewed(contestPhoto, user));
        model.addAttribute("reviewDTO", new ReviewDTO());
        model.addAttribute("isLoggedUserJury", userService.isUserJury(contestId, user.getId()));
        model.addAttribute("isLoggedUserOwner", contestPhoto.getUser().getId() == user.getId());
        return "contest-photo";
    }

    @GetMapping("/{contestId}/contestPhotoForm")
    public String showUploadContestPhotoPage(@PathVariable int contestId, Model model) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Contest contest = contestService.getById(contestId);
        if (didUserAlreadyUpload(contestId, user.getId())) {
            return "redirect:/contests/" + contestId;
        }
        model.addAttribute("contest", contest);
        model.addAttribute("contestPhotoMvcDTO", new ContestPhotoMvcDTO());
        return "contest-photo-upload";
    }

    @PostMapping("/{contestId}/contestPhotoForm")
    public String UploadContestPhoto(@PathVariable int contestId, @Valid @ModelAttribute("contestPhotoMvcDTO")
            ContestPhotoMvcDTO contestPhotoMvcDTO, BindingResult errors, Model model) throws IOException {
        Contest contest = contestService.getById(contestId);
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);

        if (contestPhotoMvcDTO.getMultipartPhoto().isEmpty()) {
            model.addAttribute("contest", contest);
            errors.rejectValue("multipartPhoto", "404", "You must upload a photo!");
            return "contest-photo-upload";
        }
        if (errors.hasErrors()) {
            model.addAttribute("contest", contest);
            return "contest-photo-upload";
        }

        try {
            String photoUrl = contestPhotoService.savePhoto(contestPhotoMvcDTO.getMultipartPhoto(), contestId, user);
            ContestPhoto contestPhoto = contestPhotoMapper.dtoToObject(contestPhotoMvcDTO, contestId, user, photoUrl);
            contestPhotoService.create(contestPhoto, user);
            return "redirect:/contests/{contestId}/contestPhoto/" + contestPhoto.getId();
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "access-denied";
        }
    }

    private boolean didUserAlreadyUpload(int contestId, int userId) {
        for (ContestPhoto photo : contestPhotoService.getAllByContestId(contestId)) {
            if (photo.getUser().getId() == userId) {
                return true;
            }
        }

        return false;
    }

    @GetMapping("/{contestId}/contestPhoto/{contestPhotoId}/delete")
    public String deleteContestPhoto(@PathVariable int contestId, @PathVariable int contestPhotoId) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        contestPhotoService.delete(contestId, contestPhotoId, user);
        return "redirect:/contests/" + contestId;
    }

    @ModelAttribute("loggedUser")
    public User loadCurrentUser() {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        return authenticationHelper.tryGetUser(securityUser);
    }
}
