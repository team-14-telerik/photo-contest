package com.example.intellijproject.controllers.mvc;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.controllers.rest.AuthenticationHelper;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.LoginUserDto;
import com.example.intellijproject.services.contracts.ContestPhotoService;
import com.example.intellijproject.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class HomeMvcController {

    private final ContestService contestService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestPhotoService contestPhotoService;

    @Autowired
    public HomeMvcController(ContestService contestService, AuthenticationHelper authenticationHelper, ContestPhotoService contestPhotoService) {
        this.contestService = contestService;
        this.authenticationHelper = authenticationHelper;
        this.contestPhotoService = contestPhotoService;
    }

    @GetMapping()
    public String redirectToHome() {
        return "redirect:/home";
    }

    @GetMapping("/public/home")
    public String showPublicHomePage(Model model) {
        List<Contest> newestContests = contestService.getTenNewest();
        List<Contest> finishedContests = contestService.getAllFinished();
        model.addAttribute("finishedContests", finishedContests);
        model.addAttribute("newestContests", newestContests);
        model.addAttribute("loginUserDto", new LoginUserDto());
        model.addAttribute("auth", false);
        return "index";
    }

    @GetMapping("/home")
    public String showAuthHomePage(Model model) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        String userPhoto = currentUser.getUserPhotoUrl();
        List<Contest> newestContests = contestService.getAllPhaseOne();
        List<Contest> contestFinished = contestService.getAllFinished();
        List<ContestPhoto> popularPhotos = contestPhotoService.getEightMostPopular();
        model.addAttribute("loginUserDto", new LoginUserDto());
        model.addAttribute("auth", true);
        model.addAttribute("loggedUserId", currentUser.getId());
        model.addAttribute("userPhoto", userPhoto);
        model.addAttribute("newestContests", newestContests);
        model.addAttribute("finishedContests", contestFinished);
        model.addAttribute("isOrganizerDashboard", false);
        model.addAttribute("popularPhotos", popularPhotos);
        model.addAttribute("loggedUser", currentUser);
        model.addAttribute("loggedUserRole", currentUser.getRole().getId());
        return "index";
    }
}
