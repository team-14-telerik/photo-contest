package com.example.intellijproject.controllers.mvc;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.controllers.rest.AuthenticationHelper;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.Notification;
import com.example.intellijproject.models.User;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.services.contracts.NotificationService;
import com.example.intellijproject.services.contracts.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/organizers")
public class OrganizerMvcController {
    public static final String PHASE_ONE = "Phase One";
    public static final String PHASE_TWO = "Phase Two";
    public static final String FINISHED = "Finished";
    private final ContestService contestService;
    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final NotificationService notificationService;

    public OrganizerMvcController(ContestService contestService, UserService userService, AuthenticationHelper authenticationHelper, NotificationService notificationService) {
        this.contestService = contestService;
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.notificationService = notificationService;
    }

    @GetMapping("/dashboard")
    public String showOrganizersDashboard(Model model) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User loggedUser = authenticationHelper.tryGetUser(securityUser);
        String userPhoto = loggedUser.getUserPhotoUrl();
        List<Notification> notifications = notificationService.getAllActiveUserNotifications(loggedUser.getId());
        List<Contest> contestPhaseOne = contestService.getAllPhaseOne();
        List<Contest> contestPhaseTwo = contestService.getAllPhaseTwo();
        List<Contest> contestFinished = contestService.getAllFinished();
        List<User> users = userService.getAll(Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(), Optional.of("score"), Optional.of("desc"));
        model.addAttribute("contestsPhaseOne", contestPhaseOne);
        model.addAttribute("contestsPhaseTwo", contestPhaseTwo);
        model.addAttribute("contestsFinished", contestFinished);
        model.addAttribute("sliderTitle1", PHASE_ONE);
        model.addAttribute("sliderTitle2", PHASE_TWO);
        model.addAttribute("sliderTitle3", FINISHED);
        model.addAttribute("isOrganizerDashboard", true);
        model.addAttribute("users", users);
        model.addAttribute("loggedUserId", loggedUser.getId());
        model.addAttribute("loggedUser", loggedUser);
        model.addAttribute("userPhoto", userPhoto);
        model.addAttribute("notifications", notifications);
        return "dashboard-organizers";
    }

    @ModelAttribute("loggedUser")
    public User loadCurrentUser() {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        return authenticationHelper.tryGetUser(securityUser);
    }
}
