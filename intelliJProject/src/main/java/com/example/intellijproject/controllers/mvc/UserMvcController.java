package com.example.intellijproject.controllers.mvc;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.controllers.rest.AuthenticationHelper;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.Notification;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.RoleDTO;
import com.example.intellijproject.models.dtos.UsersFilterDTO;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.services.contracts.NotificationService;
import com.example.intellijproject.services.contracts.RoleService;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.UserMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/users")
public class UserMvcController {
    public static final String OPEN = "Open";
    public static final String MY_ACTIVE_CONTESTS = "My active contests";
    public static final String MY_FINISHED_CONTESTS = "My finished contests";
    public static final String FOR_REVIEW = "For Review";
    private final UserService userService;
    private final ContestService contestService;
    private final UserMapper userMapper;
    private final AuthenticationHelper authenticationHelper;
    private final RoleService roleService;
    private final NotificationService notificationService;

    public UserMvcController(UserService userService, ContestService contestService, UserMapper userMapper,
                             AuthenticationHelper authenticationHelper, RoleService roleService, NotificationService notificationService) {
        this.userService = userService;
        this.contestService = contestService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
        this.roleService = roleService;
        this.notificationService = notificationService;
    }

    @GetMapping("/{id}")
    public String showUserProfile(@PathVariable int id, Model model) {
        try {
            User user = userService.getById(id);
            model.addAttribute("user", user);
            model.addAttribute("myContests", contestService.getAllFinishedByUserId(id));
            return "profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("errorMessage", e.getMessage());
            return "404_not-found";
        }
    }

    @GetMapping("/dashboard")
    public String showUserDashboard(Model model) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        List<Contest> contestOpen = contestService.getAllOpen();
        List<Contest> contestCurrentlyPart = contestService.getAllByUserId(currentUser.getId());
        List<Contest> contestFinished = contestService.getAllFinishedByUserId(currentUser.getId());
        List<Notification> notifications = notificationService.getAllActiveUserNotifications(currentUser.getId());
        String userPhoto = currentUser.getUserPhotoUrl();
        List<User> users = userService.getAll(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.of("score"),
                Optional.of("desc")
        );
        List<Contest> juriedContests = contestService.getAllJuriedByUserId(currentUser.getId());
        model.addAttribute("contestsPhaseOne", contestOpen);
        model.addAttribute("contestsPhaseTwo", contestCurrentlyPart);
        model.addAttribute("juriedContests", juriedContests);
        model.addAttribute("loggedUserId", currentUser.getId());
        model.addAttribute("loggedUser", currentUser);
        model.addAttribute("userPhoto", userPhoto);
        model.addAttribute("contestsFinished", contestFinished);
        model.addAttribute("isOrganizerDashboard", false);
        model.addAttribute("users", users);
        model.addAttribute("notifications", notifications);
        model.addAttribute("sliderTitle1", OPEN);
        model.addAttribute("sliderTitle2", MY_ACTIVE_CONTESTS);
        model.addAttribute("sliderTitle3", MY_FINISHED_CONTESTS);
        model.addAttribute("sliderTitle4", FOR_REVIEW);
        model.addAttribute("userPhoto", userPhoto);
        return "dashboard-users";
    }

    @GetMapping()
    public String showUsersPage(Model model) {

        List<User> userList = userService.getAll(Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(),
                Optional.of("id"), Optional.empty()
        );
        model.addAttribute("users", userList);
        model.addAttribute("filterDTO", new UsersFilterDTO());
        return "users";
    }

    @PostMapping()
    public String showUsersPageFiltered(@ModelAttribute("filterDTO") UsersFilterDTO filterDTO, Model model) {
        List<User> userList = userService.getAll(
                Optional.ofNullable(filterDTO.getUsername().isEmpty() ? null : filterDTO.getUsername()),
                Optional.ofNullable(filterDTO.getFirstName().isEmpty() ? null : filterDTO.getFirstName()),
                Optional.ofNullable(filterDTO.getLastName().isEmpty() ? null : filterDTO.getLastName()),
                Optional.ofNullable(filterDTO.getEmail().isEmpty() ? null : filterDTO.getEmail()),
                Optional.ofNullable(filterDTO.getUserRole().isEmpty() ? null : filterDTO.getUserRole()),
                Optional.ofNullable(filterDTO.getUserRank().isEmpty() ? null : filterDTO.getUserRank()),
                Optional.of(filterDTO.getMinScore() == null ? 0 : filterDTO.getMinScore()),
                Optional.of(filterDTO.getMaxScore() == null ? 3000 : filterDTO.getMaxScore()),
                Optional.of(filterDTO.getSortBy()),
                Optional.of(filterDTO.getSortOrder())
        );
        model.addAttribute("users", userList);
        model.addAttribute("filterDTO", filterDTO);
        return "users";
    }


    @ExceptionHandler(MissingServletRequestParameterException.class)
    public String handleMissingParams(MissingServletRequestParameterException ex, Model model) {
        String name = ex.getParameterName();
        model.addAttribute("errorMessage", "not found");
        return "404_not-found";
    }

    @ExceptionHandler(ClassCastException.class)
    public String handleNotAuthorized() {
        return "redirect:/public/home";
    }


    @GetMapping("/{id}/block")
    public String blockUser(@PathVariable int id, Model model) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        userService.updateUserBlockStatus(user, id);
        return "redirect:/users/" + id;
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        userService.delete(user, id);
        return "redirect:/users";
    }

    @PostMapping("/{id}/roles")
    public String changeUserRole(@Valid RoleDTO roleDTO, @PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Role role = roleService.getByRoleName(roleDTO.getRoleName());
        userService.updateRole(user, id, role);
        return "redirect:/users/" + id;
    }

    @ModelAttribute("loggedUser")
    public User loadCurrentUser() {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        return authenticationHelper.tryGetUser(securityUser);
    }
}
