package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.UserDTOOUTAdmin;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/admins")
public class AdminController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;

    @Autowired
    public AdminController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
    }

    @PostMapping("/{id}")
    public UserDTOOUTAdmin makeAdmin(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        return userMapper.UserToDTOOutAdmin(userService.makeAdmin(currentUser, id));
    }

    @DeleteMapping("/{id}")
    public UserDTOOUTAdmin removeAdminPrivileges(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        return userMapper.UserToDTOOutAdmin(userService.demoteAdmin(currentUser, id));
    }

}
