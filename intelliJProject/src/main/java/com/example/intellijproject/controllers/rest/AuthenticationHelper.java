package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.models.User;
import com.example.intellijproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ResponseStatusException;


@Component
public class AuthenticationHelper {

    public static final String UNAUTHORISED_NOT_LOGGED = "You have to be logged in order to access the requested resource!";
    public static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    public static final String AUTHENTICATION_FAILURE_MESSAGE = "Wrong username or password.";
    public static final String REGISTERED_USER_ERR_MSG = "User is logged in and already registered. Registration resource is available only to unregistered users. Please log out to create a new account!";
    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(SecurityUser securityUser) {
        return userService.getByUsername(securityUser.getUsername());
    }


    public void validateUserIsNotLoggedIn(SecurityUser securityUser) {
        if (securityUser == null) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN,
                    REGISTERED_USER_ERR_MSG);
        }
    }
}