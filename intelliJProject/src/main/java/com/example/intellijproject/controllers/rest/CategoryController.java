package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.models.Category;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.CategoryDTO;
import com.example.intellijproject.services.contracts.CategoryService;
import com.example.intellijproject.utils.CategoryMapper;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/categories")
public class CategoryController {
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;
    private final AuthenticationHelper authenticationHelper;

    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper, AuthenticationHelper authenticationHelper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping
    public List<Category> getAll() {
        return categoryService.getAll();
    }

    @GetMapping("/{id}")
    public Category getById(@PathVariable int id) {
        return categoryService.getById(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody CategoryDTO categoryDTO) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Category category = categoryMapper.fromDto(categoryDTO);
        categoryService.create(category, user);
    }

    @PutMapping("/{id}")
    public Category update(@RequestHeader HttpHeaders headers, @PathVariable int id,
                           @Valid @RequestBody CategoryDTO categoryDTO) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Category category = categoryMapper.fromDto(categoryDTO, id);
        categoryService.update(category, user);
        return category;
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        categoryService.delete(id, user);
    }
}
