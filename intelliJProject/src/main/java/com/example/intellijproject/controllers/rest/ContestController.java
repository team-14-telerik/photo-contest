package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.Review;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.ContestDTO;
import com.example.intellijproject.models.dtos.ContestPhotoDTO;
import com.example.intellijproject.models.dtos.ReviewDTO;
import com.example.intellijproject.models.dtos.UpdateReviewDTO;
import com.example.intellijproject.services.contracts.ContestPhotoService;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.services.contracts.ReviewService;
import com.example.intellijproject.utils.ContestMapper;
import com.example.intellijproject.utils.ContestPhotoMapper;
import com.example.intellijproject.utils.ReviewMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/contests")
public class ContestController {

    private final ContestService contestService;
    private final ContestPhotoService contestPhotoService;
    private final ContestMapper contestMapper;
    private final ContestPhotoMapper contestPhotoMapper;
    private final ReviewMapper reviewMapper;
    private final ReviewService reviewService;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ContestController(ContestService contestService, ContestPhotoService contestPhotoService, ContestMapper contestMapper, ContestPhotoMapper contestPhotoMapper, ReviewMapper reviewMapper, ReviewService reviewService, AuthenticationHelper authenticationHelper) {
        this.contestService = contestService;
        this.contestPhotoService = contestPhotoService;
        this.contestMapper = contestMapper;
        this.contestPhotoMapper = contestPhotoMapper;
        this.reviewMapper = reviewMapper;
        this.reviewService = reviewService;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping()
    public List<Contest> getAllFiltered(@RequestParam(required = false) Optional<String> title,
                                        @RequestParam(required = false) Optional<String> category,
                                        @RequestParam(required = false) Optional<String> status,
                                        @RequestParam(required = false) Optional<String> phase,
                                        @RequestParam(required = false) Optional<String> sort,
                                        @RequestParam(required = false) Optional<String> finished,
                                        @RequestParam(required = false) Optional<String> username) {
        return contestService.getAllFiltered(title, category, status, phase, sort, finished, username);
    }

    @GetMapping("/contest-photos/filtered")
    public List<ContestPhoto> getAllContestPhotosFiltered(@RequestParam(required = false) Optional<String> title) {
        return contestPhotoService.getAllFiltered(title);
    }

    @GetMapping("/{id}")
    public Contest getById(@PathVariable int id) {
        return contestService.getById(id);
    }

    @GetMapping("/{contestId}/photos/{photoId}")
    public ContestPhoto getContestPhotoById(@PathVariable int contestId, @PathVariable int photoId) {
        return contestPhotoService.getById(photoId);
    }

    @GetMapping("/{contestId}/photos/{photoId}/reviews/{reviewId}")
    public Review getReviewById(@PathVariable int contestId, @PathVariable int photoId, @PathVariable int reviewId) {
        return reviewService.getById(reviewId);
    }


    @GetMapping("/phase-one")
    public List<Contest> getAllPhaseOne() {
        return contestService.getAllPhaseOne();
    }

    @GetMapping("/phase-two")
    public List<Contest> getAllPhaseTwo() {
        return contestService.getAllPhaseTwo();
    }

    @GetMapping("/finished")
    public List<Contest> getAllFinished() {
        return contestService.getAllFinished();
    }

    @GetMapping("/open")
    public List<Contest> getAllOpen() {
        return contestService.getAllOpen();
    }

    @GetMapping("/participants/{id}")
    public List<Contest> getAllActiveByUserId(@PathVariable int id) {
        return contestService.getAllByUserId(id);
    }

    @GetMapping("/finished/participants/{id}")
    public List<Contest> getAllFinishedByUserId(@PathVariable int id) {
        return contestService.getAllFinishedByUserId(id);
    }

    @PostMapping
    public void create(@Valid @RequestBody ContestDTO contestDTO) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Contest contest = contestMapper.dtoToObject(contestDTO);
        contestService.create(contest, user);
    }


    @PostMapping("/{id}/photos")
    public void submitContestPhoto(@Valid @RequestBody ContestPhotoDTO contestPhotoDTO,
                                   @PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        ContestPhoto contestPhoto = contestPhotoMapper.dtoToObject(contestPhotoDTO, id, user);
        contestPhotoService.create(contestPhoto, user);
    }

    @DeleteMapping("/{contestId}/photos/{photoId}")
    public void deleteContestPhoto(@PathVariable int contestId,
                                   @PathVariable int photoId) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        contestPhotoService.delete(contestId, photoId, user);
    }

    @PostMapping("/{contestId}/photos/{photoId}/reviews")
    public void createPhotoReview(@Valid @RequestBody ReviewDTO reviewDTO,
                                  @PathVariable int contestId, @PathVariable int photoId) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Review review = reviewMapper.fromDto(reviewDTO, user, photoId);
        reviewService.create(review, user);
    }

    @PutMapping("/{contestId}/photos/{photoId}/reviews/{reviewId}")
    public Review updatePhotoReview(@Valid @RequestBody UpdateReviewDTO updateReviewDTO,
                                    @PathVariable int contestId, @PathVariable int photoId, @PathVariable int reviewId) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Review review = reviewMapper.fromDto(reviewId, updateReviewDTO);
        reviewService.update(review, user);
        return review;
    }

    @DeleteMapping("/{contestId}/photos/{photoId}/reviews/{reviewId}")
    public void deletePhotoReview(@PathVariable int contestId, @PathVariable int photoId, @PathVariable int reviewId) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        reviewService.delete(reviewId, user);
    }

    @PutMapping("/{id}")
    public Contest update(@PathVariable int id, @Valid @RequestBody ContestDTO contestDTO) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        Contest contest = contestMapper.dtoToObject(contestDTO, id);
        contestService.update(contest, user);
        return contest;
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        contestService.delete(id, user);
    }
}
