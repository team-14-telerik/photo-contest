package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.models.Notification;
import com.example.intellijproject.services.contracts.NotificationService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/notifications")
public class NotificationController {
    private final NotificationService notificationService;

    public NotificationController(NotificationService notificationService) {
        this.notificationService = notificationService;
    }

    @PutMapping("/{id}")
    public Notification updateNotification(@PathVariable int id) {
        Notification notification = notificationService.getById(id);
        notification.setRead(true);
        notificationService.update(notification);
        return notification;
    }
}
