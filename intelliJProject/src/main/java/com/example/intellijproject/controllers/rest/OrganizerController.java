package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.UserDTOOut;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.UserMapper;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/api/organizers")
public class OrganizerController {

    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final UserMapper userMapper;


    public OrganizerController(AuthenticationHelper authenticationHelper, UserService userService,
                               UserMapper userMapper) {
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.userMapper = userMapper;
    }


    @PostMapping("/{id}")
    public UserDTOOut makeOrganizer(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        return userMapper.UserToDTOOut(userService.makeOrganizer(currentUser, id));
    }


    @DeleteMapping("/{id}")
    public UserDTOOut demoteOrganizer(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User user = authenticationHelper.tryGetUser(securityUser);
        return userMapper.UserToDTOOut(userService.demoteOrganizer(user, id));
    }
}



