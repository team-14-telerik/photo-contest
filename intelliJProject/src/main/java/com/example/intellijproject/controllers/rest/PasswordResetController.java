package com.example.intellijproject.controllers.rest;

import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.ChangePasswordDTO;
import com.example.intellijproject.models.dtos.ResetPasswordDTO;
import com.example.intellijproject.services.contracts.ResetPasswordService;
import com.example.intellijproject.services.contracts.TokenService;
import com.example.intellijproject.services.contracts.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequestMapping("/api/passwords")
public class PasswordResetController {

    public static final String EMAIL_SENT_MSG = "Email sent. Check your inbox.";
    public static final String PASSWORD_UPADATE_MSG = "Your password was successfully changed";
    private final UserService userService;
    private final ResetPasswordService resetPasswordService;
    private final TokenService tokenService;

    public PasswordResetController(UserService userService, ResetPasswordService resetPasswordService, TokenService tokenService) {
        this.userService = userService;
        this.resetPasswordService = resetPasswordService;
        this.tokenService = tokenService;
    }

    @PostMapping("/forgot_password")
    public String resetPassword(@RequestBody ResetPasswordDTO resetPasswordDTO) {
        User user = userService.getByEmail(resetPasswordDTO.getEmail());
        resetPasswordService.generateResetToken(user);
        return EMAIL_SENT_MSG;
    }

    @PostMapping("/reset_password")
    public String changePassword(@RequestBody ChangePasswordDTO changePasswordDTO) {
        ConfirmationToken confirmationToken = tokenService.findByToken(changePasswordDTO.getToken());
        confirmationToken.setConfirmedAt(LocalDateTime.now());
        tokenService.updateConfirmationToken(confirmationToken);
        User user = confirmationToken.getUser();
        String newPassword = changePasswordDTO.getNewPassword();
        String confirmedPassword = changePasswordDTO.getConfirmedPassword();
        userService.changeUserPassword(confirmedPassword, newPassword, user);
        return PASSWORD_UPADATE_MSG;
    }
}
