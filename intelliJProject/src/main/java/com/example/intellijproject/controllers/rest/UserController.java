package com.example.intellijproject.controllers.rest;


import com.example.intellijproject.config.security.models.SecurityUser;
import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.*;
import com.example.intellijproject.services.contracts.TokenService;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;
    private final AuthenticationHelper authenticationHelper;
    private final UserMapper userMapper;
    private final TokenService tokenService;

    @Autowired
    public UserController(UserService userService, AuthenticationHelper authenticationHelper, UserMapper userMapper, TokenService tokenService) {
        this.userService = userService;
        this.authenticationHelper = authenticationHelper;
        this.userMapper = userMapper;
        this.tokenService = tokenService;
    }

    @GetMapping()
    public List<UserDTOOutInterface> getAllUsers(
            @RequestParam(required = false) Optional<String> username,
            @RequestParam(required = false) Optional<String> firstname,
            @RequestParam(required = false) Optional<String> lastname,
            @RequestParam(required = false) Optional<String> email,
            @RequestParam(required = false) Optional<String> userRole,
            @RequestParam(required = false) Optional<String> userRank,
            @RequestParam(required = false) Optional<Integer> minScore,
            @RequestParam(required = false) Optional<Integer> maxScore,
            @RequestParam(required = false) Optional<String> sortBy,
            @RequestParam(required = false) Optional<String> sortOrder) {
        List<User> result;
        try {
            result = userService.getAll(username, firstname, lastname, email, userRole, userRank, minScore, maxScore, sortBy, sortOrder);
        } catch (InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
        return result.stream().map(userMapper::UserToDTOOut).collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    public UserDTOOutInterface getUserByID(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User requester = authenticationHelper.tryGetUser(securityUser);
        User userById;
        try {
            userById = userService.getById(id);
        } catch (InvalidParameterException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

        if (requester.getId() == id) {
            return userMapper.UserToDTOOutOwner(userById);
        } else {
            return userMapper.UserToDTOOut(userById);
        }
    }

    @PostMapping()
    public UserDTOOutInterface createUser(@Valid @RequestBody UserDTOIn userDTOIn) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        authenticationHelper.validateUserIsNotLoggedIn(securityUser);

        User userObject = userMapper.createFromDto(userDTOIn);
        try {
            return userMapper.UserToDTOOutOwner(userService.create(userObject));
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public UserDTOOutOwner update(@PathVariable int id, @Valid @RequestBody UserDTOIn userDTOIn) {

        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        User user = userMapper.updateFromDto(userDTOIn, id);

        try {
            return userMapper.UserToDTOOutOwner(userService.update(id, user, currentUser));
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        userService.delete(currentUser, id);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PutMapping("/{id}/block")
    public UserDTOOUTAdmin blockUser(@PathVariable int id) {
        SecurityUser securityUser = SecurityUser.getCurrentlyLoggedUser();
        User currentUser = authenticationHelper.tryGetUser(securityUser);
        return userMapper.UserToDTOOutAdmin(userService.updateUserBlockStatus(currentUser, id));
    }

    @GetMapping("/admins")
    public List<UserDTOOut> showAdmins() {
        return userService.getAllAdmins().stream().map(userMapper::UserToDTOOut).collect(Collectors.toList());
    }

    @GetMapping("/organizers")
    public List<UserDTOOut> showOrganizers() {
        return userService.getAllOrganizers().stream().map(userMapper::UserToDTOOut).collect(Collectors.toList());
    }

    @GetMapping("/photo_junkies")
    public List<UserDTOOut> showAllJunkies() {
        return userService.getAllJunkies().stream().map(userMapper::UserToDTOOut).collect(Collectors.toList());
    }

    @GetMapping("/topJunkies")
    public List<UserDTOOut> getTopJunkies(@RequestParam(required = false) Optional<Integer> count) {
        return userService.getTopJunkies(count).stream().map(userMapper::UserToDTOOut).collect(Collectors.toList());
    }

    @PostMapping("/activation")
    public void activateUserProfile(@RequestParam String activationToken) {
        try {
            ConfirmationToken confirmationToken = tokenService.findByToken(activationToken);
            confirmationToken.setConfirmedAt(LocalDateTime.now());
            tokenService.updateConfirmationToken(confirmationToken);
            User user = confirmationToken.getUser();
            user.setActive(true);
            userService.update(user.getId(), user, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/login")
    public User login(@RequestBody LoginUserDto loginUserDto) {
        User user = userService.getByUsername(loginUserDto.getUsername());
        String password = loginUserDto.getPassword();

        return userService.login(user, password);
    }


}
