package com.example.intellijproject.exceptions;

public class WrongDateException extends RuntimeException {

    public WrongDateException(String message) {
        super(message);
    }
}
