package com.example.intellijproject.exceptions.config;

import com.example.intellijproject.exceptions.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice("com.example.intellijproject.controllers.rest")
public class GlobalExceptionHandler {

    @ExceptionHandler(value = {EntityNotFoundException.class})
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException e) {
        HttpStatus notFound = HttpStatus.NOT_FOUND;
        GlobalException apiException = new GlobalException(e.getMessage(), notFound);

        return new ResponseEntity<>(apiException, notFound);
    }

    @ExceptionHandler(value = {AuthenticationFailureException.class})
    public ResponseEntity<Object> handleAuthenticationFailure(AuthenticationFailureException e) {
        HttpStatus notFound = HttpStatus.UNAUTHORIZED;
        GlobalException apiException = new GlobalException(e.getMessage(), notFound);

        return new ResponseEntity<>(apiException, notFound);
    }

    @ExceptionHandler(value = {DuplicateEntityException.class})
    public ResponseEntity<Object> handleDuplicateEntityException(DuplicateEntityException e) {
        HttpStatus duplicate = HttpStatus.CONFLICT;
        GlobalException apiException = new GlobalException(e.getMessage(), duplicate);

        return new ResponseEntity<>(apiException, duplicate);
    }

    @ExceptionHandler(value = {WrongDateException.class})
    public ResponseEntity<Object> handleWrongDateException(WrongDateException e) {
        HttpStatus invalid = HttpStatus.BAD_REQUEST;
        GlobalException apiException = new GlobalException(e.getMessage(), invalid);

        return new ResponseEntity<>(apiException, invalid);
    }

    @ExceptionHandler(value = {UnauthorizedOperationException.class})
    public ResponseEntity<Object> handleUnauthorizedOperationException(UnauthorizedOperationException e) {
        HttpStatus invalid = HttpStatus.UNAUTHORIZED;
        GlobalException apiException = new GlobalException(e.getMessage(), invalid);

        return new ResponseEntity<>(apiException, invalid);
    }


}
