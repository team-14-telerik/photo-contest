package com.example.intellijproject.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.text.DateFormatSymbols;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "contests")
public class Contest {
    public static final String SPACE = " ";
    public static final String COMMA = ",";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "title")
    private String title;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @ManyToOne
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "deadline_phase_1")
    private Date deadlinePhaseOne;

    @Column(name = "deadline_phase_2")
    private LocalDateTime deadlinePhaseTwo;

    @Column(name = "photo_url")
    private String photoUrl;

    private boolean finished;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "jury",
            joinColumns = @JoinColumn(name = "contest_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<User> juries;

    @JsonIgnore
    @OneToMany(mappedBy = "contest", fetch = FetchType.EAGER)
    private List<ContestPhoto> contestPhotos;

    public Contest() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getDeadlinePhaseOne() {
        return deadlinePhaseOne;
    }

    public void setDeadlinePhaseOne(Date deadlinePhaseOne) {
        this.deadlinePhaseOne = deadlinePhaseOne;
    }

    public LocalDateTime getDeadlinePhaseTwo() {
        return deadlinePhaseTwo;
    }

    public void setDeadlinePhaseTwo(LocalDateTime deadlinePhaseTwo) {
        this.deadlinePhaseTwo = deadlinePhaseTwo;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public Set<User> getJuries() {
        return juries;
    }

    public void setJuries(Set<User> juries) {
        this.juries = juries;
    }

    public boolean isFinished() {
        return finished;
    }

    public void setFinished(boolean finished) {
        this.finished = finished;
    }

    public List<ContestPhoto> getContestPhotos() {
        contestPhotos.sort(Collections.reverseOrder());
        return contestPhotos;
    }

    public void setContestPhotos(List<ContestPhoto> contestPhotos) {
        this.contestPhotos = contestPhotos;
    }

    @Transient
    public String getEndDateFormatted() {
        StringBuilder builder = new StringBuilder();
        String monthName = getMonthForInt(deadlinePhaseTwo.getMonthValue());
        int day = deadlinePhaseTwo.getDayOfMonth();
        int year = deadlinePhaseTwo.getYear();
        builder.append(monthName).append(SPACE).append(day).append(COMMA).append(SPACE).append(year);
        return builder.toString();
    }

    @Transient
    public String getStartDateFormatted() {
        StringBuilder builder = new StringBuilder();
        String monthName = getMonthForInt(deadlinePhaseOne.getMonth());
        SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
        SimpleDateFormat formatDay = new SimpleDateFormat("dd");
        String day = formatDay.format(deadlinePhaseOne);
        String year = formatYear.format(deadlinePhaseOne);
        builder.append(monthName).append(SPACE).append(day).append(COMMA).append(SPACE).append(year);
        return builder.toString();
    }


    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }
}
