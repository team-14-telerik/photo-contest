package com.example.intellijproject.models;

import com.example.intellijproject.models.contracts.Userable;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "contests_photos")
public class ContestPhoto implements Userable, Comparable<ContestPhoto> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "contest_id")
    private Contest contest;

    @Column(name = "photo_url")
    private String photoUrl;

    @Column(name = "title")
    private String title;

    @Column(name = "story")
    private String story;

    @JsonIgnore
    @OneToMany(mappedBy = "contestPhoto", fetch = FetchType.EAGER)
    private Set<Review> reviews;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public ContestPhoto() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Review> getReviews() {
        return reviews;
    }

    public void setReviews(Set<Review> reviews) {
        this.reviews = reviews;
    }

    @Transient
    public int getPoints() {
        int points = 0;
        for (Review review : getReviews()) {
            points += review.getScore();
        }

        if (getReviews().size() < contest.getJuries().size()) {
            int dif = contest.getJuries().size() - getReviews().size();
            points += (dif * 3);
        }

        return points / contest.getJuries().size();
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    @Override
    public int compareTo(ContestPhoto o) {
        return Integer.compare(this.getPoints(), o.getPoints());
    }
}
