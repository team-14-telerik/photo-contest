package com.example.intellijproject.models;

import javax.persistence.*;
import java.text.DateFormatSymbols;
import java.time.LocalDateTime;

@Entity
@Table(name = "notifications")
public class Notification {
    public static final String SPACE = " ";
    public static final String COMMA = ",";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "message")
    private String message;

    @Column(name = "creation_date")
    private LocalDateTime creationDate;

    @Column(name = "is_read")
    private boolean isRead;

    @JoinColumn(name = "user_id")
    @ManyToOne
    private User user;

    @JoinColumn(name = "type")
    @ManyToOne
    private NotificationType type;

    @JoinColumn(name = "contest_id")
    @ManyToOne
    private Contest contest;

    public Notification() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(LocalDateTime creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isRead() {
        return isRead;
    }

    public void setRead(boolean read) {
        isRead = read;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Contest getContest() {
        return contest;
    }

    public void setContest(Contest contest) {
        this.contest = contest;
    }

    public NotificationType getType() {
        return type;
    }

    public void setType(NotificationType type) {
        this.type = type;
    }

    @Transient
    public String getCreationDateFormatted() {
        StringBuilder builder = new StringBuilder();
        String monthName = getMonthForInt(creationDate.getMonthValue());
        int day = creationDate.getDayOfMonth();
        int year = creationDate.getYear();
        builder.append(monthName).append(SPACE).append(day).append(COMMA).append(SPACE).append(year);
        return builder.toString();
    }

    private String getMonthForInt(int num) {
        String month = "wrong";
        DateFormatSymbols dfs = new DateFormatSymbols();
        String[] months = dfs.getMonths();
        if (num >= 0 && num <= 11) {
            month = months[num];
        }
        return month;
    }
}