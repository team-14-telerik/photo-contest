package com.example.intellijproject.models;

import javax.persistence.*;

@Entity
@Table(name = "ranks")
public class Rank {

    public static final String JUNKIE = "junkie";
    public static final String ENTHUSIAST = "enthusiast";
    public static final String MASTER = "master";
    public static final String DICTATOR = "wise_and_benevolent_dictator";


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "rank_name")
    private String name;

    @Column(name = "min_score")
    private int minScore;

    public Rank() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinScore() {
        return minScore;
    }

    public void setMinScore(int minScore) {
        this.minScore = minScore;
    }
}
