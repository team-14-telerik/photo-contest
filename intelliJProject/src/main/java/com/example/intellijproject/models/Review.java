package com.example.intellijproject.models;

import com.example.intellijproject.models.contracts.Userable;

import javax.persistence.*;

@Entity
@Table(name = "reviews")
public class Review implements Userable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @JoinColumn(name = "user_id")
    @ManyToOne
    private User user;

    @Column(name = "score")
    private int score;

    @Column(name = "comment")
    private String comment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "photo_id")
    private ContestPhoto contestPhoto;

    public Review() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public ContestPhoto getContestPhoto() {
        return contestPhoto;
    }

    public void setContestPhoto(ContestPhoto contestPhoto) {
        this.contestPhoto = contestPhoto;
    }
}
