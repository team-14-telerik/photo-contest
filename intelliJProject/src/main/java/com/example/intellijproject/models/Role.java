package com.example.intellijproject.models;

import javax.persistence.*;

@Entity
@Table(name = "roles")
public class Role {

    public static final String ORGANIZER = "organizer";
    public static final int ORGANIZER_ROLE_ID = 2;
    public static final String ADMIN = "admin";
    public static final int ADMIN_ROLE_ID = 3;
    public static final String PHOTO_JUNKIE = "photo_junkie";
    public static final int PHOTO_JUNKIE_ROLE_ID = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "role_name")
    private String name;

    public Role() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
