package com.example.intellijproject.models.contracts;

import com.example.intellijproject.models.User;

public interface Userable {
    User getUser();
}
