package com.example.intellijproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class CategoryDTO {
    @NotNull
    @Size(min = 10, max = 100, message = "Name should be between 10 and 150 symbols.")
    private String name;

    public CategoryDTO() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
