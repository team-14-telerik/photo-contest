package com.example.intellijproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ChangePasswordDTO {

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 20, message = "Password be between 8 and 20 symbols")
    private String newPassword;

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 20, message = "Password be between 8 and 20 symbols")
    private String confirmedPassword;

    private String token;

    public ChangePasswordDTO() {

    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }
}
