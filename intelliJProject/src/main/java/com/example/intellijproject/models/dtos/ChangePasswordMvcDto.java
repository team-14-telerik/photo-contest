package com.example.intellijproject.models.dtos;

public class ChangePasswordMvcDto {
    private String newPassword;
    private String confirmedPassword;

    public ChangePasswordMvcDto() {
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getConfirmedPassword() {
        return confirmedPassword;
    }

    public void setConfirmedPassword(String confirmedPassword) {
        this.confirmedPassword = confirmedPassword;
    }
}
