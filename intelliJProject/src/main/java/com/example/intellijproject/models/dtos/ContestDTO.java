package com.example.intellijproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.Date;

public class ContestDTO {
    @NotNull
    @Size(min = 10, max = 150, message = "Title should be between 10 and 150 symbols.")
    private String title;

    @NotNull
    @Positive
    private int categoryId;

    @NotNull
    @Positive
    private int statusId;

    @NotNull
    private String photoUrl;

    @NotNull
    private Date deadlinePhaseOne;

    @NotNull
    private Date deadlinePhaseTwo;

    public ContestDTO() {

    }

    public String getTitle() {
        return title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public int getStatusId() {
        return statusId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public Date getDeadlinePhaseOne() {
        return deadlinePhaseOne;
    }

    public Date getDeadlinePhaseTwo() {
        return deadlinePhaseTwo;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public void setDeadlinePhaseOne(Date deadlinePhaseOne) {
        this.deadlinePhaseOne = deadlinePhaseOne;
    }

    public void setDeadlinePhaseTwo(Date deadlinePhaseTwo) {
        this.deadlinePhaseTwo = deadlinePhaseTwo;
    }
}
