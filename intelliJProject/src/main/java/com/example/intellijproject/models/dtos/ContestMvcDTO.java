package com.example.intellijproject.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

public class ContestMvcDTO {
    @NotNull
    @Size(min = 10, max = 150, message = "Title should be between 10 and 150 symbols.")
    private String title;

    @NotNull
    @Positive
    private int categoryId;

    @NotNull
    @Positive
    private int statusId;

    private String photoUrl;

    @NotNull
    private int deadlinePhaseOne;

    @NotNull
    private int deadlinePhaseTwo;

    private MultipartFile multipartPhoto;

    private String oldContestPhotoUrl;

    private String jury;

    private String users;

    public ContestMvcDTO() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getStatusId() {
        return statusId;
    }

    public void setStatusId(int statusId) {
        this.statusId = statusId;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public int getDeadlinePhaseOne() {
        return deadlinePhaseOne;
    }

    public void setDeadlinePhaseOne(int deadlinePhaseOne) {
        this.deadlinePhaseOne = deadlinePhaseOne;
    }

    public int getDeadlinePhaseTwo() {
        return deadlinePhaseTwo;
    }

    public void setDeadlinePhaseTwo(int deadlinePhaseTwo) {
        this.deadlinePhaseTwo = deadlinePhaseTwo;
    }

    public MultipartFile getMultipartPhoto() {
        return multipartPhoto;
    }

    public void setMultipartPhoto(MultipartFile multipartPhoto) {
        this.multipartPhoto = multipartPhoto;
    }

    public String getOldContestPhotoUrl() {
        return oldContestPhotoUrl;
    }

    public void setOldContestPhotoUrl(String oldContestPhotoUrl) {
        this.oldContestPhotoUrl = oldContestPhotoUrl;
    }

    public String getJury() {
        return jury;
    }

    public void setJury(String jury) {
        this.jury = jury;
    }

    public String getUsers() {
        return users;
    }

    public void setUsers(String users) {
        this.users = users;
    }
}
