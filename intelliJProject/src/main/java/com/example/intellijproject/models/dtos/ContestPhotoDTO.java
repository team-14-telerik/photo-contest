package com.example.intellijproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestPhotoDTO {

    @NotNull
    @Size(min = 10, max = 100, message = "Title should be between 10 and 100 symbols.")
    private String title;

    @NotNull
    private String photoUrl;

    @NotNull
    private String story;

    public ContestPhotoDTO() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }
}
