package com.example.intellijproject.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class ContestPhotoMvcDTO {
    @NotNull
    @Size(min = 10, max = 100, message = "Title should be between 10 and 100 symbols.")
    private String title;

    @NotNull
    @Size(min = 10, max = 2000, message = "Story should be between 10 and 100 symbols.")
    private String story;

    private MultipartFile multipartPhoto;

    public ContestPhotoMvcDTO() {

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStory() {
        return story;
    }

    public void setStory(String story) {
        this.story = story;
    }

    public MultipartFile getMultipartPhoto() {
        return multipartPhoto;
    }

    public void setMultipartPhoto(MultipartFile multipartPhoto) {
        this.multipartPhoto = multipartPhoto;
    }
}
