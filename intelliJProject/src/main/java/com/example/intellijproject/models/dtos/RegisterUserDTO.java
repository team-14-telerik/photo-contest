package com.example.intellijproject.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RegisterUserDTO {
    @NotNull(message = "First name can't be empty")
    @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols")
    private String firstName;

    @NotNull(message = "Last name can't be empty")
    @Size(min = 2, max = 20, message = "Name should be between 2 and 20 symbols")
    private String lastName;

    @NotNull(message = "Email can't be empty")
    @Size(min = 3, max = 20, message = "Email should be between 3 and 20 symbols")
    private String email;

    @NotNull(message = "Username can't be empty")
    @Size(min = 2, max = 20, message = "Username should be between 2 and 20 symbols")
    private String username;

    @NotNull(message = "Password can't be empty")
    @Size(min = 8, max = 20, message = "Password be between 8 and 20 symbols")
    private String password;

    private MultipartFile multipartPhoto;

    public RegisterUserDTO() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public MultipartFile getMultipartPhoto() {
        return multipartPhoto;
    }

    public void setMultipartPhoto(MultipartFile multipartPhoto) {
        this.multipartPhoto = multipartPhoto;
    }
}