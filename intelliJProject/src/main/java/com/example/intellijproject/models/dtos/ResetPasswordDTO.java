package com.example.intellijproject.models.dtos;

public class ResetPasswordDTO {
    String email;

    public ResetPasswordDTO() {

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
