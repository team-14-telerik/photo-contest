package com.example.intellijproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

public class ReviewDTO {

    @NotNull
    @Size(min = 32, max = 8192, message = "Comment should be between 32 and 8192 symbols")
    private String comment;

    @NotNull
    @PositiveOrZero
    private int score;

    public ReviewDTO() {
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
