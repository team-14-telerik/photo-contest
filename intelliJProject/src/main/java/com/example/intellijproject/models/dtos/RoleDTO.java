package com.example.intellijproject.models.dtos;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class RoleDTO {
    @NotNull(message = "Role should not be null")
    @Size(min = 3, max = 15, message = "Role should be between 3 and 15 symbols")
    private String roleName;

    public RoleDTO() {
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }
}
