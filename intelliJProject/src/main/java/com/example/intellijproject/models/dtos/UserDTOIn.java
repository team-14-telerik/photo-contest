package com.example.intellijproject.models.dtos;

import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDTOIn {

    @NotNull
    @Size(min = 4, max = 60, message = "Username must be between 4 and 60 symbols!")
    public String username;

    @NotNull
    @Size(min = 4, max = 50, message = "Firstname must be between 4 and 50 symbols!")
    public String firstName;

    @NotNull
    @Size(min = 4, max = 50, message = "Lastname must be between 4 and 50 symbols!")
    public String lastName;

    @NotNull
    @Pattern(message = "Minimum eight characters, at least one uppercase letter," +
            " one lowercase letter, one number and one special character:",
            regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$")
    public String password;

    @NotNull
    @Pattern(message = "Minimum eight characters, at least one uppercase letter," +
            " one lowercase letter, one number and one special character:",
            regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[@$!%*?&])[A-Za-z\\d@$!%*?&]{8,}$")
    public String passwordConfirmation;

    @NotNull
    @Email(message = "Please enter a valid email address.",
            regexp = "^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$")
    public String email;

    private MultipartFile multipartPhoto;

    public UserDTOIn() {
    }

    public UserDTOIn(String username, String firstName, String lastName, String password, String email) {
        this.username = username;
        this.firstName = firstName;
        this.lastName = lastName;
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public MultipartFile getMultipartPhoto() {
        return multipartPhoto;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public void setMultipartPhoto(MultipartFile multipartPhoto) {
        this.multipartPhoto = multipartPhoto;
    }
}
