package com.example.intellijproject.models.dtos;

import com.example.intellijproject.models.Rank;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.UserPhoto;

import java.time.LocalDateTime;

public class UserDTOOUTAdmin {

    private int id;
    private String username;
    private String first_name;
    private String last_name;
    private String email;
    private LocalDateTime registrationDate;
    private UserPhoto userPhoto;
    private Role role;
    private Rank rank;
    private int score;
    private boolean isDeleted;
    private boolean isBlocked;

    public UserDTOOUTAdmin(int id, String username, String first_name, String last_name, String email,
                           LocalDateTime registrationDate, UserPhoto userPhoto, Role role, Rank rank,
                           int score, boolean isDeleted, boolean isBlocked) {
        this.id = id;
        this.username = username;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.registrationDate = registrationDate;
        this.userPhoto = userPhoto;
        this.role = role;
        this.rank = rank;
        this.score = score;
        this.isDeleted = isDeleted;
        this.isBlocked = isBlocked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public UserPhoto getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(UserPhoto userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

}
