package com.example.intellijproject.models.dtos;

import com.example.intellijproject.models.Rank;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.UserPhoto;

import java.time.LocalDateTime;

public class UserDTOOut implements UserDTOOutInterface {

    private int id;
    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private LocalDateTime registrationDate;
    private UserPhoto userPhoto;
    private Role role;
    private Rank rank;
    private int score;


    public UserDTOOut(int id, String username, String first_name, String last_name, String email,
                      LocalDateTime registrationDate, UserPhoto userPhoto, Role role, Rank rank, int score) {
        this.id = id;
        this.username = username;
        this.firstName = first_name;
        this.lastName = last_name;
        this.email = email;
        this.registrationDate = registrationDate;
        this.userPhoto = userPhoto;
        this.role = role;
        this.rank = rank;
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDateTime getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDateTime registrationDate) {
        this.registrationDate = registrationDate;
    }

    public UserPhoto getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(UserPhoto userPhoto) {
        this.userPhoto = userPhoto;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Rank getRank() {
        return rank;
    }

    public void setRank(Rank rank) {
        this.rank = rank;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
