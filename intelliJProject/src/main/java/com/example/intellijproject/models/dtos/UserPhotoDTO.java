package com.example.intellijproject.models.dtos;

import org.springframework.web.multipart.MultipartFile;

public class UserPhotoDTO {

    private MultipartFile photo;

    public UserPhotoDTO() {
    }

    public UserPhotoDTO(MultipartFile photo) {
        this.photo = photo;
    }

    public MultipartFile getPhoto() {
        return photo;
    }

    public void setPhoto(MultipartFile photo) {
        this.photo = photo;
    }
}
