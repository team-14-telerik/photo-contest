package com.example.intellijproject.repositories;

import com.example.intellijproject.models.Category;
import com.example.intellijproject.repositories.contracts.CategoryRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryRepositoryImpl extends AbstractCRUDRepository<Category> implements CategoryRepository {

    @Autowired
    public CategoryRepositoryImpl(SessionFactory sessionFactory) {
        super(Category.class, sessionFactory);
    }
}