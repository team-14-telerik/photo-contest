package com.example.intellijproject.repositories;

import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.Review;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ContestPhotoRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class ContestPhotoRepositoryImpl extends AbstractCRUDRepository<ContestPhoto> implements ContestPhotoRepository {

    public static final String SORT_MIN_PARAMETERS_ERR = "Sort should have at least 1 parameter";
    public static final String SORT_MAX_PARAMETER_ERR = "Sort should have maximum two params divided by _ symbol";

    @Autowired
    public ContestPhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestPhoto.class, sessionFactory);
    }

    @Override
    public List<ContestPhoto> getAllByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<ContestPhoto> query = session.createNativeQuery(
                    " select * from `photo-contest`.contests_photos" +
                            " where contest_id = :id"
            );
            query.addEntity(ContestPhoto.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public List<ContestPhoto> getAllByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<ContestPhoto> query = session.createNativeQuery(
                    " select * from `photo-contest`.contests_photos" +
                            " where user_id = :id"
            );
            query.addEntity(ContestPhoto.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public boolean isUserParticipant(int contestId, int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<ContestPhoto> query = session.createNativeQuery(
                    "select * from `photo-contest`.contests_photos as c" +
                            " where c.user_id = :id" +
                            " and c.contest_id = :contestId "
            );
            query.addEntity(ContestPhoto.class);
            query.setParameter("id", id);
            query.setParameter("contestId", contestId);

            return query.list().size() == 1;
        }
    }

    @Override
    public List<ContestPhoto> getAllByContestIdSorted(int id) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<ContestPhoto> query = session.createNativeQuery(
                    "SELECT c.*, avg(r.score) as TotalPoints" +
                            " from `photo-contest`.contests_photos c " +
                            " join `photo-contest`.reviews r on c.id = r.photo_id" +
                            " where contest_id = :id" +
                            " GROUP BY c.id" +
                            " order by TotalPoints desc;"
            );
            query.addEntity(ContestPhoto.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public List<ContestPhoto> getEightMostPopular() {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<ContestPhoto> query = session.createNativeQuery(
                    "SELECT c.*, avg(r.score) as TotalPoints" +
                            " from `photo-contest`.contests_photos c " +
                            "  join `photo-contest`.reviews r on c.id = r.photo_id" +
                            " GROUP BY c.id" +
                            " order by TotalPoints desc" +
                            " limit 8;"
            );
            query.addEntity(ContestPhoto.class);
            return query.list();
        }
    }

    @Override
    public List<ContestPhoto> getFiltered(Optional<Integer> limit, Optional<String> sortBy, Optional<String> sortOrder) {

        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("select *  from ContestPhoto ");

            sortBy.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<ContestPhoto> queryList = session.createQuery(queryString.toString(), ContestPhoto.class);
            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");
        if (value.isEmpty()) {
            throw new UnsupportedOperationException(SORT_MIN_PARAMETERS_ERR);
        }
        switch (params[0]) {
            case "creationDate":
                queryString.append(" p.creationDate ");
                break;
            case "points":
                queryString.append(" p.user.username ");
                break;
        }
        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException(SORT_MAX_PARAMETER_ERR);
        }
        return queryString.toString();
    }

    @Override
    public boolean isNotReviewed(ContestPhoto contestPhoto, User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("select distinct r from Review r  " +
                    " where r.user.id= :userId " +
                    " and r.contestPhoto.id = :photoId ");
            query.setParameter("userId", user.getId());
            query.setParameter("photoId", contestPhoto.getId());
            return query.list().isEmpty();
        }

    }

    @Override
    public List<ContestPhoto> getAllFiltered(Optional<String> title) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestPhoto> query = session.createQuery("select distinct c from ContestPhoto c  " +
                    " where c.title like :title ");
            query.setParameter("title", title);
            return query.list();
        }
    }

}
