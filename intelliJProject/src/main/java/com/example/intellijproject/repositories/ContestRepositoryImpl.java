package com.example.intellijproject.repositories;

import com.example.intellijproject.models.Contest;
import com.example.intellijproject.repositories.contracts.ContestRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Repository
public class ContestRepositoryImpl extends AbstractCRUDRepository<Contest> implements ContestRepository {

    @Autowired
    public ContestRepositoryImpl(SessionFactory sessionFactory) {
        super(Contest.class, sessionFactory);
    }

    @Override
    public List<Contest> getAllPhaseOne() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where deadlinePhaseOne > :currentTime " +
                    " and finished = false", Contest.class);
            query.setParameter("currentTime", Date.valueOf(LocalDate.now()));
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllPhaseTwo() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where deadlinePhaseTwo > :currentTime " +
                    " and deadlinePhaseOne <= :currentDate and finished = false ", Contest.class);
            query.setParameter("currentDate", Date.valueOf(LocalDate.now()));
            query.setParameter("currentTime", LocalDateTime.now());
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllFinished() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where finished = true ", Contest.class);
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllOpen() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("from Contest where status.name like 'open' and " +
                    " deadlinePhaseOne > :currentTime and finished = false ", Contest.class);
            query.setParameter("currentTime", Date.valueOf(LocalDate.now()));
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("select distinct c from Contest c join c.contestPhotos p " +
                    " where p.user.id = :id and c.finished = false ", Contest.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllFinishedByUserId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("select distinct c from Contest c join c.contestPhotos p " +
                    " where p.user.id = :id and c.finished = true ", Contest.class);
            query.setParameter("id", id);
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllFinishing() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("select distinct c from Contest c " +
                    "where c.deadlinePhaseTwo < :date and finished = false ", Contest.class);
            query.setParameter("date", LocalDateTime.now());
            return query.list();
        }
    }

    @Override
    public List<Contest> getAllJuriedByUserId(int id) {

        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Contest> results = session.createNativeQuery(
                    " select distinct * from contests " +
                            " as c join jury j on c.id = j.contest_id " +
                            " where j.user_id = :id and c.deadline_phase_1 <= current_timestamp and c.deadline_phase_2 >= current_timestamp "
            );

            results.setParameter("id", id);
            results.addEntity(Contest.class);
            return results.list();
        }
    }

    @Override
    public List<Contest> getTenNewest() {
        try (Session session = sessionFactory.openSession()) {
            Query<Contest> query = session.createQuery("select distinct c from Contest c where finished = false " +
                    "order by deadlinePhaseOne");
            List<Contest> result = query.list();
            return result;
        }
    }

    @Override
    public List<Contest> getAllFiltered(Optional<String> title, Optional<String> category, Optional<String> status,
                                        Optional<String> phase, Optional<String> sort, Optional<String> finished, Optional<String> username) {
        try (Session session = sessionFactory.openSession()) {
            var queryString = new StringBuilder("select distinct c from Contest c ");
            var filter = new ArrayList<String>();
            var queryParams = new HashMap<String, Object>();


            title.ifPresent(value -> {
                filter.add("c.title like :title ");
                queryParams.put("title", "%" + value + "%");
            });

            username.ifPresent(value -> {
                queryString.append("join c.contestPhotos p ");
                filter.add(" p.user.username like :username ");
                queryParams.put("username", "%" + value + "%");
            });

            category.ifPresent(value -> {
                queryString.append("join c.category k ");
                filter.add(" c.category.name like :category ");
                queryParams.put("category", value);
            });

            status.ifPresent(value -> {
                queryString.append("join c.status s ");
                filter.add(" c.status.name like :type ");
                queryParams.put("type", value);
            });

            phase.ifPresent(value -> {
                switch (value) {
                    case "one":
                        filter.add(" c.deadlinePhaseOne >= :date ");
                        queryParams.put("date", Date.valueOf(LocalDate.now()));
                        filter.add(" c.finished = false ");
                        break;
                    case "two":
                        filter.add(" c.deadlinePhaseOne <= :date and c.deadlinePhaseTwo >= :date ");
                        queryParams.put("date", Date.valueOf(LocalDate.now()));
                        filter.add(" c.finished = false ");
                        break;
                    case "finished":
                        filter.add(" c.finished = true ");
                        break;
                }
            });

            if (!filter.isEmpty()) {
                queryString.append(" where ").append(String.join(" and ", filter));
            }

            sort.ifPresent(value -> {
                queryString.append(generateSortString(value));
            });

            Query<Contest> queryList = session.createQuery(queryString.toString(), Contest.class);
            queryList.setProperties(queryParams);
            return queryList.list();
        }
    }

    private String generateSortString(String value) {
        var queryString = new StringBuilder(" order by ");
        String[] params = value.split("_");
        switch (params[0]) {
            case "creationDate":
                queryString.append(" c.deadlinePhaseOne ");
                break;
            case "title":
                queryString.append(" c.title ");
                break;
            case "photos":
                queryString.append(" c.contestPhotos.size ");
        }

        if (params.length > 1 && params[1].equals("desc")) {
            queryString.append(" desc ");
        }
        if (params.length > 2) {
            throw new UnsupportedOperationException("Sort should have maximum two params divided by _ symbol");
        }
        return queryString.toString();
    }
}
