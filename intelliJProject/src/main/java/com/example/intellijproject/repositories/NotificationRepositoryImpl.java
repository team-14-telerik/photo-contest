package com.example.intellijproject.repositories;

import com.example.intellijproject.models.Notification;
import com.example.intellijproject.repositories.contracts.NotificaitonRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NotificationRepositoryImpl extends AbstractCRUDRepository<Notification> implements NotificaitonRepository {

    @Autowired
    public NotificationRepositoryImpl(SessionFactory sessionFactory) {
        super(Notification.class, sessionFactory);
    }

    @Override
    public List<Notification> getAllActiveUserNotifications(int userId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Notification> query = session.createQuery("from Notification where user.id = :id and isRead = false ",
                    Notification.class);
            query.setParameter("id", userId);
            List<Notification> result = query.list();
            return result;
        }
    }
}
