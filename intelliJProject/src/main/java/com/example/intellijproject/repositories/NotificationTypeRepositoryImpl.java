package com.example.intellijproject.repositories;

import com.example.intellijproject.models.NotificationType;
import com.example.intellijproject.repositories.contracts.NotificationTypeRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class NotificationTypeRepositoryImpl extends AbstractCRUDRepository<NotificationType> implements
        NotificationTypeRepository {

    @Autowired
    public NotificationTypeRepositoryImpl(SessionFactory sessionFactory) {
        super(NotificationType.class, sessionFactory);
    }

}
