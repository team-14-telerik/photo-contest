package com.example.intellijproject.repositories;

import com.example.intellijproject.models.Rank;
import com.example.intellijproject.repositories.contracts.RankRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RankRepositoryImpl extends AbstractReadRepository<Rank> implements RankRepository {

    @Autowired
    public RankRepositoryImpl(SessionFactory sessionFactory) {
        super(Rank.class, sessionFactory);
    }

    @Override
    public List<Rank> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Rank> query = session.createQuery("from Rank order by minScore", Rank.class);
            return query.list();
        }
    }
}
