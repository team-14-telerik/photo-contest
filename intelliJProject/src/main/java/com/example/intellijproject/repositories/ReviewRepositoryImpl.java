package com.example.intellijproject.repositories;

import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.Review;
import com.example.intellijproject.repositories.contracts.ReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReviewRepositoryImpl extends AbstractCRUDRepository<Review> implements ReviewRepository {

    @Autowired
    public ReviewRepositoryImpl(SessionFactory sessionFactory) {
        super(Review.class, sessionFactory);
    }

    @Override
    public Review getByContestPhotoIdAndUserId(int userId, int contestPhotoId) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review where user.id = :userId and " +
                    "contestPhoto.id = :contestPhotoId", Review.class);
            query.setParameter("userId", userId);
            query.setParameter("contestPhotoId", contestPhotoId);
            List<Review> result = query.list();

            if (result.size() == 0) {
                throw new EntityNotFoundException("Review", "this userId: ", String.valueOf(userId));
            }

            return result.get(0);
        }
    }

    @Override
    public List<Review> getAllByContestId(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<Review> query = session.createQuery("from Review where contestPhoto.contest.id = :id", Review.class);
            query.setParameter("id", id);
            List<Review> result = query.list();
            return result;
        }
    }
}
