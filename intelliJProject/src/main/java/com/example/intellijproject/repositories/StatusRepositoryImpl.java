package com.example.intellijproject.repositories;

import com.example.intellijproject.models.Status;
import com.example.intellijproject.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class StatusRepositoryImpl extends AbstractCRUDRepository<Status> implements StatusRepository {

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(Status.class, sessionFactory);
    }
}
