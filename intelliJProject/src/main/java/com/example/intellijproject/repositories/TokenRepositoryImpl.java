package com.example.intellijproject.repositories;

import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.repositories.contracts.TokenRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class TokenRepositoryImpl extends AbstractCRUDRepository<ConfirmationToken> implements TokenRepository {

    @Autowired
    public TokenRepositoryImpl(SessionFactory sessionFactory) {
        super(ConfirmationToken.class, sessionFactory);
    }
}
