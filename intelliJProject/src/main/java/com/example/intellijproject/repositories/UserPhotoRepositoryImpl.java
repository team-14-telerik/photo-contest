package com.example.intellijproject.repositories;

import com.example.intellijproject.models.UserPhoto;
import com.example.intellijproject.repositories.contracts.UserPhotoRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class UserPhotoRepositoryImpl extends AbstractCRUDRepository<UserPhoto> implements UserPhotoRepository {

    @Autowired
    public UserPhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(UserPhoto.class, sessionFactory);
    }

}
