package com.example.intellijproject.repositories;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.UserRepository;
import com.example.intellijproject.utils.UsersQueryMaker;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;


@Repository
public class UserRepositoryImpl extends AbstractCRUDRepository<User> implements UserRepository {

    public static final String[] PERMITTED_SORT_CRITERIA = {"id", "username", "first_name", "last_name",
            "email", "registration_date", "score", "userRole"};


    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(User.class, sessionFactory);
    }

    @Override
    public List<User> getAll(Optional<String> username, Optional<String> firstname,
                             Optional<String> lastname, Optional<String> email,
                             Optional<String> userRole, Optional<String> userRank,
                             Optional<Integer> minScore, Optional<Integer> maxScore,
                             Optional<String> sortBy, Optional<String> sortOrder) {
        try (Session session = sessionFactory.openSession()) {
            UsersQueryMaker queryMaker = new UsersQueryMaker(" select u from User u " +
                    "join u.role r join u.rank k ");
            String query = queryMaker.buildHQLSearchAndSortQuery(username, firstname, lastname,
                    email, userRole, userRank, minScore, maxScore, sortBy, sortOrder);
            HashMap<String, Object> propertiesMap = queryMaker.getProperties();
            Query<User> request = session.createQuery(query, User.class);
            request.setProperties(propertiesMap);
            return request.list();
        }
    }

    @Override
    public void validateUserParamIsUnique(String column, String value) {
        String queryString = String.format(" from User where %s = :%s ", column, column);
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery(queryString, User.class);
            query.setParameter(column, value);
            List<User> result = query.list();
            if (!result.isEmpty()) {
                throw new DuplicateEntityException("User", column, value);
            }
        }
    }


    @Override
    public List<User> getByRole(String roleName) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<User> results = session.createNativeQuery(
                    "select *\n" +
                            "from users u\n" +
                            "join roles r on u.role_id = r.id\n" +
                            "where r.role_name = :roleName"
            );

            results.setParameter("roleName", roleName);
            results.addEntity(User.class);
            return results.list();
        }
    }


    @Override
    public List<User> getTopJunkies(Optional<Integer> count) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createQuery("from User order by score desc", User.class);
            count.ifPresent(query::setMaxResults);
            return query.getResultList();
        }
    }

    @Override
    public Long getNumberOfUsers() {
        try (Session session = sessionFactory.openSession()) {
            Query<Long> query = session.createQuery(" select count(user) from User as user order by score ", Long.class);

            return query.getSingleResult();
        }
    }

    @Override
    public boolean isUserJury(int contestId, int userId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<User> query = session.createNativeQuery(
                    "select * from users" +
                            " join jury j on users.id = j.user_id" +
                            " where j.user_id = :userId and j.contest_id = :contestId"
            );
            query.addEntity(User.class);
            query.setParameter("userId", userId);
            query.setParameter("contestId", contestId);
            for (int i = 0; i < query.list().size(); i++) {
                User user = query.list().get(0);
            }
            return query.list().size() > 0;
        }
    }
}
