package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Category;

public interface CategoryRepository extends BaseCRUDRepository<Category> {
}
