package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;

import java.util.List;
import java.util.Optional;

public interface ContestPhotoRepository extends BaseCRUDRepository<ContestPhoto> {

    List<ContestPhoto> getAllByContestId(int id);

    List<ContestPhoto> getAllByUserId(int id);

    boolean isUserParticipant(int contestId, int id);

    List<ContestPhoto> getAllByContestIdSorted(int id);

    List<ContestPhoto> getEightMostPopular();

    List<ContestPhoto> getFiltered(Optional<Integer> limit, Optional<String> sortBy, Optional<String> sortOrder);

    boolean isNotReviewed(ContestPhoto contestPhoto, User user);

    List<ContestPhoto> getAllFiltered(Optional<String> title);
}