package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Contest;

import java.util.List;
import java.util.Optional;

public interface ContestRepository extends BaseCRUDRepository<Contest> {
    List<Contest> getAllFiltered(Optional<String> title, Optional<String> category, Optional<String> status, Optional<String> phase,
                                 Optional<String> sort, Optional<String> finished, Optional<String> username);

    List<Contest> getAllPhaseOne();

    List<Contest> getAllPhaseTwo();

    List<Contest> getAllFinished();

    List<Contest> getAllOpen();

    List<Contest> getAllByUserId(int id);

    List<Contest> getAllFinishedByUserId(int id);

    List<Contest> getAllFinishing();

    List<Contest> getAllJuriedByUserId(int id);

    List<Contest> getTenNewest();
}
