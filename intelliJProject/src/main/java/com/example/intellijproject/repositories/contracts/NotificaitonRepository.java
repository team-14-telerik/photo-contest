package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Notification;

import java.util.List;

public interface NotificaitonRepository extends BaseCRUDRepository<Notification> {
    List<Notification> getAllActiveUserNotifications(int userId);
}
