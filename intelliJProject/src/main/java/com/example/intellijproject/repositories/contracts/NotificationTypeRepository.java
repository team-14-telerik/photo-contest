package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.NotificationType;

public interface NotificationTypeRepository extends BaseCRUDRepository<NotificationType> {
}
