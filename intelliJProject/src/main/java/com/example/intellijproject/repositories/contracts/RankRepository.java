package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Rank;

public interface RankRepository extends BaseReadRepository<Rank> {
}
