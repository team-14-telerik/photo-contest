package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Review;

import java.util.List;

public interface ReviewRepository extends BaseCRUDRepository<Review> {
    Review getByContestPhotoIdAndUserId(int userId, int contestPhotoId);

    List<Review> getAllByContestId(int id);
}
