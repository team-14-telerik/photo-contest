package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Role;

public interface RoleRepository extends BaseReadRepository<Role> {
}
