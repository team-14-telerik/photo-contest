package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.Status;

public interface StatusRepository extends BaseCRUDRepository<Status> {
}
