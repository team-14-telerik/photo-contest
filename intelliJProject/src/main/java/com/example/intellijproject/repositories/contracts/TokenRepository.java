package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.ConfirmationToken;

public interface TokenRepository extends BaseCRUDRepository<ConfirmationToken> {
}
