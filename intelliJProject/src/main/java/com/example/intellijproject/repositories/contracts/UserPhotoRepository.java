package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.UserPhoto;

public interface UserPhotoRepository extends BaseCRUDRepository<UserPhoto> {
}
