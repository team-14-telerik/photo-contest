package com.example.intellijproject.repositories.contracts;

import com.example.intellijproject.models.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends BaseCRUDRepository<User> {

    List<User> getAll(Optional<String> username, Optional<String> firstname,
                      Optional<String> lastname, Optional<String> email,
                      Optional<String> userRole, Optional<String> userRank,
                      Optional<Integer> minScore, Optional<Integer> maxScore,
                      Optional<String> sortBy, Optional<String> sortOrder);

    void validateUserParamIsUnique(String username, String username1);

    List<User> getByRole(String roleName);

    List<User> getTopJunkies(Optional<Integer> count);

    Long getNumberOfUsers();

    boolean isUserJury(int contestId, int userId);
}
