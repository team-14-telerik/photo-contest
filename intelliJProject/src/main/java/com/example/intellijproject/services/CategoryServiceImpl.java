package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.Category;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.CategoryRepository;
import com.example.intellijproject.services.contracts.CategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.intellijproject.utils.ValidationHelpers.*;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryRepository categoryRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public List<Category> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public Category getById(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public Category getByName(String name) {
        return categoryRepository.getByField("name", name);
    }

    @Override
    public void create(Category category, User user) {
        validateUserIsDeleted(user);
        validateUserIsBlocked(user);
        boolean duplicateExists = true;
        try {
            categoryRepository.getByField("name", category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }
        validateUserIsOrganizerOrAdmin(user);
        categoryRepository.create(category);
    }

    @Override
    public void update(Category category, User user) {
        validateUserIsOrganizerOrAdmin(user);
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean duplicateExists = true;
        try {
            categoryRepository.getByField("name", category.getName());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Category", "name", category.getName());
        }
        categoryRepository.update(category);
    }

    @Override
    public void delete(int id, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsOrganizerOrAdmin(user);
        categoryRepository.delete(id);
    }
}
