package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ContestPhotoRepository;
import com.example.intellijproject.repositories.contracts.ReviewRepository;
import com.example.intellijproject.services.contracts.ContestPhotoService;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.utils.CloudinaryHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static com.example.intellijproject.utils.ValidationHelpers.*;

@Service
public class ContestPhotoServiceImpl implements ContestPhotoService {

    private final static String PARTICIPATION_ERR_MSG = "You are a participant already";
    private final ContestPhotoRepository contestPhotoRepository;
    private final CloudinaryHelper cloudinaryHelper;
    private final ContestService contestService;
    private final ReviewRepository reviewRepository;

    public ContestPhotoServiceImpl(ContestPhotoRepository contestPhotoRepository, CloudinaryHelper cloudinaryHelper,
                                   ContestService contestService, ReviewRepository reviewRepository) {
        this.contestPhotoRepository = contestPhotoRepository;
        this.cloudinaryHelper = cloudinaryHelper;
        this.contestService = contestService;
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<ContestPhoto> getAll() {
        return contestPhotoRepository.getAll();
    }

    @Override
    public ContestPhoto getById(int id) {
        return contestPhotoRepository.getById(id);
    }

    @Override
    public List<ContestPhoto> getFiltered(Optional<Integer> limit, Optional<String> sortBy, Optional<String> sortOrder) {
        return contestPhotoRepository.getFiltered(limit, sortBy, sortOrder);
    }

    @Override
    public List<ContestPhoto> getAllByContestId(int id) {
        return contestPhotoRepository.getAllByContestId(id);
    }

    @Override
    public List<ContestPhoto> getAllByUserId(int id) {
        return contestPhotoRepository.getAllByUserId(id);
    }

    @Override
    public void create(ContestPhoto contestPhoto, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsJury(contestPhoto.getContest(), user);
        validateUserIsParticipant(contestPhoto.getContest().getId(), user);
        contestPhotoRepository.create(contestPhoto);
    }

    @Override
    public String savePhoto(MultipartFile photoAsFile, int contestId, User user) throws IOException {
        validateUserIsJury(contestService.getById(contestId), user);
        validateUserIsParticipant(contestId, user);
        return cloudinaryHelper.uploadContestPhotoToCloudinary(photoAsFile);
    }

    private void validateUserIsParticipant(int contestId, User user) {
        if (contestPhotoRepository.isUserParticipant(contestId, user.getId())) {
            throw new UnauthorizedOperationException(PARTICIPATION_ERR_MSG);
        }
    }

    @Override
    public void delete(int contestId, int photoId, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        ContestPhoto photo = contestPhotoRepository.getById(photoId);
        Contest contest = contestService.getById(contestId);
        photo.getReviews().forEach(review -> reviewRepository.delete(review.getId()));
        photo.getReviews().clear();
        validateUserCannotDelete(contest, user);
        contestPhotoRepository.delete(photoId);
    }

    @Override
    public List<ContestPhoto> getAllByContestIdSorted(int id) {
        return contestPhotoRepository.getAllByContestIdSorted(id);
    }

    @Override
    public List<ContestPhoto> getEightMostPopular() {
        return contestPhotoRepository.getEightMostPopular();
    }

    @Override
    public boolean isNotReviewed(ContestPhoto contestPhoto, User user) {
        return contestPhotoRepository.isNotReviewed(contestPhoto, user);
    }

    @Override
    public List<ContestPhoto> getAllFiltered(Optional<String> title) {
        return contestPhotoRepository.getAllFiltered(title);
    }
}
