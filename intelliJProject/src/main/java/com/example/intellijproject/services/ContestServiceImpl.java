package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.WrongDateException;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ContestRepository;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.CloudinaryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import static com.example.intellijproject.utils.ValidationHelpers.*;


@Service
public class ContestServiceImpl implements ContestService {

    public static final String CONTESTS_PAST_DATE_ERR = "Contests cannot be set in the past!";
    public static final String CONTEST_DURATION_ERROR = "The length of one contest must not be longer than 1 month!";
    private final ContestRepository contestRepository;
    private final UserService userService;
    private final CloudinaryHelper cloudinaryHelper;

    @Autowired
    public ContestServiceImpl(ContestRepository contestRepository, UserService userService, CloudinaryHelper cloudinaryHelper) {
        this.contestRepository = contestRepository;
        this.userService = userService;
        this.cloudinaryHelper = cloudinaryHelper;
    }

    @Override
    public List<Contest> getAllFiltered(Optional<String> title, Optional<String> category, Optional<String> status, Optional<String> phase,
                                        Optional<String> sort, Optional<String> finished, Optional<String> username) {
        return contestRepository.getAllFiltered(title, category, status, phase, sort, finished, username);
    }

    @Override
    public List<Contest> getTenNewest() {
        return contestRepository.getTenNewest();
    }

    @Override
    public List<Contest> getAllJuriedByUserId(int id) {
        return contestRepository.getAllJuriedByUserId(id);
    }

    @Override
    public Contest getById(int id) {
        return contestRepository.getById(id);
    }

    @Override
    public List<Contest> getAllPhaseOne() {
        return contestRepository.getAllPhaseOne();
    }

    @Override
    public List<Contest> getAllPhaseTwo() {
        return contestRepository.getAllPhaseTwo();
    }

    @Override
    public List<Contest> getAllFinished() {
        return contestRepository.getAllFinished();
    }

    @Override
    public List<Contest> getAllOpen() {
        return contestRepository.getAllOpen();
    }

    @Override
    public List<Contest> getAllByUserId(int id) {
        return contestRepository.getAllByUserId(id);
    }

    @Override
    public List<Contest> getAllFinishedByUserId(int id) {
        return contestRepository.getAllFinishedByUserId(id);
    }

    @Override
    public List<Contest> getAllFinishing() {
        return contestRepository.getAllFinishing();
    }


    @Override
    public void create(Contest contest, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsOrganizerOrAdmin(user);
        boolean duplicateExists = true;
        try {
            contestRepository.getByField("title", contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        contest.getJuries().addAll(userService.getAllOrganizers());
        if (contest.getDeadlinePhaseOne().getTime() < System.currentTimeMillis() ||
                contest.getDeadlinePhaseTwo().isBefore(LocalDateTime.now())) {
            throw new WrongDateException(CONTESTS_PAST_DATE_ERR);
        }

        boolean olderThan30 = contest.getDeadlinePhaseTwo().isAfter(LocalDateTime.now().plusDays(31));
        if (olderThan30) {
            throw new WrongDateException(CONTEST_DURATION_ERROR);
        }
        contestRepository.create(contest);
    }

    @Override
    public String saveInCloudinary(MultipartFile photoAsFile) throws IOException {
        return cloudinaryHelper.uploadContestPhotoToCloudinary(photoAsFile);
    }

    @Override
    public void update(Contest contest, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsOrganizerOrAdmin(user);
        boolean duplicateExists = true;
        try {
            contestRepository.getByField("title", contest.getTitle());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("Contest", "title", contest.getTitle());
        }
        contestRepository.update(contest);
    }

    @Override
    public void delete(int id, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsOrganizerOrAdmin(user);
        Contest contest = getById(id);
        List<ContestPhoto> photos = contest.getContestPhotos();
        photos.forEach(contestPhoto -> contestPhoto.getReviews().clear());
        contest.getContestPhotos().clear();
        contest.getJuries().clear();
        contestRepository.delete(id);
    }
}
