package com.example.intellijproject.services;

import com.example.intellijproject.models.Notification;
import com.example.intellijproject.repositories.contracts.NotificaitonRepository;
import com.example.intellijproject.services.contracts.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationServiceImpl implements NotificationService {
    private final NotificaitonRepository notificationRepository;

    @Autowired
    public NotificationServiceImpl(NotificaitonRepository notificationRepository) {

        this.notificationRepository = notificationRepository;
    }

    @Override
    public List<Notification> getAll() {
        return notificationRepository.getAll();
    }

    @Override
    public Notification getById(int id) {
        return notificationRepository.getById(id);
    }

    @Override
    public List<Notification> getAllActiveUserNotifications(int userId) {
        return notificationRepository.getAllActiveUserNotifications(userId);
    }

    @Override
    public void create(Notification notification) {
        notificationRepository.create(notification);
    }

    @Override
    public void createBulk(List<Notification> notifications) {
        notifications.forEach(this::create);
    }

    @Override
    public void update(Notification notification) {
        notificationRepository.update(notification);
    }

    @Override
    public void delete(int id) {
        notificationRepository.delete(id);
    }
}
