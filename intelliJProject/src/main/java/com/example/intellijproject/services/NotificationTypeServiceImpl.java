package com.example.intellijproject.services;

import com.example.intellijproject.models.NotificationType;
import com.example.intellijproject.repositories.contracts.NotificationTypeRepository;
import com.example.intellijproject.services.contracts.NotificationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationTypeServiceImpl implements NotificationTypeService {
    private final NotificationTypeRepository notificationTypeRepository;

    @Autowired
    public NotificationTypeServiceImpl(NotificationTypeRepository notificationTypeRepository) {
        this.notificationTypeRepository = notificationTypeRepository;
    }

    @Override
    public List<NotificationType> getAll() {
        return notificationTypeRepository.getAll();
    }

    @Override
    public NotificationType getById(int id) {
        return notificationTypeRepository.getById(id);
    }

    @Override
    public void create(NotificationType notificationType) {
        notificationTypeRepository.create(notificationType);
    }

    @Override
    public void update(NotificationType notificationType) {
        notificationTypeRepository.update(notificationType);
    }

    @Override
    public void delete(int id) {
        notificationTypeRepository.delete(id);
    }
}
