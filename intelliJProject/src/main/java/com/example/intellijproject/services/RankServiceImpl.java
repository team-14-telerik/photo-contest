package com.example.intellijproject.services;

import com.example.intellijproject.models.Rank;
import com.example.intellijproject.repositories.contracts.RankRepository;
import com.example.intellijproject.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;

    @Autowired
    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    @Override
    public List<Rank> getAll() {
        return rankRepository.getAll();
    }

    @Override
    public Rank getById(int id) {
        return rankRepository.getById(id);
    }

    @Override
    public Rank getByRankName(String rankName) {
        return rankRepository.getByField("name", rankName);
    }

    public Rank getRankByPoints(int points) {
        Rank newRank = new Rank();
        for (Rank rank : getAll()) {
            if (rank.getMinScore() < points) {
                newRank = rank;
            } else {
                break;
            }
        }
        return newRank;
    }
}
