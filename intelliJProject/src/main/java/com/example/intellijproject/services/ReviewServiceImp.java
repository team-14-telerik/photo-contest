package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.Review;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ReviewRepository;
import com.example.intellijproject.services.contracts.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.example.intellijproject.utils.ValidationHelpers.*;

@Service
public class ReviewServiceImp implements ReviewService {

    public static final String REVIEW_EXISTS_ERR = "Review is already given!";
    private final ReviewRepository reviewRepository;

    @Autowired
    public ReviewServiceImp(ReviewRepository reviewRepository) {
        this.reviewRepository = reviewRepository;
    }

    @Override
    public List<Review> getAll() {
        return reviewRepository.getAll();
    }

    @Override
    public List<Review> getAllByContestId(int id) {
        return reviewRepository.getAllByContestId(id);
    }

    @Override
    public Review getById(int id) {
        return reviewRepository.getById(id);
    }

    @Override
    public void create(Review review, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        boolean duplicateExists = true;
        try {
            int contestPhotoId = review.getContestPhoto().getId();
            int userId = user.getId();
            reviewRepository.getByContestPhotoIdAndUserId(contestPhotoId, userId);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException(REVIEW_EXISTS_ERR);
        }
        reviewRepository.create(review);
    }

    @Override
    public void update(Review review, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        validateUserIsOrganizerOrOwner(user, review);
        reviewRepository.update(review);
    }

    @Override
    public void delete(int id, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        Review review = reviewRepository.getById(id);
        validateUserIsOrganizerOrOwner(user, review);
        reviewRepository.delete(id);
    }
}
