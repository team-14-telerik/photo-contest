package com.example.intellijproject.services;

import com.example.intellijproject.models.Role;
import com.example.intellijproject.repositories.contracts.RoleRepository;
import com.example.intellijproject.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }


    @Override
    public List<Role> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public Role getById(int id) {
        return roleRepository.getById(id);
    }

    @Override
    public Role getByRoleName(String roleName) {
        return roleRepository.getByField("name", roleName);
    }
}
