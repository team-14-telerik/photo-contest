package com.example.intellijproject.services;

import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ContestRepository;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.services.contracts.RankService;
import com.example.intellijproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
@EnableScheduling
public class ScoreServiceImpl {

    public static final int INVITATIONAL_POINTS = 3;
    public static final int OPEN_POINTS = 1;
    public static final int FIRST_PLACE_POINTS = 50;
    public static final int SECOND_PLACE_POINTS = 35;
    public static final int THIRD_PLACE_POINTS = 20;
    public static final int BONUS_FIRST_PLACE_POINTS = 75;
    public static final int SHARED_FIRST_PLACE_POINTS = 40;
    private static final int SHARED_SECOND_PLACE_POINTS = 25;
    public static final int SHARED_THIRD_PLACE_POINTS = 10;
    public static final int ONE_CONTESTNAT = 1;
    public static final String OPEN = "open";
    public static final int ONE_HOUR = 60 * 60 * 1000;
    private final ContestService contestService;
    private final UserService userService;
    private final RankService rankService;
    private final ContestRepository contestRepository;

    @Autowired
    public ScoreServiceImpl(ContestService contestService, UserService userService, RankService rankService, ContestRepository contestRepository) {
        this.contestService = contestService;
        this.userService = userService;
        this.rankService = rankService;
        this.contestRepository = contestRepository;
    }

    @Scheduled(fixedRate = ONE_HOUR)
    public void updateFinishingContests() {
        List<Contest> finishingContests = contestService.getAllFinishing();

        for (Contest finishingContest : finishingContests) {
            List<ContestPhoto> firstPlaces = new ArrayList<>();
            List<ContestPhoto> secondPlaces = new ArrayList<>();
            List<ContestPhoto> thirdPlaces = new ArrayList<>();

            findWinners(firstPlaces, secondPlaces, thirdPlaces, finishingContest);
            updateUserScore(firstPlaces, secondPlaces, thirdPlaces);
            updateContest(finishingContest);
        }
    }

    private void updateContest(Contest finishingContest) {
        finishingContest.setFinished(true);
        contestRepository.update(finishingContest);
    }

    private void updateUserScore(List<ContestPhoto> firstPlaces, List<ContestPhoto> secondPlaces, List<ContestPhoto> thirdPlaces) {
        int firstPlacePoints = FIRST_PLACE_POINTS;
        int secondPlacePoints = SECOND_PLACE_POINTS;
        int thirdPlacePoints = THIRD_PLACE_POINTS;

        if (!firstPlaces.isEmpty() && !secondPlaces.isEmpty()) {
            if (firstPlaces.get(0).getPoints() > secondPlaces.get(0).getPoints() * 2) {
                firstPlacePoints = BONUS_FIRST_PLACE_POINTS;
            }
        }
        if (firstPlaces.size() > ONE_CONTESTNAT) {
            firstPlacePoints = SHARED_FIRST_PLACE_POINTS;
        }
        if (secondPlaces.size() > ONE_CONTESTNAT) {
            secondPlacePoints = SHARED_SECOND_PLACE_POINTS;
        }
        if (thirdPlaces.size() > ONE_CONTESTNAT) {
            thirdPlacePoints = SHARED_THIRD_PLACE_POINTS;
        }
        for (ContestPhoto firstPlacePhoto : firstPlaces) {
            addPoints(firstPlacePhoto.getUser(), firstPlacePoints, firstPlacePhoto.getContest());
        }
        for (ContestPhoto secondPlacePhoto : secondPlaces) {
            addPoints(secondPlacePhoto.getUser(), secondPlacePoints, secondPlacePhoto.getContest());
        }
        for (ContestPhoto thirdPlacePhoto : thirdPlaces) {
            addPoints(thirdPlacePhoto.getUser(), thirdPlacePoints, thirdPlacePhoto.getContest());
        }
    }

    private void addPoints(User user, int points, Contest contest) {
        points += contest.getStatus().getName().equals(OPEN) ? OPEN_POINTS : INVITATIONAL_POINTS;
        user.setScore(user.getScore() + points);
        updateUserRanking(user);
        userService.update(user.getId(), user, user);
    }

    private void updateUserRanking(User user) {
        user.setRank(rankService.getRankByPoints(user.getScore()));
    }

    private void findWinners(List<ContestPhoto> firstPlaces, List<ContestPhoto> secondPlaces, List<ContestPhoto> thirdPlaces, Contest contest) {
        for (ContestPhoto photo : contest.getContestPhotos()) {
            orderPhotosByScore(photo, firstPlaces, secondPlaces, thirdPlaces);
        }
    }

    private void orderPhotosByScore(ContestPhoto photo, List<ContestPhoto> firstPlaces, List<ContestPhoto> secondPlaces, List<ContestPhoto> thirdPlaces) {
        if (firstPlaces.isEmpty()) {
            firstPlaces.add(photo);
            return;
        }

        if (firstPlaces.get(0).getPoints() == photo.getPoints()) {
            firstPlaces.add(photo);
            return;
        }

        if (secondPlaces.isEmpty()) {
            secondPlaces.add(photo);
            return;
        }

        if (secondPlaces.get(0).getPoints() == photo.getPoints()) {
            secondPlaces.add(photo);
            return;
        }

        if (thirdPlaces.isEmpty()) {
            thirdPlaces.add(photo);
            return;
        }

        if (thirdPlaces.get(0).getPoints() == photo.getPoints()) {
            thirdPlaces.add(photo);
        }

    }
}
