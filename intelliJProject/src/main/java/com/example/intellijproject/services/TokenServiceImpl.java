package com.example.intellijproject.services;

import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.repositories.contracts.TokenRepository;
import com.example.intellijproject.services.contracts.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    private final TokenRepository tokenRepository;

    @Autowired
    public TokenServiceImpl(TokenRepository tokenRepository) {
        this.tokenRepository = tokenRepository;
    }

    @Override
    public ConfirmationToken findByToken(String token) {
        return tokenRepository.getByField("token", token);
    }

    @Override
    public void saveConfirmationToken(ConfirmationToken token) {
        tokenRepository.create(token);
    }

    @Override
    public void updateConfirmationToken(ConfirmationToken token) {
        tokenRepository.update(token);
    }
}
