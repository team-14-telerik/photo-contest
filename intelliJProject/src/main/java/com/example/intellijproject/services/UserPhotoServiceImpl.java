package com.example.intellijproject.services;


import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.UserPhoto;
import com.example.intellijproject.repositories.contracts.UserPhotoRepository;
import com.example.intellijproject.services.contracts.UserPhotoService;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.CloudinaryHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class UserPhotoServiceImpl implements UserPhotoService {

    public static final String USER_LACKING_PROFILE_ERR = "User with id %d does not have profile photo!";
    private final UserPhotoRepository userPhotoRepository;
    private final CloudinaryHelper cloudinaryHelper;
    private final UserService userService;

    @Autowired
    public UserPhotoServiceImpl(UserPhotoRepository userPhotoRepository, CloudinaryHelper cloudinaryHelper, UserService userService) {
        this.userPhotoRepository = userPhotoRepository;
        this.cloudinaryHelper = cloudinaryHelper;
        this.userService = userService;
    }


    @Override
    public User save(MultipartFile photoAsFile, int userID) throws IOException {
        User user = userService.getById(userID);

        String photoURL = cloudinaryHelper.uploadToCloudinary(photoAsFile, user);

        UserPhoto photo = new UserPhoto();
        photo.setPhotoUrl(photoURL);
        photo.setUser(user);

        userPhotoRepository.create(photo);
        return userService.getById(userID);
    }


    @Override
    public User update(MultipartFile photoAsFile, int userID) throws IOException {
        User user = userService.getById(userID);
        String photoURL = cloudinaryHelper.uploadToCloudinary(photoAsFile, user);

        UserPhoto photo = new UserPhoto();
        photo.setPhotoUrl(photoURL);
        photo.setUser(user);

        userPhotoRepository.update(photo);

        return userService.getById(userID);
    }


    @Override
    public User delete(int userID) throws Exception {
        User user = userService.getById(userID);

        cloudinaryHelper.deleteFromCloudinary(userID);

        int photoID;
        try {
            photoID = user.getUserPhoto().getId();
        } catch (NullPointerException e) {
            throw new EntityNotFoundException(String.format(
                    USER_LACKING_PROFILE_ERR, userID));
        }

        userPhotoRepository.delete(photoID);

        return userService.getById(userID);
    }

}
