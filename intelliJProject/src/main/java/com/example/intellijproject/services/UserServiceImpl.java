package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.AuthenticationFailureException;
import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.UserRepository;
import com.example.intellijproject.services.contracts.TokenService;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.EmailHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static com.example.intellijproject.utils.ValidationHelpers.*;

@Service
public class UserServiceImpl implements UserService {

    public static final String USER_ALREADY_ADMIN_ERR = "User with id %d is already an admin!";
    public static final String USER_NOT_ADMIN_ERROR = "User with id %d is not an admin and cannot be demoted!";
    public static final String USER_NO_FURTHER_PROMOTION_ERR = "User with id %d is already and organizer and cannot be promoted further!";
    public static final String USER_NO_FURTHER_DEMOTION_ERR = "User with id %d is already a photo junkie and cannot be demoted further!";
    public static final String WRONG_USERNAME_OR_PASSWORD_ERR = "Wrong username or password";
    public static final String PROFILE_ACTIVATION_ERR = "You need to activate your profile first!";
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final TokenService tokenService;


    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder,
                           TokenService tokenService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.tokenService = tokenService;
    }

    @Override
    public List<User> getAll(Optional<String> username, Optional<String> firstname,
                             Optional<String> lastname, Optional<String> email,
                             Optional<String> userRole, Optional<String> userRank,
                             Optional<Integer> minScore, Optional<Integer> maxScore,
                             Optional<String> sortBy, Optional<String> sortOrder) {
        return userRepository.getAll(username, firstname, lastname, email, userRole,
                userRank, minScore, maxScore, sortBy, sortOrder);
    }

    @Override
    public User getById(int id) {
        validateID(id);
        User user = userRepository.getById(id);
        if (user.isDeleted()) {
            throw new EntityNotFoundException("User", id);
        }
        return user;
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByField("email", email);
    }

    public User getByUsername(String username) {
        return userRepository.getByField("username", username);
    }

    @Override
    public List<User> getAllAdmins() {
        return userRepository.getByRole("admin");
    }

    @Override
    public List<User> getAllOrganizers() {
        return userRepository.getByRole("organizer");
    }

    @Override
    public List<User> getAllJunkies() {
        return userRepository.getByRole("photo_junkie");
    }

    @Override
    public List<User> getTopJunkies(Optional<Integer> count) {
        return userRepository.getTopJunkies(count);
    }

    @Override
    public User makeAdmin(User currentUser, int userToPromoteID) {
        validateID(userToPromoteID);
        validateIsAdmin(currentUser);
        User userToPromote = getById(userToPromoteID);
        validateUserIsBlocked(userToPromote);

        if (userToPromote.getRole().getName().equalsIgnoreCase(ADMIN)) {
            throw new DuplicateEntityException(String.format(USER_ALREADY_ADMIN_ERR, userToPromoteID));
        }

        userToPromote.getRole().setId(Role.ADMIN_ROLE_ID);
        userRepository.update(userToPromote);
        return getById(userToPromoteID);
    }

    @Override
    public User demoteAdmin(User currentUser, int userToDemoteID) {
        validateID(userToDemoteID);
        validateIsAdmin(currentUser);
        User userToDemote = getById(userToDemoteID);
        validateUserIsBlocked(userToDemote);
        try {
            validateIsAdmin(userToDemote);
        } catch (UnauthorizedOperationException e) {
            throw new DuplicateEntityException(String.format(
                    USER_NOT_ADMIN_ERROR, userToDemoteID));
        }
        userToDemote.getRole().setId(Role.PHOTO_JUNKIE_ROLE_ID);
        userRepository.update(userToDemote);
        return getById(userToDemoteID);
    }


    @Override
    public User updateUserBlockStatus(User currentUser, int userToBeBlockedUnblockedID) {
        User userToBeUpdated = getById(userToBeBlockedUnblockedID);
        validateIsAdmin(currentUser);
        boolean newBlockedStatus = !userToBeUpdated.isBlocked();
        userToBeUpdated.setBlocked(newBlockedStatus);
        userRepository.update(userToBeUpdated);
        return getById(userToBeBlockedUnblockedID);
    }

    @Override
    public User create(User user) {
        boolean duplicateExists = true;
        try {
            userRepository.getByField("username", user.getUsername());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", user.getUsername());
        }
        duplicateExists = true;
        try {
            userRepository.getByField("email", user.getEmail());
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", user.getEmail());
        }
        User createdUser = userRepository.create(user);
        generateResetToken(createdUser);
        return getById(user.getId());
    }

    @Override
    public User update(int id, User userUpdate, User currentUser) {
        validateUserIsDeleted(userUpdate);
        validateID(id);
        validateIfOwner(currentUser, userUpdate);
        boolean duplicateExists = true;
        try {
            User existingUserWithGivenUsername = userRepository.getByField("username", userUpdate.getUsername());
            if (existingUserWithGivenUsername.getId() == id) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "username", userUpdate.getUsername());
        }
        duplicateExists = true;
        try {
            User existingUserWithGivenEmail = userRepository.getByField("email", userUpdate.getEmail());
            if (existingUserWithGivenEmail.getId() == id) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }
        if (duplicateExists) {
            throw new DuplicateEntityException("User", "email", userUpdate.getEmail());
        }
        userRepository.update(userUpdate);
        return userRepository.getById(userUpdate.getId());
    }

    @Override
    public void delete(User requester, int id) {
        validateUserIsBlocked(requester);
        validateUserIsDeleted(requester);
        validateID(id);
        validateIfOwnerOrAdmin(requester, id);
        User targetUserToDelete = userRepository.getById(id);
        targetUserToDelete.setDeleted(true);
        userRepository.update(targetUserToDelete);
    }

    @Override
    public User updateRole(User executingUser, int id, Role roleToUpdate) {
        User userToGetRole = getById(id);
        validateIsAdmin(executingUser);
        userToGetRole.setRole(roleToUpdate);
        userRepository.update(userToGetRole);
        return userToGetRole;
    }

    @Override
    public User makeOrganizer(User currentUser, int userToPromoteID) {
        validateID(userToPromoteID);
        validateIsAdmin(currentUser);
        User userToPromote = getById(userToPromoteID);
        validateUserIsBlocked(userToPromote);
        validateUserIsDeleted(userToPromote);
        if (userToPromote.getRole().getName().equalsIgnoreCase(Role.ORGANIZER)) {
            throw new DuplicateEntityException(String.format(
                    USER_NO_FURTHER_PROMOTION_ERR, userToPromoteID));
        }
        userToPromote.getRole().setId(Role.ORGANIZER_ROLE_ID);
        userRepository.update(userToPromote);
        return getById(userToPromoteID);
    }

    @Override
    public User demoteOrganizer(User currentUser, int userToDemoteID) {
        validateID(userToDemoteID);
        validateIsAdmin(currentUser);
        User userToDemote = getById(userToDemoteID);
        validateUserIsBlocked(userToDemote);
        validateUserIsDeleted(userToDemote);
        if (userToDemote.getRole().getName().equalsIgnoreCase(Role.PHOTO_JUNKIE)) {
            throw new DuplicateEntityException(String.format(
                    USER_NO_FURTHER_DEMOTION_ERR, userToDemoteID));

        }
        userToDemote.getRole().setId(Role.PHOTO_JUNKIE_ROLE_ID);
        userRepository.update(userToDemote);
        return getById(userToDemoteID);
    }

    @Override
    public boolean isUserJury(int contestId, int userId) {
        return userRepository.isUserJury(contestId, userId);
    }

    @Override
    public void changeUserPassword(String oldPassword, String newPassword, User user) {
        validateUserIsBlocked(user);
        validateUserIsDeleted(user);
        user.setPassword(passwordEncoder.encode(newPassword));
        userRepository.update(user);
    }

    @Override
    public User login(User user, String password) {
        validateUserIsDeleted(user);
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new AuthenticationFailureException(WRONG_USERNAME_OR_PASSWORD_ERR);
        }
        if (!user.isActive()) {
            throw new AuthenticationFailureException(PROFILE_ACTIVATION_ERR);
        }
        return user;
    }

    public String generateResetToken(User user) {
        String token = UUID.randomUUID().toString();
        ConfirmationToken confirmationToken = new ConfirmationToken(token,
                LocalDateTime.now(),
                LocalDateTime.now().plusMinutes(15),
                user);

        tokenService.saveConfirmationToken(confirmationToken);
        sendEmailWithToken(user, token);
        return token;
    }

    private void sendEmailWithToken(User user, String token) {
        String link = "http://localhost:8080/public/home?activationToken=" + token;
        EmailHelper.send(user.getEmail(), buildEmail(user.getFirstName(), link));
    }

    private String buildEmail(String name, String link) {
        return "<div style=\"font-family:Helvetica,Arial,sans-serif;font-size:16px;margin:0;color:#0b0c0c\">\n" +
                "\n" +
                "<span style=\"display:none;font-size:1px;color:#fff;max-height:0\"></span>\n" +
                "\n" +
                "  <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;min-width:100%;width:100%!important\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"100%\" height=\"53\" bgcolor=\"#0b0c0c\">\n" +
                "        \n" +
                "        <table role=\"presentation\" width=\"100%\" style=\"border-collapse:collapse;max-width:580px\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" align=\"center\">\n" +
                "          <tbody><tr>\n" +
                "            <td width=\"70\" bgcolor=\"#0b0c0c\" valign=\"middle\">\n" +
                "                <table role=\"presentation\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td style=\"padding-left:10px\">\n" +
                "                  \n" +
                "                    </td>\n" +
                "                    <td style=\"font-size:28px;line-height:1.315789474;Margin-top:4px;padding-left:10px\">\n" +
                "                      <span style=\"font-family:Helvetica,Arial,sans-serif;font-weight:700;color:#ffffff;text-decoration:none;vertical-align:top;display:inline-block\">Confirm your email</span>\n" +
                "                    </td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "              </a>\n" +
                "            </td>\n" +
                "          </tr>\n" +
                "        </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td width=\"10\" height=\"10\" valign=\"middle\"></td>\n" +
                "      <td>\n" +
                "        \n" +
                "                <table role=\"presentation\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse\">\n" +
                "                  <tbody><tr>\n" +
                "                    <td bgcolor=\"#1D70B8\" width=\"100%\" height=\"10\"></td>\n" +
                "                  </tr>\n" +
                "                </tbody></table>\n" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\" height=\"10\"></td>\n" +
                "    </tr>\n" +
                "  </tbody></table>\n" +
                "\n" +
                "\n" +
                "\n" +
                "  <table role=\"presentation\" class=\"m_-6186904992287805515content\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" style=\"border-collapse:collapse;max-width:580px;width:100%!important\" width=\"100%\">\n" +
                "    <tbody><tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "      <td style=\"font-family:Helvetica,Arial,sans-serif;font-size:19px;line-height:1.315789474;max-width:560px\">\n" +
                "        \n" +
                "            <p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\">Hi " + name + ",</p><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> Thank you for registering. Please click on the below link to activate your account: </p><blockquote style=\"Margin:0 0 20px 0;border-left:10px solid #b1b4b6;padding:15px 0 0.1px 15px;font-size:19px;line-height:25px\"><p style=\"Margin:0 0 20px 0;font-size:19px;line-height:25px;color:#0b0c0c\"> <a href=\"" + link + "\">Activate Now</a> </p></blockquote>\n   <p>See you soon</p>" +
                "        \n" +
                "      </td>\n" +
                "      <td width=\"10\" valign=\"middle\"><br></td>\n" +
                "    </tr>\n" +
                "    <tr>\n" +
                "      <td height=\"30\"><br></td>\n" +
                "    </tr>\n" +
                "  </tbody></table><div class=\"yj6qo\"></div><div class=\"adL\">\n" +
                "\n" +
                "</div></div>";
    }

}
