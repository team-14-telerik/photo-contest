package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Category;
import com.example.intellijproject.models.User;

import java.util.List;

public interface CategoryService {

    List<Category> getAll();

    Category getById(int id);

    Category getByName(String name);

    void create(Category category, User user);

    void update(Category category, User user);

    void delete(int id, User user);
}
