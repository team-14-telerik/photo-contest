package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ContestPhotoService {

    List<ContestPhoto> getAll();

    ContestPhoto getById(int id);

    List<ContestPhoto> getFiltered(Optional<Integer> limit, Optional<String> sortBy, Optional<String> sortOrder);

    List<ContestPhoto> getAllByContestId(int id);

    List<ContestPhoto> getAllByUserId(int id);

    void create(ContestPhoto contestPhoto, User user);

    String savePhoto(MultipartFile photoAsFile, int contestId, User user) throws IOException;

    void delete(int contestId, int contestPhotoId, User user);

    List<ContestPhoto> getAllByContestIdSorted(int id);

    List<ContestPhoto> getEightMostPopular();

    boolean isNotReviewed(ContestPhoto contestPhoto, User user);

    List<ContestPhoto> getAllFiltered(Optional<String> title);
}
