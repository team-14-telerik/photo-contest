package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

public interface ContestService {
    List<Contest> getAllFiltered(Optional<String> title, Optional<String> category, Optional<String> status,
                                 Optional<String> phase, Optional<String> sort, Optional<String> finished,
                                 Optional<String> username);

    List<Contest> getTenNewest();

    List<Contest> getAllJuriedByUserId(int id);

    Contest getById(int id);

    void create(Contest contest, User user);

    String saveInCloudinary(MultipartFile photoAsFile) throws IOException;

    void update(Contest contest, User user);

    void delete(int id, User user);

    List<Contest> getAllPhaseOne();

    List<Contest> getAllPhaseTwo();

    List<Contest> getAllFinished();

    List<Contest> getAllOpen();

    List<Contest> getAllByUserId(int id);

    List<Contest> getAllFinishedByUserId(int id);

    List<Contest> getAllFinishing();
}
