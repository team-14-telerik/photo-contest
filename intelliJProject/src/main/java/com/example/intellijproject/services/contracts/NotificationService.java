package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Notification;

import java.util.List;

public interface NotificationService {

    List<Notification> getAll();

    Notification getById(int id);

    List<Notification> getAllActiveUserNotifications(int userId);

    void create(Notification notification);

    void createBulk(List<Notification> notifications);

    void update(Notification notification);

    void delete(int id);
}
