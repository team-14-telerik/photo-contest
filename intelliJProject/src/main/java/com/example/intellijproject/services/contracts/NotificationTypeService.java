package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.NotificationType;

import java.util.List;

public interface NotificationTypeService {

    List<NotificationType> getAll();

    NotificationType getById(int id);

    void create(NotificationType notificationType);

    void update(NotificationType notificationType);

    void delete(int id);
}
