package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Rank;

import java.util.List;

public interface RankService {
    List<Rank> getAll();

    Rank getById(int id);

    Rank getByRankName(String rankName);

    Rank getRankByPoints(int points);
}
