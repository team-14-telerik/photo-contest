package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.User;

public interface ResetPasswordService {

    String generateResetToken(User user);
}
