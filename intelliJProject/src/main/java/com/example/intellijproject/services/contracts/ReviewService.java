package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Review;
import com.example.intellijproject.models.User;

import java.util.List;

public interface ReviewService {

    List<Review> getAll();

    List<Review> getAllByContestId(int id);

    Review getById(int id);

    void create(Review review, User user);

    void update(Review review, User user);

    void delete(int id, User user);
}
