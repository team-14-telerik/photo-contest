package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Role;

import java.util.List;

public interface RoleService {
    List<Role> getAll();

    Role getById(int id);

    Role getByRoleName(String roleName);
}
