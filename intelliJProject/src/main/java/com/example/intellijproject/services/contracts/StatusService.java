package com.example.intellijproject.services.contracts;


import com.example.intellijproject.models.Status;

import java.util.List;

public interface StatusService {
    List<Status> getAll();

    public Status getById(int id);
}
