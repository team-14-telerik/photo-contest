package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.ConfirmationToken;

public interface TokenService {

    ConfirmationToken findByToken(String token);

    void saveConfirmationToken(ConfirmationToken token);

    void updateConfirmationToken(ConfirmationToken token);
}