package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.User;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserPhotoService {
    User save(MultipartFile photoAsFile, int userID) throws IOException;

    User update(MultipartFile photoAsFile, int userID) throws IOException;

    User delete(int userID) throws Exception;
}
