package com.example.intellijproject.services.contracts;

import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.User;

import java.util.List;
import java.util.Optional;

public interface UserService {
    List<User> getAll(Optional<String> username, Optional<String> firstname,
                      Optional<String> lastname, Optional<String> email,
                      Optional<String> userRole, Optional<String> userRank,
                      Optional<Integer> minScore, Optional<Integer> maxScore,
                      Optional<String> sortBy, Optional<String> sortOrder);

    User getById(int id);

    User getByEmail(String email);

    User create(User user);

    User update(int id, User userUpdate, User currentUser);

    void delete(User requester, int id);

    User getByUsername(String username);

    List<User> getAllAdmins();

    List<User> getAllOrganizers();

    List<User> getAllJunkies();

    List<User> getTopJunkies(Optional<Integer> count);

    User makeAdmin(User currentUser, int userToPromoteID);

    User demoteAdmin(User currentUser, int id);

    User updateUserBlockStatus(User currentUser, int id);

    User makeOrganizer(User currentUser, int id);

    User demoteOrganizer(User currentUser, int id);

    boolean isUserJury(int contestId, int userId);

    void changeUserPassword(String oldPassword, String newPassword, User user);

    User updateRole(User executingUser, int id, Role roleToUpdate);

    User login(User user, String password);
}
