package com.example.intellijproject.utils;

import com.example.intellijproject.models.Category;
import com.example.intellijproject.models.dtos.CategoryDTO;
import com.example.intellijproject.services.contracts.CategoryService;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper {
    private final CategoryService categoryService;

    public CategoryMapper(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    public Category fromDto(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setName(categoryDTO.getName());
        return category;
    }

    public Category fromDto(CategoryDTO categoryDTO, int existingCategoryId) {
        Category categoryToBeUpdated = categoryService.getById(existingCategoryId);
        categoryToBeUpdated.setName(categoryDTO.getName());
        return categoryToBeUpdated;
    }
}
