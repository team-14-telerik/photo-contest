package com.example.intellijproject.utils;

import com.cloudinary.Cloudinary;
import com.cloudinary.utils.ObjectUtils;
import com.example.intellijproject.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Map;

@Component
public class CloudinaryHelper {

    public static final String CLOUDINARY_PUBLIC_USER_ID = "PhotoContest/ProfilePhotos/userID:";
    public static final String CLOUDINARY_PUBLIC_CONTEST_ID = "PhotoContest/ContestPhotos/contestPhotoID:";
    private final Cloudinary cloudinary;

    @Autowired
    public CloudinaryHelper(Environment env) {
        String cloudName = env.getProperty("cloud_name");
        String apiKey = env.getProperty("api_key");
        String apiSecret = env.getProperty("api_secret");

        cloudinary = new Cloudinary(ObjectUtils.asMap(
                "cloud_name", cloudName,
                "api_key", apiKey,
                "api_secret", apiSecret,
                "secure", true));
    }

    public String uploadToCloudinary(MultipartFile photoAsFile, User user) throws IOException {
        var params = ObjectUtils.asMap(
                "public_id", CLOUDINARY_PUBLIC_USER_ID + user.getId(),
                "overwrite", true,
                "resource_type", "image",
                "tag", "userID:" + user.getId()
        );

        var uploadResult = cloudinary.uploader().upload(photoAsFile.getBytes(), params);

        return uploadResult.get("url").toString();
    }

    public void deleteFromCloudinary(long userID) throws Exception {
        String assetName = CLOUDINARY_PUBLIC_USER_ID + userID;

        cloudinary.uploader().destroy(assetName, Map.of());
    }


    public String uploadContestPhotoToCloudinary(MultipartFile photoAsFile) throws IOException {
        var params = ObjectUtils.asMap(
                "public_id", CLOUDINARY_PUBLIC_CONTEST_ID + photoAsFile.getOriginalFilename(),
                "overwrite", true,
                "resource_type", "image",
                "tag", "contestPhotoName:" + photoAsFile.getOriginalFilename()
        );

        var uploadResult = cloudinary.uploader().upload(photoAsFile.getBytes(), params);

        return uploadResult.get("url").toString();
    }
}
