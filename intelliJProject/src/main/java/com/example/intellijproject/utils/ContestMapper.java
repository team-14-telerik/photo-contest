package com.example.intellijproject.utils;

import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.models.Category;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.Status;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.ContestDTO;
import com.example.intellijproject.models.dtos.ContestMvcDTO;
import com.example.intellijproject.services.contracts.*;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.Date;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

@Component
public class ContestMapper {

    private final ContestService contestService;
    private final CategoryService categoryService;
    private final StatusService statusService;
    private final CloudinaryHelper cloudinaryHelper;
    private final UserService userService;

    public ContestMapper(ContestService contestService, CategoryService categoryService, StatusService statusService, CloudinaryHelper cloudinaryHelper, UserService userService) {
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.statusService = statusService;
        this.cloudinaryHelper = cloudinaryHelper;
        this.userService = userService;
    }

    public Contest dtoToObject(ContestDTO contestDTO) {
        Category category = categoryService.getById(contestDTO.getCategoryId());
        Status status = statusService.getById(contestDTO.getStatusId());
        Contest contest = new Contest();
        contest.setTitle(contestDTO.getTitle());
        contest.setCategory(category);
        contest.setStatus(status);
        contest.setPhotoUrl(contestDTO.getPhotoUrl());
        contest.setDeadlinePhaseOne(contestDTO.getDeadlinePhaseOne());
        contest.setDeadlinePhaseTwo(Instant.ofEpochMilli(contestDTO.getDeadlinePhaseTwo().getTime())
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime());
        contest.setFinished(false);
        contest.setContestPhotos(new ArrayList<>());
        contest.setJuries(new HashSet<>());
        return contest;
    }

    public Contest dtoToObject(ContestDTO contestDTO, int id) {
        Category category = categoryService.getById(contestDTO.getCategoryId());
        Status status = statusService.getById(contestDTO.getStatusId());
        Contest contest = contestService.getById(id);
        contest.setTitle(contestDTO.getTitle());
        contest.setCategory(category);
        contest.setStatus(status);
        contest.setPhotoUrl(contestDTO.getPhotoUrl());
        return contest;
    }

    public Contest mvcDTOToObject(ContestMvcDTO contestMvcDTO) throws IOException {
        Contest contest = new Contest();
        Category category = categoryService.getById(contestMvcDTO.getCategoryId());
        Status status = statusService.getById(contestMvcDTO.getStatusId());
        contest.setCategory(category);
        contest.setStatus(status);

        contest.setTitle(contestMvcDTO.getTitle());
        contest.setDeadlinePhaseOne(Date.valueOf(LocalDate.now().plusDays(contestMvcDTO.getDeadlinePhaseOne())));
        LocalDateTime phaseTwo = LocalDate.now().plusDays(contestMvcDTO.getDeadlinePhaseOne())
                .atStartOfDay().plusHours(contestMvcDTO.getDeadlinePhaseTwo());
        contest.setDeadlinePhaseTwo(phaseTwo);
        contest.setFinished(false);
        contest.setContestPhotos(new ArrayList<>());
        if (contestMvcDTO.getJury().isEmpty()) {
            contest.setJuries(new HashSet<>());
        } else {
            initializeJurySetInContest(contestMvcDTO, contest);
        }
        if (!contestMvcDTO.getPhotoUrl().isEmpty()) {
            contest.setPhotoUrl(contestMvcDTO.getPhotoUrl());
        } else if (contestMvcDTO.getOldContestPhotoUrl() != null) {
            contest.setPhotoUrl(contestMvcDTO.getOldContestPhotoUrl());
        } else if (!contestMvcDTO.getMultipartPhoto().isEmpty()) {
            contest.setPhotoUrl(cloudinaryHelper.uploadContestPhotoToCloudinary(contestMvcDTO.getMultipartPhoto()));
        }
        return contest;
    }

    private void initializeJurySetInContest(ContestMvcDTO contestMvcDTO, Contest contest) {
        String[] juryNames = contestMvcDTO.getJury().split(";");
        Set<User> jury = new HashSet<>();
        for (String username : juryNames) {
            User user = new User();
            try {
                user = userService.getByUsername(username);
                jury.add(user);
            } catch (EntityNotFoundException ignored) {

            }
        }
        contest.setJuries(jury);
    }
}

