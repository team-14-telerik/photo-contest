package com.example.intellijproject.utils;

import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.ContestPhotoDTO;
import com.example.intellijproject.models.dtos.ContestPhotoMvcDTO;
import com.example.intellijproject.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ContestPhotoMapper {

    private final ContestService contestService;

    @Autowired
    public ContestPhotoMapper(ContestService contestService) {
        this.contestService = contestService;
    }

    public ContestPhoto dtoToObject(ContestPhotoDTO contestPhotoDTo, int id, User user) {
        ContestPhoto contestPhoto = new ContestPhoto();
        Contest contest = contestService.getById(id);
        contestPhoto.setContest(contest);
        contestPhoto.setUser(user);
        contestPhoto.setTitle(contestPhotoDTo.getTitle());
        contestPhoto.setPhotoUrl(contestPhotoDTo.getPhotoUrl());
        contestPhoto.setStory(contestPhotoDTo.getStory());
        return contestPhoto;
    }

    public ContestPhoto dtoToObject(ContestPhotoMvcDTO contestPhotoMvcDTo, int id, User user, String photoUrl) {
        ContestPhoto contestPhoto = new ContestPhoto();
        Contest contest = contestService.getById(id);
        contestPhoto.setContest(contest);
        contestPhoto.setUser(user);
        contestPhoto.setTitle(contestPhotoMvcDTo.getTitle());
        contestPhoto.setPhotoUrl(photoUrl);
        contestPhoto.setStory(contestPhotoMvcDTo.getStory());
        return contestPhoto;
    }
}
