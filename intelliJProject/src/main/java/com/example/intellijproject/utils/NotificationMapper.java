package com.example.intellijproject.utils;

import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.Notification;
import com.example.intellijproject.models.NotificationType;
import com.example.intellijproject.models.User;
import com.example.intellijproject.services.contracts.NotificationTypeService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Component
public class NotificationMapper {
    public static final int JURY_INVITATION_TYPE_ID = 1;
    public static final int CONTEST_INVITATION_TYPE = 2;
    private final NotificationTypeService notificationTypeService;
    public static final String JURY_NOTIFICATION_MESSAGE = "You have been selected to be jury for this contest. Review wisely!";
    public static final String CONTEST_PARTICIPATION_NOTIFICATION_MESSAGE = "You have been invited to participate in this contest. Good luck!";

    public NotificationMapper(NotificationTypeService notificationTypeService) {
        this.notificationTypeService = notificationTypeService;
    }

    public List<Notification> createJuryNotifications(Set<User> jury, Contest contest) {
        List<Notification> notifications = new ArrayList<>();
        for (User user : jury) {
            Notification notification = new Notification();
            notification.setMessage(JURY_NOTIFICATION_MESSAGE);
            notification.setCreationDate(LocalDateTime.now());
            notification.setRead(false);
            notification.setUser(user);
            NotificationType notificationType = notificationTypeService.getById(JURY_INVITATION_TYPE_ID);
            notification.setType(notificationType);
            notification.setContest(contest);
            notifications.add(notification);
        }
        return notifications;
    }

    public List<Notification> createContestInvitationNotification(Set<User> invitedUsers, Contest contest) {
        List<Notification> notifications = new ArrayList<>();
        for (User user : invitedUsers) {
            Notification notification = new Notification();
            notification.setMessage(CONTEST_PARTICIPATION_NOTIFICATION_MESSAGE);
            notification.setCreationDate(LocalDateTime.now());
            notification.setRead(false);
            notification.setUser(user);
            NotificationType notificationType = notificationTypeService.getById(CONTEST_INVITATION_TYPE);
            notification.setType(notificationType);
            notification.setContest(contest);
            notifications.add(notification);
        }
        return notifications;
    }
}
