package com.example.intellijproject.utils;

import com.example.intellijproject.models.ContestPhoto;
import com.example.intellijproject.models.Review;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.ReviewDTO;
import com.example.intellijproject.models.dtos.UpdateReviewDTO;
import com.example.intellijproject.services.contracts.ContestPhotoService;
import com.example.intellijproject.services.contracts.ReviewService;
import org.springframework.stereotype.Component;

@Component
public class ReviewMapper {
    private final ReviewService reviewService;
    private final ContestPhotoService contestPhotoService;

    public ReviewMapper(ReviewService reviewService, ContestPhotoService contestPhotoService) {
        this.reviewService = reviewService;
        this.contestPhotoService = contestPhotoService;
    }

    public Review fromDto(ReviewDTO reviewDTO, User user, int contestPhotoId) {
        Review review = new Review();
        ContestPhoto contestPhoto = contestPhotoService.getById(contestPhotoId);
        review.setUser(user);
        review.setScore(reviewDTO.getScore());
        review.setComment(reviewDTO.getComment());
        review.setContestPhoto(contestPhoto);
        return review;
    }

    public Review fromDto(int reviewId, UpdateReviewDTO updateReviewDTO) {
        Review review = reviewService.getById(reviewId);
        review.setComment(updateReviewDTO.getComment());
        review.setScore(updateReviewDTO.getScore());
        return review;
    }
}
