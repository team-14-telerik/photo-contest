package com.example.intellijproject.utils;

import com.example.intellijproject.models.Rank;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.dtos.UserDTOIn;
import com.example.intellijproject.models.dtos.UserDTOOUTAdmin;
import com.example.intellijproject.models.dtos.UserDTOOut;
import com.example.intellijproject.models.dtos.UserDTOOutOwner;
import com.example.intellijproject.services.contracts.RankService;
import com.example.intellijproject.services.contracts.RoleService;
import com.example.intellijproject.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class UserMapper {

    private final UserService userService;
    private final RankService rankService;
    private final RoleService roleService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserMapper(UserService userService, RankService rankService, RoleService roleService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.rankService = rankService;
        this.roleService = roleService;
        this.passwordEncoder = passwordEncoder;
    }


    public User createFromDto(UserDTOIn userDTOIn) {
        User user = new User();
        user.setUsername(userDTOIn.getUsername());
        user.setRegistrationDate(LocalDateTime.now());
        user.setRank(rankService.getByRankName(Rank.JUNKIE));
        user.setRole(roleService.getByRoleName(Role.PHOTO_JUNKIE));
        dtoToObject(userDTOIn, user);
        return user;
    }

    public User updateFromDto(UserDTOIn userDTOIn, int id) {
        User user = userService.getById(id);
        dtoToObject(userDTOIn, user);
        return user;
    }

    private void dtoToObject(UserDTOIn userDTOIn, User user) {
        user.setFirstName(userDTOIn.getFirstName());
        user.setLastName(userDTOIn.getLastName());
        user.setPassword(passwordEncoder.encode(userDTOIn.getPassword()));
        user.setEmail(userDTOIn.getEmail());
    }

    public UserDTOOutOwner UserToDTOOutOwner(User user) {
        return new UserDTOOutOwner(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getPassword(),
                user.getEmail(),
                user.getRegistrationDate(),
                user.getUserPhoto(),
                user.getRole(),
                user.getRank(),
                user.getScore());
    }

    public UserDTOOut UserToDTOOut(User user) {
        return new UserDTOOut(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getRegistrationDate(),
                user.getUserPhoto(),
                user.getRole(),
                user.getRank(),
                user.getScore()
        );
    }

    public UserDTOOUTAdmin UserToDTOOutAdmin(User user) {
        return new UserDTOOUTAdmin(
                user.getId(),
                user.getUsername(),
                user.getFirstName(),
                user.getLastName(),
                user.getEmail(),
                user.getRegistrationDate(),
                user.getUserPhoto(),
                user.getRole(),
                user.getRank(),
                user.getScore(),
                user.isDeleted(),
                user.isBlocked()
        );
    }
}
