package com.example.intellijproject.utils;


import com.example.intellijproject.models.Rank;

import java.security.InvalidParameterException;
import java.util.*;
import java.util.stream.Collectors;

public class UsersQueryMaker {

    public static final String[] PERMITTED_SORT_CRITERIA = {"username", "id", "firstName",
            "lastName", "email", "registrationDate", "score"};

    public static final String[] PERMITTED_USER_ROLES = {
            ValidationHelpers.ADMIN,
            ValidationHelpers.ORGANIZER_ROLE,
            ValidationHelpers.PHOTO_JUNKIE
    };

    public static final String[] PERMITTED_USER_RANKS = {
            Rank.JUNKIE, Rank.ENTHUSIAST, Rank.MASTER, Rank.DICTATOR
    };

    private final StringBuilder query;
    private final List<String> filters;
    private final HashMap<String, Object> propertiesMap;

    public UsersQueryMaker(String query) {
        this.query = new StringBuilder(query);
        filters = new ArrayList<>();
        propertiesMap = new HashMap<>();
    }


    public String buildHQLSearchAndSortQuery(Optional<String> username, Optional<String> firstName,
                                             Optional<String> lastName, Optional<String> email,
                                             Optional<String> userRole, Optional<String> userRank,
                                             Optional<Integer> minScore, Optional<Integer> maxScore,
                                             Optional<String> sortBy, Optional<String> sortOrder) {
        buildLikeSearch(filters, propertiesMap, "username", username);
        buildLikeSearch(filters, propertiesMap, "first_name", firstName);
        buildLikeSearch(filters, propertiesMap, "last_name", lastName);
        buildLikeSearch(filters, propertiesMap, "email", email);

        userRole.ifPresent(value -> {
                    if (!isValidUserRoleParameter(value)) {
                        throw new InvalidParameterException("User role can only be one of 'admin', 'organizer' " +
                                "or 'photo_junkie'");
                    }
                }
        );
        buildExactSearch(filters, propertiesMap, "role_name", userRole);

        userRank.ifPresent(value -> {
                    if (!isValidUserRankParameter(value)) {
                        throw new InvalidParameterException("User rank can only be one of 'junkie', 'enthusiast' ," +
                                " 'master' or 'dictator'");
                    }
                }
        );
        buildExactSearch(filters, propertiesMap, "rank_name", userRank);

        minScore.ifPresent(value -> {
            filters.add(" score >= :minScore ");
            propertiesMap.put("minScore", value);
        });

        maxScore.ifPresent(value -> {
            filters.add(" score <= :maxScore ");
            propertiesMap.put("maxScore", value);
        });

        filters.add(" is_deleted = false ");

        joinFilters();

        sortBy.ifPresent(value -> {
            query.append(addSortCriteria(sortBy, sortOrder));
        });

        return query.toString();
    }

    private boolean isValidUserRoleParameter(String userRole) {
        List<String> result = Arrays.stream(PERMITTED_USER_ROLES)
                .filter(e -> !e.equals(userRole))
                .collect(Collectors.toList());

        return !result.isEmpty();
    }

    private boolean isValidUserRankParameter(String userRank) {
        List<String> result = Arrays.stream(PERMITTED_USER_RANKS)
                .filter(e -> e.equals(userRank))
                .collect(Collectors.toList());

        return !result.isEmpty();
    }

    public HashMap<String, Object> getProperties() {
        return propertiesMap;
    }

    private void joinFilters() {
        if (!filters.isEmpty()) {
            query.append(" where ").append(String.join(" and ", filters));
        }
    }

    private void buildLikeSearch(List<String> query, HashMap<String, Object> properties,
                                 String column, Optional<String> parameter) {
        parameter.ifPresent(value -> {
            query.add(String.format(" %s like :%s ", column, column));
            properties.put(column, "%" + value + "%");
        });
    }

    private void buildExactSearch(List<String> query, HashMap<String, Object> properties,
                                  String column, Optional<String> parameter) {
        parameter.ifPresent(value -> {
            query.add(String.format(" %s = :%s ", column, column));
            properties.put(column, value);
        });
    }

    private String addSortCriteria(Optional<String> sortBy, Optional<String> sortOrder) {
        if (sortBy.isEmpty() || sortBy.get().equals("")) {
            return " order by u.id ";
        }

        StringBuilder addToQuery = new StringBuilder(" order by u.");

        if (!checkIfValidSortCriteria(sortBy)) {
            throw new InvalidParameterException(String.format("The sort criteria chosen is not a valid one. Please " +
                    "enter one of the following criteria: %s.", String.join(", ", PERMITTED_SORT_CRITERIA)));
        }

        addToQuery.append(sortBy.get());

        if (sortOrder.isEmpty() || sortOrder.get().equals("asc")) {
            return addToQuery.toString();
        } else if (sortOrder.get().equalsIgnoreCase("desc")) {
            addToQuery.append(" desc");
        } else {
            throw new InvalidParameterException("Invalid sort order has been selected. You can only select 'asc' or 'desc' " +
                    "sort order. If nothing is selected results will be returned in ascending order.");
        }

        return addToQuery.toString();
    }

    private boolean checkIfValidSortCriteria(Optional<String> sortBy) {
        List<String> result = Arrays.stream(PERMITTED_SORT_CRITERIA)
                .filter(e -> e.equals(sortBy.get()))
                .collect(Collectors.toList());

        return !result.isEmpty();
    }

}
