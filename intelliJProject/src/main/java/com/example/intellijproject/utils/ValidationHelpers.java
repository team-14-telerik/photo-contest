package com.example.intellijproject.utils;

import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.User;
import com.example.intellijproject.models.contracts.Userable;
import com.example.intellijproject.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.security.InvalidParameterException;

@Component
public class ValidationHelpers {
    public static final String ORGANIZER_ROLE = "organizer";
    public static final String ADMIN = "admin";
    public static final String PHOTO_JUNKIE = "photo_junkie";
    public static final String UNAUTHORIZED_POST_UPDATE = "";
    public static final String UNAUTHORIZED_ADMIN_ACTION = "Only admin can make this changes!";
    public static final String UNAUTHORIZED_ORGANIZER_ACTION = "Only an organizer can make this changes!";
    public static final String USER_DELETED_ERR_MSG = "User with username %s is deleted. " +
            "This operation cannot be applied to a deleted user.";
    public static final String USER_BLOCKED_ERR_MSG = "User with username %s is blocked. " +
            "This operation cannot be performed on a blocked user!";
    private static final String DELETE_USER_ERROR_MESSAGE = "User is deleted!";
    public static final String ADMIN_OR_OWNER_VERIFICATION_ERR = "Only admin or account owner can delete user account";
    public static final String OWNER_ACCOUNT_CHANGE_ERR = "You are not authorized to make changes to this user." +
            "Only the owner of the account can make changes.";
    public static final String JURRY_NON_PARTICIPATION_ERR = "You cannot participate if you are jury!";
    public static final String NON_JURRY_DELETE_ERR = "Sorry! Only a juror can delete a contest photo.";
    public static final String POSITIVE_ID_VERFICATION_ERR = "Please provide a positive integer for ID";
    public static final String NON_ORGANIZER_MODIFICATION_ERR = "Only an organizer can modify this entity!";

    private final UserRepository userRepository;

    @Autowired
    public ValidationHelpers(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static void validateUserIsOrganizerOrAdmin(User user) {
        if (!user.getRole().getName().equalsIgnoreCase(ORGANIZER_ROLE) &&
                !user.getRole().getName().equalsIgnoreCase(ADMIN)) {
            throw new UnauthorizedOperationException(NON_ORGANIZER_MODIFICATION_ERR);
        }
    }

    public static <T extends Userable> void validateUserIsOrganizerOrOwner(User user, T model) {
        if (!user.getRole().getName().equalsIgnoreCase(ORGANIZER_ROLE)) {
            if (model.getUser().getId() != user.getId()) {
                throw new UnauthorizedOperationException(UNAUTHORIZED_POST_UPDATE);
            }
        }
    }

    public static void validateIsAdmin(User user) {
        if (!user.getRole().getName().equalsIgnoreCase(ADMIN)) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ADMIN_ACTION);
        }
    }

    public static void validateUserIsDeleted(User user) {
        if (user.isDeleted()) {
            throw new UnauthorizedOperationException(DELETE_USER_ERROR_MESSAGE);
        }
    }

    public static void validateUserIsBlocked(User user) {
        if (user.isBlocked()) {
            throw new UnauthorizedOperationException(String.format(USER_BLOCKED_ERR_MSG, user.getUsername()));
        }
    }

    public static void validateIfOwnerOrAdmin(User requester, int id) {
        if (requester.getId() != id && !requester.getRole().getName().equalsIgnoreCase(ADMIN)) {
            throw new UnauthorizedOperationException(ADMIN_OR_OWNER_VERIFICATION_ERR);
        }
    }

    public static void validateIfOwner(User requester, User user) {
        if (!requester.getUsername().equals(user.getUsername())) {
            throw new UnauthorizedOperationException(OWNER_ACCOUNT_CHANGE_ERR);
        }
    }

    public static void validateUserIsOrganizer(User user) {
        if (!user.getRole().getName().equalsIgnoreCase(ORGANIZER_ROLE)) {
            throw new UnauthorizedOperationException(UNAUTHORIZED_ORGANIZER_ACTION);
        }
    }

    public static void validateUserIsJury(Contest contest, User user) {
        if (contest.getJuries().contains(user)) {
            throw new UnauthorizedOperationException(JURRY_NON_PARTICIPATION_ERR);
        }
    }

    public static void validateUserCannotDelete(Contest contest, User user) {
        if (!contest.getJuries().contains(user)) {
            throw new UnauthorizedOperationException(NON_JURRY_DELETE_ERR);
        }
    }

    public static void validateID(int id) {
        if (id <= 0) {
            throw new InvalidParameterException(POSITIVE_ID_VERFICATION_ERR);
        }
    }
}
