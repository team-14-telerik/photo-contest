const inpFile = document.getElementById("example-user-photo")
const previewContainer = document.getElementById("imagePreview")
const previewImage = previewContainer.querySelector(".image-preview__image")
const previewDefaultText = previewContainer.querySelector(".image-preview__default-text")

inpFile.addEventListener("change", function () {
    const file = this.files[0]

    if (file) {
        const reader = new FileReader()

        previewDefaultText.style.display = "none"
        previewImage.style.display = "block"

        reader.addEventListener("load", function () {
            previewImage.setAttribute("src", this.result)
        })

        reader.readAsDataURL(file)

    } else {
        previewDefaultText.style.display = null
        previewImage.style.display = null
        previewImage.setAttribute("src", "")
    }

})

//Live search of extra jury
let jury = []
function showJuryResult(str) {

    if (str.length == 0) {
        hideJuryResults("juryLiveSearch")
        return
    }

    $.ajax(
        {
            type: "GET",
            url: `http://localhost:8080/api/users?username=${str}&userRole=photo_junkie`,
            dataType: "json",
            contentType: "application/json charset=utf-8",
            success: function (results) {
                showJuryResults(results)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Something went wrong, please try again later")
            }
        })

    function showJuryResults(results) {
        console.log(results.length)
        if (results.length > 0) {
            const usersContainer = document.createElement("div")
            for (let i = 0; i < results.length; i++) {
                const user = results[i]
                const username = user.username
                const rank = user.rank.name

                if(!jury.includes(username)) {
                    const photoUrl = user.userPhoto ? user.userPhoto.photoUrl : "https://res.cloudinary.com/photocontestapi/image/upload/v1650127833/user_1_tnkuk5.png"
                    const row = document.createElement("div")
                    row.innerHTML = `<div onclick="addJury(event)" class="liveSearch_row">
                                    <div class="liveSearch_user_details">
                                        <img class="liveSearch_avatar" src="${photoUrl}" alt="">
                                        <span class="username">
                                            ${username} 
                                         </span>
                                     </div>
                                    <span class="liveSearch_rank">${rank}</span>

                            </div>`
                    usersContainer.append(row)
                }
            }
            document.getElementById("juryLiveSearch").innerHTML = ""
            document.getElementById("juryLiveSearch").append(usersContainer)
            document.getElementById("juryLiveSearch").style.border = "2px solid #C6C6C6"
            document.getElementById("juryLiveSearch").style.padding = "10px"
        } else {
            hideJuryResults("juryLiveSearch")
        }
    }
}

function hideJuryResults(id) {
    document.getElementById(id).innerHTML = ""
    document.getElementById(id).style.border = "0px"
    document.getElementById(id).style.padding = "0px"
}

function addJury(e) {
    const el = e.currentTarget
    const username = el.querySelector(".username").textContent.trim()
    const juryHolderInput = document.querySelector("#allJuryHolder_input")
    const jurySearchInput = document.querySelector("#jurySearch_input")

    if(!jury.includes(username)) {
        jury.push(username);

        juryHolderInput.value += username + ";"

        const juryContainer = document.querySelector("#jury_container")
        const juryPerson = document.createElement("div")
        juryPerson.setAttribute("id", username)
        const closeButtonId = "removeJury_btn_" + username
        juryPerson.innerHTML = `
        <span>${username}</span>
        <span class="removeJury_btn" data-jury="${username}" id="${closeButtonId}">X</span>
    `
        juryPerson.classList.add("juryPreview_container")
        juryContainer.append(juryPerson)

        const removeJuryBtn = document.getElementById("removeJury_btn_" + username)
        removeJuryBtn.addEventListener("click", function (e) {
            const clickedBtn = e.target
            const clickedBtnValue = clickedBtn.dataset.jury
            juryHolderInput.value = juryHolderInput.value.replace(clickedBtnValue + ";", "")
            const tagToRemove = document.getElementById(clickedBtnValue)
            jury = jury.filter(tag => tag !== clickedBtnValue)
            tagToRemove.remove()
        })

        jurySearchInput.value = ""
        hideJuryResults("juryLiveSearch")
    }
}

const contestStatusCheckbox = document.querySelector("#contestStatus_checkbox")
contestStatusCheckbox.addEventListener("change", function (e) {
    if(e.target.value == "1") {
        document.querySelector("#invitations").classList.add("hide")
    } else {
        document.querySelector("#invitations").classList.remove("hide")
    }
})

function showUserResult(str) {

    if (str.length == 0) {
        hideInvitedUsersResults("usersLiveSearch")
        return
    }

    $.ajax(
        {
            type: "GET",
            url: `http://localhost:8080/api/users?username=${str}&userRole=photo_junkie`,
            dataType: "json",
            contentType: "application/json charset=utf-8",
            success: function (results) {
                showJuryResults(results)
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                alert("Something went wrong, please try again later")
            }
        })

    function showJuryResults(results) {
        if (results.length > 0) {
            const usersContainer = document.createElement("div")
            for (let i = 0; i < results.length; i++) {
                const user = results[i]
                const username = user.username
                const rank = user.rank.name

                if(!jury.includes(username)) {
                    const photoUrl = user.userPhoto ? user.userPhoto.photoUrl : "https://res.cloudinary.com/photocontestapi/image/upload/v1650127833/user_1_tnkuk5.png"
                    const row = document.createElement("div")
                    row.innerHTML = `<div onclick="inviteUser(event)" class="liveSearch_row">
                                    <div class="liveSearch_user_details">
                                        <img class="liveSearch_avatar" src="${photoUrl}" alt="">
                                        <span class="username">
                                            ${username} 
                                         </span>
                                     </div>
                                    <span class="liveSearch_rank">${rank}</span>

                            </div>`
                    usersContainer.append(row)
                }
            }
            document.getElementById("usersLiveSearch").innerHTML = ""
            document.getElementById("usersLiveSearch").append(usersContainer)
            document.getElementById("usersLiveSearch").style.border = "2px solid #C6C6C6"
            document.getElementById("usersLiveSearch").style.padding = "10px"
        } else {
            hideInvitedUsersResults("usersLiveSearch")
        }
    }
}

function hideInvitedUsersResults(id) {
    document.getElementById(id).innerHTML = ""
    document.getElementById(id).style.border = "0px"
    document.getElementById(id).style.padding = "0px"
}

function inviteUser(e) {
    const el = e.currentTarget
    const username = el.querySelector(".username").textContent.trim()
    const juryHolderInput = document.querySelector("#allInvitedUsersHolder_input")
    const jurySearchInput = document.querySelector("#userSearch_input")

    if(!jury.includes(username)) {
        jury.push(username);

        juryHolderInput.value += username + ";"

        const juryContainer = document.querySelector("#invitedUsers_container")
        const juryPerson = document.createElement("div")
        juryPerson.setAttribute("id", username)
        const closeButtonId = "removeJury_btn_" + username
        juryPerson.innerHTML = `
        <span>${username}</span>
        <span class="removeJury_btn" data-jury="${username}" id="${closeButtonId}">X</span>
    `
        juryPerson.classList.add("juryPreview_container")
        juryContainer.append(juryPerson)

        const removeJuryBtn = document.getElementById("removeJury_btn_" + username)
        removeJuryBtn.addEventListener("click", function (e) {
            const clickedBtn = e.target
            const clickedBtnValue = clickedBtn.dataset.jury
            juryHolderInput.value = juryHolderInput.value.replace(clickedBtnValue + ";", "")
            const tagToRemove = document.getElementById(clickedBtnValue)
            jury = jury.filter(tag => tag !== clickedBtnValue)
            tagToRemove.remove()
        })

        jurySearchInput.value = ""
        hideJuryResults("usersLiveSearch")
    }
}