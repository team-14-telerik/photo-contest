// close form
const formCloseBtn = document.querySelector("#createPost_close_btn")

formCloseBtn.addEventListener("click", function () {
    const popupWrapper = document.querySelector(".popup_wrapper")
    popupWrapper.classList.add("hide")
})

// show form
const showFormBtn = document.querySelector("#review_button")
if (showFormBtn) {
    showFormBtn.addEventListener("click", function (e) {
        const popupWrapper = document.querySelector(".popup_wrapper")
        popupWrapper.classList.remove("hide")
    })
}

// background
const creationFormPopupBackground = document.querySelector("#creationForm_background")
creationFormPopupBackground.addEventListener("click", function () {
    const popupWrapper = document.querySelector(".popup_wrapper")
    popupWrapper.classList.add("hide")
})

const formSubmitButton = document.querySelector(".reviewForm_submit")
formSubmitButton.addEventListener("click", function (e) {
    const form = document.querySelector("#giveReview_form")
    const score = form.score.value
    const comment = form.comment.value
    console.log(score)
    console.log(comment)
    const commentErrorLabel = document.querySelector("#commentError_label")
    const scoreErrorLabel = document.querySelector("#scoreError_label")
    scoreErrorLabel.classList.add("hide")
    commentErrorLabel.classList.add("hide")


    if(!score) {
        e.preventDefault()
        scoreErrorLabel.classList.remove("hide")
    }

    if(comment.length < 32) {
        e.preventDefault()
        commentErrorLabel.classList.remove("hide")
    }

})

let firstStar;
const invalidCategoryCheckbox = document.querySelector("#invalidCategory_checkbox")
invalidCategoryCheckbox.addEventListener("change", function (e) {
    const scoringContainer = document.querySelector(".scoring_row")
    scoringContainer.classList.toggle("hide")

    if(e.target.checked) {
        const stars = document.querySelector(".rate").querySelectorAll("input")

        for (let i = 0; i < stars.length-1; i++) {
            const star = stars[i]
            star.checked = false
        }

        firstStar = stars[0]
        firstStar.checked = true
        firstStar.value = 0;
    } else {
        firstStar.checked = false
        firstStar.value = 10;
    }
})

//counter
$('#review_textarea').keyup(function() {
    var characterCount = $(this).val().length,
        current = $('#current'),
        maximum = $('#maximum'),
        theCount = $('#the-count');

    current.text(characterCount);

    if (characterCount < 70) {
        current.css('color', '#666');
    }
    if (characterCount > 70 && characterCount < 90) {
        current.css('color', '#6d5555');
    }
    if (characterCount > 90 && characterCount < 100) {
        current.css('color', '#793535');
    }
    if (characterCount > 100 && characterCount < 120) {
        current.css('color', '#841c1c');
    }
    if (characterCount > 120 && characterCount < 139) {
        current.css('color', '#8f0001');
    }

    if (characterCount >= 140) {
        maximum.css('color', '#8f0001');
        current.css('color', '#8f0001');
        theCount.css('font-weight','bold');
    } else {
        maximum.css('color','#666');
        theCount.css('font-weight','normal');
    }
});
