document.querySelector("#allParticipants_btn").addEventListener("click", function (e) {
    const button = e.target
    const targetedContainer = document.querySelector("#all-participants")
    const containers = document.querySelectorAll(".contestPhotos_container")
    const buttons = document.querySelectorAll(".contestPageMenu_button")

    for (let i = 0; i < containers.length; i++) {
        const container = containers[i]
        const button = buttons[i]

        container.classList.add("hide")
        button.classList.remove("contestPageMenu_button--active")
    }

    targetedContainer.classList.remove("hide")
    button.classList.add("contestPageMenu_button--active")
})

const winnersBtn = document.querySelector("#winners_btn")

if (winnersBtn) {
    winnersBtn.addEventListener("click", function (e) {
        const targetedContainer = document.querySelector("#winners")
        const button = e.target
        const containers = document.querySelectorAll(".contestPhotos_container")
        const buttons = document.querySelectorAll(".contestPageMenu_button")

        for (let i = 0; i < containers.length; i++) {
            const container = containers[i]
            const button = buttons[i]

            container.classList.add("hide")
            button.classList.remove("contestPageMenu_button--active")
        }

        targetedContainer.classList.remove("hide")
        button.classList.add("contestPageMenu_button--active")
    })
}

document.querySelector("#details_btn").addEventListener("click", function (e) {
    const targetedContainer = document.querySelector("#details")
    const button = e.target
    const containers = document.querySelectorAll(".contestPhotos_container")
    const buttons = document.querySelectorAll(".contestPageMenu_button")

    for (let i = 0; i < containers.length; i++) {
        const container = containers[i]
        const button = buttons[i]

        container.classList.add("hide")
        button.classList.remove("contestPageMenu_button--active")
    }

    targetedContainer.classList.remove("hide")
    button.classList.add("contestPageMenu_button--active")
})
