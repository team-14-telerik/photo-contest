function hideNotification(event) {
    const notificationContainer = event.target.parentElement
    notificationContainer.classList.add("hide")
    const notificationId = event.target.dataset.id
    updateNotification(notificationId)
}

function updateNotification(notificationId) {
    $.ajax(
        {
            type: "PUT",
            url: 'http://localhost:8080/api/notifications/' + notificationId,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (obj) {
                console.log("done")
            },
            error: function (xhr, errorThrown) {
                console.log("error...", xhr.status)
            }
        })
}