const token = location.search.split('activationToken=')[1]

if(token) {
    $.ajax(
        {
            type: "POST",
            url: 'http://localhost:8080/api/users/activation?activationToken=' + token,
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (obj) {
                //e.preventDefault()

            },
            error: function (xhr, errorThrown) {
                console.log("error...", xhr.status)
                if(xhr.status == 404) {
                    alert("There was an error creating an account for you, please try again late...")
                } else if(xhr.status == 200) {
                    alert("You can login now!")
                    const loginForm = document.querySelector(".popup_wrapper")
                    loginForm.classList.remove("hide")
                }
            }
        })
}
