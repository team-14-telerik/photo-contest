// close form
const formCloseBtn = document.querySelector("#createPost_close_btn")

formCloseBtn.addEventListener("click", function () {
    const popupWrapper = document.querySelector(".popup_wrapper")
    popupWrapper.classList.add("hide")
})

// show form
const showFormBtn = document.querySelector("#show_form")
if (showFormBtn) {
    showFormBtn.addEventListener("click", function () {
        const popupWrapper = document.querySelector(".popup_wrapper")
        popupWrapper.classList.remove("hide")
    })
}

// background
const creationFormPopupBackground = document.querySelector("#creationForm_background")
creationFormPopupBackground.addEventListener("click", function () {
    const popupWrapper = document.querySelector(".popup_wrapper")
    popupWrapper.classList.add("hide")
})

// validate login user details
const createPostButton = document.querySelector("#createPost_btn")
createPostButton.addEventListener("click", function (e) {
    e.preventDefault()
    const form = document.querySelector("#createPost_form")
    const username = form.username.value
    const password = form.password.value
    const usernameErrorLabel = document.querySelector("#usernameError_label")
    const passwordErrorLabel = document.querySelector("#passwordError_label")
    const wrongCredentialsError = document.querySelector("#wrongCredentials_error")
    usernameErrorLabel.classList.add("hide")
    passwordErrorLabel.classList.add("hide")

    if (username == null || username.length > 60 || username.length < 2) {
        e.preventDefault()
        usernameErrorLabel.classList.remove("hide")
        return
    }
    if (password == null || password.length > 32 || password.length < 8) {
        e.preventDefault()
        passwordErrorLabel.classList.remove("hide")
        return
    }
    obj = {username: username, password: password}
    $.ajax(
        {
            type: "POST",
            url: 'http://localhost:8080/api/users/login',
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(obj),
            success: function (obj) {
                //e.preventDefault()
                form.submit()
            },
            error: function (request, status, error) {
                const message = JSON.parse(request.responseText).message
                wrongCredentialsError.innerHTML = message
                wrongCredentialsError.classList.remove("hide")
            }
        })
})


