//Timer
const timers = document.querySelectorAll(".timer_container")
if(timers.length > 0) {
    addTimer(timers)
}
const now = new Date().getTime();

function hideTimer(timer) {
    for (let i = 0; i < timers.length; i++) {
        const timer = timers[0]
        timer.remove()
    }
}
function addTimer(timers) {
    for (let i = 0; i < timers.length; i++) {
        const timer = timers[i]
        const countDownDate =  new Date(timer.dataset.time).getTime()

        const x = setInterval(function() {
            // Get today's date and time
            const now = new Date().getTime();

            // Find the distance between now and the count down date
            const distance = countDownDate - now;

            // Time calculations for days, hours, minutes and seconds
            const days = Math.floor(distance / (1000 * 60 * 60 * 24));
            const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((distance % (1000 * 60)) / 1000);
            // Display the result in the element with id="demo"
            timer.innerHTML = days + "d " + hours + "h "
                + minutes + "m " + seconds + "s ";

            // If the count down is finished, write some text
            if (distance < 0) {
                clearInterval(x);
                timer.innerHTML = "EXPIRED";
            }
        }, 1000);
    }
}