$.extend( $.fn.dataTable.defaults, {
    searching: false,
    ordering:  false
} );

$(document).ready( function () {
    $('#myTable').DataTable();
} );

if ( window.history.replaceState ) {
    window.history.replaceState( null, null, window.location.href );
}