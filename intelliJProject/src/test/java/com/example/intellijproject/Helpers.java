package com.example.intellijproject;

import com.example.intellijproject.models.*;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;

public class Helpers {
    public static Category createMockCategory(){
        var mockCategory = new Category();
        mockCategory.setId(1);
        mockCategory.setName("MockCategory");
        return mockCategory;
    }

    public static User createMockUser() {
        return createMockUser("photo_junkie");
    }

    public static User createMockAdmin() {
        return createMockUser("admin");
    }

    public static User createMockOrganiser() {
        return createMockUser("organizer");
    }

    private static User createMockUser(String role) {
        var mockUser = new User();
        mockUser.setId(1);
        mockUser.setEmail("mock@user.com");
        mockUser.setUsername("MockUsername");
        mockUser.setLastName("MockLastName");
        mockUser.setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setRegistrationDate(LocalDateTime.now());
        mockUser.setBlocked(false);
        mockUser.setDeleted(false);
        mockUser.setRole(createMockRole(role));
        mockUser.setRank(createMockRank());
        return mockUser;
    }

    public static User createMockUser2() {
        var mockUser = new User();
        mockUser.setId(2);
        mockUser.setEmail("Email@Mock");
        mockUser.setUsername("UsernameMock");
        mockUser.setLastName("LastNameMock");
        mockUser.setPassword("PasswordMock");
        mockUser.setFirstName("FirstNameMock");
        mockUser.setRegistrationDate(LocalDateTime.now());
        mockUser.setBlocked(false);
        mockUser.setDeleted(false);
        mockUser.setRole(createMockRole("photo_junkie"));
        mockUser.setRank(createMockRank());
        return mockUser;
    }

    public static Rank createMockRank(){
        var mockRank = new Rank();
        mockRank.setId(1);
        mockRank.setName("junkie");
        return mockRank;
    }

    public static Role createMockRole(String role) {
        var mockRole = new Role();
        mockRole.setId(1);
        mockRole.setName(role);
        return mockRole;
    }

    public static Contest createMockOpenContest() {
        return createMockContest("open");
    }

    public static Contest createMockInvitationalContest() {
        return createMockContest("invitational");
    }

    public static Contest createMockContest(String status) {
        var mockContest = new Contest();
        mockContest.setId(1);
        mockContest.setTitle("mockTitle");
        mockContest.setCategory(createMockCategory());
        mockContest.setStatus(createMockStatus(status));
        mockContest.setDeadlinePhaseOne(Date.valueOf(LocalDate.now().plusDays(28)));
        mockContest.setDeadlinePhaseTwo(LocalDateTime.now().plusDays(29));
        mockContest.setPhotoUrl("mockPhotoUrl");
        mockContest.setFinished(false);
        mockContest.setJuries(new HashSet<>());
        mockContest.setContestPhotos(new ArrayList<>());
        return mockContest;
    }

    public static Status createMockStatus(String status) {
        var mockStatus = new Status();
        mockStatus.setId(1);
        mockStatus.setName(status);
        return mockStatus;
    }

    public static ConfirmationToken createMockToken() {
        var mockToken = new ConfirmationToken();
        mockToken.setUser(createMockUser());
        mockToken.setToken("asdasdasdasd");
        mockToken.setCreatedAt(LocalDateTime.now());
        mockToken.setExpiredAt(LocalDateTime.now().plusDays(1));
        mockToken.setId(1);
        return mockToken;
    }

    public static Notification createMockNotification() {
        var mockNotification = new Notification();
        mockNotification.setId(1);
        mockNotification.setUser(createMockUser());
        mockNotification.setContest(createMockContest("open"));
        mockNotification.setRead(false);
        mockNotification.setMessage("MockMEssage");
        mockNotification.setCreationDate(LocalDateTime.now());
        return mockNotification;
    }

    public static NotificationType createMockNotificationType() {
        var mockNotificationType = new NotificationType();
        mockNotificationType.setId(1);
        mockNotificationType.setType("jury");
        return mockNotificationType;
    }

    public static ContestPhoto createContestPhoto() {
        var contestPhoto = new ContestPhoto();
        contestPhoto.setId(1);
        contestPhoto.setContest(createMockContest("open"));
        contestPhoto.setPhotoUrl("photoUrl");
        contestPhoto.setStory("MockStory");
        contestPhoto.setTitle("MockTitle");
        contestPhoto.setReviews(new HashSet<>());
        contestPhoto.setUser(createMockUser());
        return contestPhoto;
    }

    public static Review createMockReview() {
        var mockReview = new Review();
        mockReview.setScore(10);
        mockReview.setComment("mockCommentFor Review owow great Photo");
        mockReview.setId(1);
        mockReview.setUser(createMockUser());
        mockReview.setContestPhoto(createContestPhoto());
        return mockReview;
    }

    public static UserPhoto createMockUserPhoto() {
        var mockUserPhoto = new UserPhoto();
        mockUserPhoto.setPhotoUrl("mockUrl");
        mockUserPhoto.setUser(createMockUser());
        mockUserPhoto.setId(1);
        return mockUserPhoto;
    }


}
