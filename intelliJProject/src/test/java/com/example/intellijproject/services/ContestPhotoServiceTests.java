package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.*;
import com.example.intellijproject.repositories.contracts.ContestPhotoRepository;
import com.example.intellijproject.repositories.contracts.ContestRepository;
import com.example.intellijproject.repositories.contracts.ReviewRepository;
import com.example.intellijproject.services.contracts.ContestService;
import com.example.intellijproject.utils.CloudinaryHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.Optional;

import static com.example.intellijproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestPhotoServiceTests {

    @Mock
    ContestPhotoRepository mockRepository;

    @Mock
    ContestService contestService;

    @Mock
    ContestRepository contestRepository;

    @Mock
    ReviewRepository reviewRepository;

    @Mock
    CloudinaryHelper cloudinaryHelper;

    @InjectMocks
    ContestPhotoServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_contestPhoto_when_contestPhotoExists() {
        ContestPhoto contestPhoto = createContestPhoto();
        Mockito.when(mockRepository.getById(contestPhoto.getId()))
                .thenReturn(contestPhoto);

        ContestPhoto result = service.getById(contestPhoto.getId());


        Assertions.assertEquals(contestPhoto.getId(), result.getId());
    }

    @Test
    public void getAllByContestId_should_return_notification_when_notificationExists() {
        ContestPhoto contestPhoto = createContestPhoto();

        service.getAllByContestId(contestPhoto.getId());


        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByContestId(contestPhoto.getId());
    }

    @Test
    public void getAllByUserId_should_return_notification_when_notificationExists() {
        ContestPhoto contestPhoto = createContestPhoto();


        service.getAllByUserId(contestPhoto.getId());


        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByUserId(contestPhoto.getId());
    }

    @Test
    public void create_Should_Throw_When_UserIsDeleted() {
        ContestPhoto contestPhoto = createContestPhoto();
        contestPhoto.getUser().setDeleted(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contestPhoto,contestPhoto.getUser()));
    }

    @Test
    public void create_Should_Throw_When_UserIsBlocked() {
        ContestPhoto contestPhoto = createContestPhoto();
        contestPhoto.getUser().setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contestPhoto,contestPhoto.getUser()));
    }

    @Test
    public void create_Should_Throw_When_UserIsJury() {
        User user = createMockUser();
        ContestPhoto contestPhoto = createContestPhoto();
        contestPhoto.getContest().getJuries().add(user);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contestPhoto,contestPhoto.getUser()));
    }

    @Test
    public void create_Should_Throw_When_UserIsParticipant() {
        User user = createMockUser();
        ContestPhoto contestPhoto = createContestPhoto();

        Mockito.when(mockRepository.isUserParticipant(contestPhoto.getId(), user.getId()))
                        .thenReturn(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contestPhoto,contestPhoto.getUser()));
    }

    @Test
    public void create_Should_callRepository() {
        ContestPhoto contestPhoto = createContestPhoto();

        mockRepository.create(contestPhoto);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(contestPhoto);
    }

    @Test
    public void getFiltered_should_callRepository() {
        service.getFiltered(Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getFiltered(Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void getAllFiltered_should_callRepository() {
        service.getAllFiltered(Optional.empty());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllFiltered(Optional.empty());
    }

    @Test
    public void getAllByContestIdSorted_should_callRepository() {
        service.getAllByContestIdSorted(Mockito.anyInt());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByContestIdSorted(Mockito.anyInt());
    }

    @Test
    public void getEightMostPopular_should_callRepository() {
        service.getEightMostPopular();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getEightMostPopular();
    }

    @Test
    public void isNotReviewed_should_returnTrue_when_UserReviewed() {
        ContestPhoto contestPhoto = createContestPhoto();
        User user = createMockUser();
        contestPhoto.setUser(user);

        Assertions.assertFalse(service.isNotReviewed(contestPhoto, user));
    }

    @Test
    public void create_should_callRepository() {
        User user = createMockUser();
        ContestPhoto mockPhoto = createContestPhoto();
        mockPhoto.setUser(user);


        service.create(mockPhoto, user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockPhoto);
    }

    @Test
    public void delete_should_callRepository() {
        User user = createMockUser();
        Contest mockContest = createMockContest("open");
        ContestPhoto mockPhoto = createContestPhoto();
        mockPhoto.setContest(mockContest);
        mockPhoto.setUser(user);
        mockContest.getJuries().add(user);

        Mockito.when(contestService.getById(mockContest.getId()))
                .thenReturn(mockContest);

        Mockito.when(mockRepository.getById(mockPhoto.getId()))
                        .thenReturn(mockPhoto);

        service.delete(mockContest.getId(), mockPhoto.getId(), user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockPhoto.getId());
    }

    @Test
    public void deleteContestPhoto_should_deleteReviews() {
        User user = createMockUser();
        Review mockReview = createMockReview();
        Contest mockContest = createMockContest("open");
        ContestPhoto mockPhoto = createContestPhoto();
        mockPhoto.setContest(mockContest);
        mockPhoto.setUser(user);
        mockContest.getJuries().add(user);

        mockReview.setContestPhoto(mockPhoto);
        mockPhoto.getReviews().add(mockReview);

        Mockito.when(contestService.getById(mockContest.getId()))
                .thenReturn(mockContest);

        Mockito.when(mockRepository.getById(mockPhoto.getId()))
                .thenReturn(mockPhoto);

        service.delete(mockContest.getId(), mockPhoto.getId(), user);

        Mockito.verify(reviewRepository, Mockito.times(1))
                .delete(mockReview.getId());
    }

//    @Test
//    public void savePhoto_Should_Throw_When_UserIsParticipant() {
//        User user = createMockUser();
//        ContestPhoto contestPhoto = createContestPhoto();
//        contestPhoto.getContest().getJuries().add(user);
//        MockMultipartFile file
//                = new MockMultipartFile(
//                "file",
//                "hello.txt",
//                MediaType.TEXT_PLAIN_VALUE,
//                "Hello, World!".getBytes()
//        );
//
//        Mockito.when(contestRepository.getById(Mockito.anyInt())).thenReturn(contestPhoto.getContest());
//        Mockito.when(mockRepository.isUserParticipant(contestPhoto.getId(), user.getId()))
//                .thenReturn(true);
//
//
//        Assertions.assertThrows(UnauthorizedOperationException.class,
//                () -> service.savePhoto(file,contestPhoto.getContest().getId(), user));
//    }

}
