package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.exceptions.WrongDateException;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ContestRepository;
import com.example.intellijproject.services.contracts.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.sql.Date;
import java.time.LocalDate;
import java.time.LocalDateTime;

import static com.example.intellijproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ContestServiceTests {

    @Mock
    ContestRepository mockRepository;

    @Mock
    UserService mockUserService;

    @InjectMocks
    ContestServiceImpl service;

    @Test
    public void getTenNewest_Should_callRepository() {
        service.getTenNewest();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getTenNewest();
    }

    @Test
    public void getAllJuried_Should_callRepository() {
        service.getAllJuriedByUserId(Mockito.anyInt());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllJuriedByUserId(Mockito.anyInt());
    }

    @Test
    public void getById_should_return_contestPhoto_when_contestPhotoExists() {
        Contest contest = createMockContest("open");
        Mockito.when(mockRepository.getById(contest.getId()))
                .thenReturn(contest);

        Contest result = service.getById(contest.getId());


        Assertions.assertEquals(contest.getId(), result.getId());
    }

    @Test
    public void getAllPhaseOne_Should_callRepository() {
        service.getAllPhaseOne();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllPhaseOne();
    }

    @Test
    public void getAllPhaseTwo_Should_callRepository() {
        service.getAllPhaseTwo();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllPhaseTwo();
    }

    @Test
    public void getAllOpen_Should_callRepository() {
        service.getAllOpen();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllOpen();
    }

    @Test
    public void getAllByUserId_Should_callRepository() {
        service.getAllByUserId(Mockito.anyInt());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByUserId(Mockito.anyInt());
    }

    @Test
    public void getAllFinished_Should_callRepository() {
        service.getAllFinished();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllFinished();
    }

    @Test
    public void getAllFinishedByUserId_Should_callRepository() {
       service.getAllFinishedByUserId(Mockito.anyInt());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllFinishedByUserId(Mockito.anyInt());
    }

    @Test
    public void createContest_Should_Throw_When_UserIsDeleted() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contest, mockOrganizer));
    }

    @Test
    public void createContest_Should_Throw_When_UserIsBlocked() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contest, mockOrganizer));
    }

    @Test
    public void createContest_Should_Throw_When_UserIsNotOrganizer() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockUser();



        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(contest, mockOrganizer));
    }

    @Test
    public void createContest_should_throw_when_TitleExists() {
        Contest contest = createMockContest("open");
        User user = createMockOrganiser();

        Mockito.when(mockRepository.getByField("title", contest.getTitle()))
                .thenReturn(contest);


        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(contest, user));
    }

    @Test
    public void createContest_should_throw_when_PhaseOneIsBeforeToday() {
        Contest contest = createMockContest("open");
        contest.setDeadlinePhaseOne(Date.valueOf(LocalDate.now().minusDays(10)));
        User user = createMockOrganiser();

        Mockito.when(mockRepository.getByField("title", contest.getTitle()))
                .thenThrow(EntityNotFoundException.class);


        Assertions.assertThrows(WrongDateException.class, () -> service.create(contest, user));
    }

    @Test
    public void createContest_should_throw_when_PhaseTwoIsAfter1Month() {
        Contest contest = createMockContest("open");
        contest.setDeadlinePhaseTwo(LocalDateTime.now().plusDays(50));
        User user = createMockOrganiser();

        Mockito.when(mockRepository.getByField("title", contest.getTitle()))
                .thenThrow(EntityNotFoundException.class);


        Assertions.assertThrows(WrongDateException.class, () -> service.create(contest, user));
    }

    @Test
    public void createContest_should_call_Repository() {
        Contest contest = createMockContest("open");
        User user = createMockOrganiser();

        Mockito.when(mockRepository.getByField("title", contest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        service.create(contest, user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(contest);
    }

    @Test
    public void updateContest_Should_Throw_When_UserIsDeleted() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(contest, mockOrganizer));
    }

    @Test
    public void updateContest_Should_Throw_When_UserIsBlocked() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(contest, mockOrganizer));
    }

    @Test
    public void updateContest_Should_Throw_When_UserIsNotOrganizer() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockUser();


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(contest, mockOrganizer));
    }

    @Test
    public void updateContest_should_throw_when_TitleExists() {
        Contest contest = createMockContest("open");
        User user = createMockOrganiser();

        Mockito.when(mockRepository.getByField("title", contest.getTitle()))
                .thenReturn(contest);


        Assertions.assertThrows(DuplicateEntityException.class, () -> service.update(contest, user));
    }

    @Test
    public void updateContest_should_call_Repository() {
        Contest contest = createMockContest("open");
        User user = createMockOrganiser();

        Mockito.when(mockRepository.getByField("title", contest.getTitle()))
                .thenThrow(EntityNotFoundException.class);

        service.update(contest, user);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(contest);
    }

    @Test
    public void deleteContest_Should_Throw_When_UserIsDeleted() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(contest.getId(), mockOrganizer));
    }

    @Test
    public void deleteContest_Should_Throw_When_UserIsBlocked() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(contest.getId(), mockOrganizer));
    }

    @Test
    public void deleteContest_Should_Throw_When_UserIsNotOrganizer() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockUser();


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(contest.getId(), mockOrganizer));
    }

    @Test
    public void deleteContest_Should_callRepository() {
        Contest contest = createMockContest("open");
        User mockOrganizer = createMockOrganiser();

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(contest);
        service.delete(contest.getId(), mockOrganizer);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(contest.getId());
    }

}
