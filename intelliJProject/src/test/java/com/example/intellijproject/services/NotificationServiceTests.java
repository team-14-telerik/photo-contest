package com.example.intellijproject.services;

import com.example.intellijproject.models.Notification;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.NotificaitonRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.intellijproject.Helpers.createMockNotification;
import static com.example.intellijproject.Helpers.createMockUser;

@ExtendWith(MockitoExtension.class)
public class NotificationServiceTests {
    @Mock
    NotificaitonRepository mockRepository;

    @InjectMocks
    NotificationServiceImpl service;


    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_notification_when_notificationExists() {
       Notification mockNotification = createMockNotification();
        Mockito.when(mockRepository.getById(mockNotification.getId()))
                .thenReturn(mockNotification);

        Notification result = service.getById(mockNotification.getId());


       Assertions.assertEquals(mockNotification.getId(), result.getId());
    }

    @Test
    public void createNotification_should_callRepository() {
        Notification mockNotification = createMockNotification();

        service.create(mockNotification);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create( mockNotification);
    }

    @Test
    public void updateNotification_should_callRepository() {
        Notification mockNotification = createMockNotification();

        service.update(mockNotification);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockNotification);
    }

    @Test
    public void createBulk_should_callRepository() {
        List<Notification> notifications = new ArrayList<>();
        Notification mockNotification = createMockNotification();
        notifications.add(mockNotification);

        service.createBulk(notifications);

        for (Notification notification: notifications) {
            Mockito.verify(mockRepository, Mockito.times(1))
                    .create(notification);
        }
    }

    @Test
    public void deleteNotification_should_callRepository() {
        Notification mockNotification = createMockNotification();

        service.delete(mockNotification.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockNotification.getId());
    }

    @Test
    public void getAllActiveUserNotifications_should_callRepository() {
        User mockUser = createMockUser();

        service.getAllActiveUserNotifications(mockUser.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllActiveUserNotifications(mockUser.getId());
    }


}
