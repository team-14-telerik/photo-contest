package com.example.intellijproject.services;


import com.example.intellijproject.models.NotificationType;
import com.example.intellijproject.repositories.contracts.NotificationTypeRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.intellijproject.Helpers.createMockNotificationType;

@ExtendWith(MockitoExtension.class)
public class NotificationTypeServiceTests {

    @Mock
    NotificationTypeRepository mockRepository;

    @InjectMocks
    NotificationTypeServiceImpl service;


    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_notification_when_notificationExists() {
        NotificationType mockNotificationType = createMockNotificationType();
        Mockito.when(mockRepository.getById(mockNotificationType.getId()))
                .thenReturn(mockNotificationType);

        NotificationType result = service.getById(mockNotificationType.getId());


        Assertions.assertEquals(mockNotificationType.getId(), result.getId());
    }

    @Test
    public void createNotification_should_callRepository() {
        NotificationType mockNotificationType = createMockNotificationType();

        service.create(mockNotificationType);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockNotificationType);
    }

    @Test
    public void updateNotification_should_callRepository() {
        NotificationType mockNotificationType = createMockNotificationType();

        service.update(mockNotificationType);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockNotificationType);
    }

    @Test
    public void deleteNotification_should_callRepository() {
        NotificationType mockNotificationType = createMockNotificationType();

        service.delete(mockNotificationType.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockNotificationType.getId());
    }
}
