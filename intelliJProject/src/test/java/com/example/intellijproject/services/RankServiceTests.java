package com.example.intellijproject.services;

import com.example.intellijproject.models.Rank;
import com.example.intellijproject.repositories.contracts.RankRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static com.example.intellijproject.Helpers.createMockRank;

@ExtendWith(MockitoExtension.class)
public class RankServiceTests {

    @Mock
    RankRepository mockRepository;

    @InjectMocks
    RankServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_role_when_roleExists() {
        Rank mockRank = createMockRank();

        Mockito.when(mockRepository.getById(mockRank.getId()))
                .thenReturn(mockRank);

        Rank result = service.getById(mockRank.getId());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRank.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRank.getName(), result.getName())
        );
    }

    @Test
    public void getByRoleName_should_return_role_when_roleExists() {
        Rank mockRank = createMockRank();

        Mockito.when(mockRepository.getByField("name", mockRank.getName()))
                .thenReturn(mockRank);

        Rank result = service.getByRankName(mockRank.getName());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRank.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRank.getName(), result.getName())
        );
    }

    @Test
    public void getRankByPoints_should_return_Rank() {
        int mockPoints = 30;
        Rank rank = createMockRank();
        List<Rank> list = new ArrayList<>();
        list.add(rank);

        Mockito.when(mockRepository.getAll()).thenReturn(list);
        Rank result = service.getRankByPoints(30);

        Assertions.assertEquals(result.getName(),"junkie");
    }

}
