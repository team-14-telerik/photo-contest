package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.Contest;
import com.example.intellijproject.models.Review;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.ReviewRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.intellijproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class ReviewServiceTests {


    @Mock
    ReviewRepository mockRepository;

    @InjectMocks
    ReviewServiceImp service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void getAllByContestId_should_callRepository() {

        Mockito.when(mockRepository.getAllByContestId(1))
                .thenReturn(new ArrayList<>());

        service.getAllByContestId(1);

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAllByContestId(1);
    }

    @Test
    public void getById_should_return_role_when_roleExists() {
       Review mockReview = createMockReview();

        Mockito.when(mockRepository.getById(mockReview.getId()))
                .thenReturn(mockReview);

        Review result = service.getById(mockReview.getId());

        Assertions.assertEquals(mockReview.getId(), result.getId());
    }

    @Test
    public void createReview_Should_Throw_When_UserIsDeleted() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockReview, mockOrganizer));
    }

    @Test
    public void createReview_Should_Throw_When_UserIsBlocked() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.create(mockReview, mockOrganizer));
    }

    @Test
    public void createReview_throw_whenDuplicateExists() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        Contest contest = createMockContest("open");

        Mockito.when(mockRepository.getByContestPhotoIdAndUserId(mockOrganizer.getId(), contest.getId() ))
                .thenReturn(mockReview);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.create(mockReview, mockOrganizer));
    }


    @Test
    public void createReview_Should_callRepo() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        Contest contest = createMockContest("open");

        Mockito.when(mockRepository.getByContestPhotoIdAndUserId(mockOrganizer.getId(), contest.getId() )).thenThrow(EntityNotFoundException.class);
        service.create(mockReview, mockOrganizer);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockReview);
    }


    @Test
    public void updateReview_Should_Throw_When_UserIsDeleted() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockReview, mockOrganizer));
    }

    @Test
    public void updateReview_Should_Throw_When_UserIsBlocked() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockReview, mockOrganizer));
    }

    @Test
    public void updateReview_Should_Throw_When_UserIsNotOrganizer() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockUser();
        mockOrganizer.setId(333);



        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(mockReview, mockOrganizer));
    }

    @Test
    public void updateReview_Should_callRepo() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        Contest contest = createMockContest("open");

        service.update(mockReview, mockOrganizer);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockReview);
    }

    @Test
    public void deleteReview_Should_Throw_When_UserIsDeleted() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockReview.getId(), mockOrganizer));
    }

    @Test
    public void deleteReview_Should_Throw_When_UserIsBlocked() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();
        mockOrganizer.setBlocked(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockReview.getId(), mockOrganizer));
    }

    @Test
    public void deleteReview_Should_Throw_When_UserIsNotOrganizer() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockUser();
        mockOrganizer.setId(333);

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockReview);



        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(mockReview.getId(), mockOrganizer));
    }

    @Test
    public void deleteReview_Should_callRepo() {
        Review mockReview = createMockReview();
        User mockOrganizer = createMockOrganiser();

        Mockito.when(mockRepository.getById(Mockito.anyInt())).thenReturn(mockReview);
        service.delete(mockReview.getId(), mockOrganizer);

        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockReview.getId());
    }




}
