package com.example.intellijproject.services;

import com.example.intellijproject.models.Role;
import com.example.intellijproject.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.intellijproject.Helpers.createMockRole;

@ExtendWith(MockitoExtension.class)
public class RoleServiceTests {

    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_role_when_roleExists() {
        Role mockRole = createMockRole("user");

        Mockito.when(mockRepository.getById(mockRole.getId()))
                .thenReturn(mockRole);

        Role result = service.getById(mockRole.getId());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getName(), result.getName())
        );
    }

    @Test
    public void getByRoleName_should_return_role_when_roleExists() {
        Role mockRole = createMockRole("user");

        Mockito.when(mockRepository.getByField("name", mockRole.getName()))
                .thenReturn(mockRole);

        Role result = service.getByRoleName(mockRole.getName());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockRole.getId(), result.getId()),
                () -> Assertions.assertEquals(mockRole.getName(), result.getName())
        );
    }
}
