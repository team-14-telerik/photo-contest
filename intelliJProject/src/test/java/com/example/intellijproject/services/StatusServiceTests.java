package com.example.intellijproject.services;

import com.example.intellijproject.models.Status;
import com.example.intellijproject.repositories.contracts.StatusRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;

import static com.example.intellijproject.Helpers.createMockStatus;

@ExtendWith(MockitoExtension.class)
public class StatusServiceTests {

    @Mock
    StatusRepository mockRepository;

    @InjectMocks
    StatusServiceImpl service;

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll())
                .thenReturn(new ArrayList<>());

        service.getAll();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    public void getById_should_return_status_when_statusExists() {
        Status mockStatus = createMockStatus("open");

        Mockito.when(mockRepository.getById( mockStatus.getId()))
                .thenReturn(mockStatus);

        Status result = service.getById(mockStatus.getId());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockStatus.getId(), result.getId()),
                () -> Assertions.assertEquals(mockStatus.getName(), result.getName())
        );
    }
}
