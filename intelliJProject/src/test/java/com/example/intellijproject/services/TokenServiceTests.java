package com.example.intellijproject.services;

import com.example.intellijproject.models.ConfirmationToken;
import com.example.intellijproject.repositories.contracts.TokenRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static com.example.intellijproject.Helpers.createMockToken;

@ExtendWith(MockitoExtension.class)
public class TokenServiceTests {

    @Mock
    TokenRepository mockRepository;

    @InjectMocks
    TokenServiceImpl service;

    @Test
    public void findByToken_should_return_token_when_tokenExists() {
        ConfirmationToken mockToken = createMockToken();

        Mockito.when(mockRepository.getByField("token", mockToken.getToken()))
                .thenReturn(mockToken);


        ConfirmationToken result = service.findByToken(mockToken.getToken());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockToken.getId(), result.getId()),
                () -> Assertions.assertEquals(mockToken.getToken(), result.getToken())
        );

    }

    @Test
    public void saveToken_should_callRepository() {
        ConfirmationToken mockToken = createMockToken();

        service.saveConfirmationToken(mockToken);

        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockToken);
    }

    @Test
    public void updateToken_should_callRepository() {
        ConfirmationToken mockToken = createMockToken();

        service.updateConfirmationToken(mockToken);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockToken);
    }

}
