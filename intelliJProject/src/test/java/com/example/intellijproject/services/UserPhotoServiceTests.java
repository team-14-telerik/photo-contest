package com.example.intellijproject.services;

import com.example.intellijproject.models.User;
import com.example.intellijproject.models.UserPhoto;
import com.example.intellijproject.repositories.contracts.UserPhotoRepository;
import com.example.intellijproject.services.contracts.UserService;
import com.example.intellijproject.utils.CloudinaryHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;

import static com.example.intellijproject.Helpers.createMockUser;
import static com.example.intellijproject.Helpers.createMockUserPhoto;

@ExtendWith(MockitoExtension.class)
public class UserPhotoServiceTests {

    @InjectMocks
    UserPhotoServiceImpl service;

    @Mock
    UserPhotoRepository mockRepository;

    @Mock
    CloudinaryHelper cloudinaryHelper;

    @Mock
    UserService userService;

    @Test
    public void save_should_return_User_withPhoto() throws IOException {
      User user = createMockUser();
      UserPhoto userPhoto = createMockUserPhoto();
      String url = "mockUrl";
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );

        Mockito.when(userService.getById(Mockito.anyInt()))
                .thenReturn(user);

      service.save(file, user.getId());
      //todo to be refactored
    }

    @Test
    public void update_should_return_User_withPhoto() throws IOException {
        User user = createMockUser();
        String url = "mockUrl";
        MockMultipartFile file
                = new MockMultipartFile(
                "file",
                "hello.txt",
                MediaType.TEXT_PLAIN_VALUE,
                "Hello, World!".getBytes()
        );
        Mockito.when(userService.getById(Mockito.anyInt()))
                .thenReturn(user);

        service.update(file, user.getId());
        //todo to be refactored
    }
}
