package com.example.intellijproject.services;

import com.example.intellijproject.exceptions.DuplicateEntityException;
import com.example.intellijproject.exceptions.EntityNotFoundException;
import com.example.intellijproject.exceptions.UnauthorizedOperationException;
import com.example.intellijproject.models.Role;
import com.example.intellijproject.models.User;
import com.example.intellijproject.repositories.contracts.UserRepository;
import com.example.intellijproject.services.contracts.TokenService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Optional;

import static com.example.intellijproject.Helpers.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTests {

    @Mock
    UserRepository mockRepository;

    @Mock
    TokenService tokenService;

    @InjectMocks
    UserServiceImpl service;

    @Mock
    PasswordEncoder passwordEncoder;


    @Test
    public void getById_should_return_User_when_userExists() {
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getById(mockUser.getId()))
                .thenReturn(mockUser);

        User result = service.getById(mockUser.getId());

        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername())
        );
    }

    @Test
    void getAll_should_callRepository() {
        Mockito.when(mockRepository.getAll(Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty()))
                .thenReturn(new ArrayList<>());

        service.getAll(Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                Optional.empty(), Optional.empty(), Optional.empty());

        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll(Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty(), Optional.empty(),
                        Optional.empty(), Optional.empty(), Optional.empty());
    }

    @Test
    public void getByUsername_should_return_User_when_userExists() {
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(mockUser);

        User result = service.getByUsername(mockUser.getUsername());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword())
        );
    }

    @Test
    public void getByEmail_should_return_User_when_userExists() {
        User mockUser = createMockUser();
        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        User result = service.getByEmail(mockUser.getEmail());


        Assertions.assertAll(
                () -> Assertions.assertEquals(mockUser.getId(), result.getId()),
                () -> Assertions.assertEquals(mockUser.getEmail(), result.getEmail()),
                () -> Assertions.assertEquals(mockUser.getUsername(), result.getUsername()),
                () -> Assertions.assertEquals(mockUser.getRole(), result.getRole()),
                () -> Assertions.assertEquals(mockUser.getPassword(), result.getPassword())
        );
    }

    @Test
    public void getAllAdmins_should_callRepository() {
        Mockito.when(mockRepository.getByRole("admin"))
                .thenReturn(new ArrayList<>());

        service.getAllAdmins();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByRole("admin");
    }

    @Test
    public void getAllOrganizers_should_callRepository() {
        Mockito.when(mockRepository.getByRole("organizer"))
                .thenReturn(new ArrayList<>());

        service.getAllOrganizers();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByRole("organizer");
    }

    @Test
    public void getAllJunkies_should_callRepository() {
        Mockito.when(mockRepository.getByRole("photo_junkie"))
                .thenReturn(new ArrayList<>());

        service.getAllJunkies();

        Mockito.verify(mockRepository, Mockito.times(1))
                .getByRole("photo_junkie");
    }

    @Test
    public void makeAdmin_should_throw_when_UserToPromoteIdIsNotValid() {
        User currentUser = createMockAdmin();
        int userToPromoteId = -1;

        Assertions.assertThrows(InvalidParameterException.class,
                () -> service.makeAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void makeAdmin_should_throw_when_currentUserIsNotAdmin() {
        User currentUser = createMockUser();
        int userToPromoteId = 1;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.makeAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void makeAdmin_should_throw_when_userToBePromotedIsDeleted() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        userToBePromoted.setDeleted(true);
        int userToPromoteId = 1;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.makeAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void makeAdmin_should_throw_when_userToBePromotedIsBlocked() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        userToBePromoted.setBlocked(true);
        int userToPromoteId = 1;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.makeAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void makeAdmin_should_throw_when_userToBePromotedIsAdminAlready() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockAdmin();
        int userToPromoteId = 7;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.makeAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void makeAdmin_should_make_when_UserToExecuteIsAdmin() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        int userToPromoteId = 1;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        service.makeAdmin(currentUser,userToPromoteId);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(userToBePromoted );
    }

    @Test
    public void demoteAdmin_should_throw_when_UserToPromoteIdIsNotValid() {
        User currentUser = createMockAdmin();
        int userToPromoteId = -1;

        Assertions.assertThrows(InvalidParameterException.class,
                () -> service.demoteAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void demoteAdmin_should_throw_when_currentUserIsNotAdmin() {
        User currentUser = createMockUser();
        int userToPromoteId = 1;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.demoteAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void demoteAdmin_should_throw_when_userToBePromotedIsBlocked() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        userToBePromoted.setBlocked(true);
        int userToPromoteId = 1;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.demoteAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void demoteAdmin_should_make_when_UserToExecuteIsAdmin() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockAdmin();
        int userToPromoteId = 1;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        service.demoteAdmin(currentUser,userToPromoteId);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(userToBePromoted );
    }

    @Test
    public void demoteAdmin_should_throw_when_userToBePromotedIsUserAlready() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        int userToPromoteId = 1;

        Mockito.when(mockRepository.getById(userToPromoteId))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.demoteAdmin(currentUser, userToPromoteId));
    }

    @Test
    public void updateBlocked_should_throw_when_executingUserIsNotAdmin() {
        User executingUser = createMockUser();
        User mockUserToBeUpdated = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateUserBlockStatus(executingUser, mockUserToBeUpdated.getId()));
    }

    @Test
    public void updateBlocked_should_return_block_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        service.updateUserBlockStatus(executingUser, mockUserToBeUpdated.getId());

        Assertions.assertTrue(mockUserToBeUpdated.isBlocked());
    }

    @Test
    public void create_should_throw_when_userWithSameUsernameExists() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByField("username", mockUser.getUsername()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_throw_when_userWithSameEmailExists() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByField("username", mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenReturn(mockUser);

        Assertions.assertThrows(DuplicateEntityException.class, () -> service.create(mockUser));
    }

    @Test
    public void create_should_callRepository_when_userWithSameEmailDoesNotExist() {
        User mockUser = createMockUser();

        Mockito.when(mockRepository.getByField("username", mockUser.getUsername()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.getByField("email", mockUser.getEmail()))
                .thenThrow(EntityNotFoundException.class);

        Mockito.when(mockRepository.create(mockUser)).thenReturn(mockUser);

        Mockito.when(mockRepository.getById(mockUser.getId())).thenReturn(mockUser);

        service.create(mockUser);



        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockUser);
    }

    @Test
    public void update_should_throwException_when_userIsNotOwner() {
        User mockInitiator = createMockUser();
        User mockUserObject = createMockUser2();


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(2, mockInitiator, mockUserObject));
    }

    @Test
    public void update_should_throwException_when_userIsDeleted() {
        User mockInitiator = createMockUser();
        User mockUserObject = createMockUser2();
        mockUserObject.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.update(2, mockInitiator, mockUserObject));
    }

    @Test
    public void update_should_throwException_when_userUsernameAlreadyExists() {
        User mockUserObject = createMockUser();
        User mockInitiator = createMockAdmin();
        mockUserObject.setId(2);

        Mockito.when(mockRepository.getByField("username", mockInitiator.getUsername()))
                .thenReturn(mockUserObject);


        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockInitiator.getId(), mockInitiator, mockInitiator));
    }

    @Test
    public void update_should_throwException_when_userEmailAlreadyExists() {
        User mockUserObject = createMockUser();
        mockUserObject.setId(10);
        User mockInitiator = createMockAdmin();


        Mockito.when(mockRepository.getByField("username", mockInitiator.getUsername()))
                .thenReturn(mockInitiator);

        Mockito.when(mockRepository.getByField("email", mockInitiator.getEmail()))
                .thenReturn(mockUserObject);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.update(mockInitiator.getId(), mockInitiator, mockUserObject));
    }

    @Test
    public void update_should_return_updateUser_when_userIsOwner() {
        User mockInitiator = createMockAdmin();

        Mockito.when(mockRepository.getByField("username", mockInitiator.getUsername()))
                .thenReturn(mockInitiator);

        Mockito.when(mockRepository.getByField("email", mockInitiator.getEmail()))
                .thenReturn(mockInitiator);

        service.update(mockInitiator.getId(), mockInitiator, mockInitiator);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockInitiator);

    }

    @Test
    public void delete_should_throw_when_userIsDeleted() {
        User currentUser = createMockAdmin();
        currentUser.setDeleted(true);
        int userToPromoteId = 1;


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(currentUser, userToPromoteId));
    }

    @Test
    public void delete_should_throw_when_userIsBlocked() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        currentUser.setBlocked(true);
        int userToPromoteId = 1;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(currentUser, userToPromoteId));
    }

    @Test
    public void delete_should_throw_when_IdIsInvalid() {
        User currentUser = createMockAdmin();
        int userToPromoteId = -1;

        Assertions.assertThrows(InvalidParameterException.class,
                () -> service.delete(currentUser, userToPromoteId));
    }

    @Test
    public void delete_should_throw_when_RequesterIsNotOwner() {
        User currentUser = createMockUser();
        int userToPromoteId = 2;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(currentUser, userToPromoteId));
    }

    @Test
    public void delete_should_throw_when_RequesterIsNotAdmin() {
        User currentUser = createMockUser();
        int userToPromoteId = 2;

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.delete(currentUser, userToPromoteId));
    }

    @Test
    public void delete_should_delete_when_RequesterIsAdmin() {
        User currentUser = createMockAdmin();
        User target = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(target);

        service.delete(currentUser, target.getId());

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(target);
    }

    @Test
    public void updateRole_should_return_update_when_executingUserIsAdmin() {
        User executingUser = createMockAdmin();
        User mockUserToBeUpdated = createMockUser();
        Role mockRole = createMockRole("admin");

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        service.updateRole(executingUser, mockUserToBeUpdated.getId(), mockRole);

        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockUserToBeUpdated);
    }

    @Test
    public void updateRole_should_throw_when_executingUserIsNotAdmin() {
        User executingUser = createMockUser2();
        User mockUserToBeUpdated = createMockUser();
        Role mockRole = createMockRole("admin");

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockUserToBeUpdated);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.updateRole(executingUser, mockUserToBeUpdated.getId(), mockRole));
    }

    @Test
    public void makeOrganizer_should_throw_when_IdIsInvalid() {
        User currentUser = createMockAdmin();
        int userToPromoteId = -1;

        Assertions.assertThrows(InvalidParameterException.class,
                () -> service.makeOrganizer(currentUser, userToPromoteId));
    }

    @Test
    public void makeOrganizer_should_throw_when_UserIsNotAdmin() {
        User currentUser = createMockOrganiser();
        User userToBePromoted = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.makeOrganizer(currentUser, userToBePromoted.getId()));
    }

    @Test
    public void makeOrganizer_should_throw_when_userIsBlocked() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        userToBePromoted.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.makeOrganizer(currentUser, userToBePromoted.getId()));
    }

    @Test
    public void makeOrganizer_should_throw_when_userIsDeleted() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        userToBePromoted.setDeleted(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.makeOrganizer(currentUser, userToBePromoted.getId()));
    }

    @Test
    public void makeOrganizer_should_throw_when_userIsAlreadyOrganizer() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockOrganiser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBePromoted);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.makeOrganizer(currentUser, userToBePromoted.getId()));
    }

    @Test
    public void makeOrganizer_should_update() {
        User currentUser = createMockAdmin();
        User userToBePromoted = createMockUser();
        int roleId = Role.ORGANIZER_ROLE_ID;

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBePromoted);

        service.makeOrganizer(currentUser, userToBePromoted.getId());

        Assertions.assertEquals(roleId, userToBePromoted.getRole().getId());
    }


    @Test
    public void demoteOrganizer_should_throw_when_IdIsInvalid() {
        User currentUser = createMockAdmin();
        int userToPromoteId = -1;

        Assertions.assertThrows(InvalidParameterException.class,
                () -> service.demoteOrganizer(currentUser, userToPromoteId));
    }

    @Test
    public void demoteOrganizer_should_throw_when_UserIsNotAdmin() {
        User currentUser = createMockOrganiser();
        User userToBeDemoted = createMockUser();

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.demoteOrganizer(currentUser, userToBeDemoted.getId()));
    }

    @Test
    public void demoteOrganizer_should_throw_when_userIsBlocked() {
        User currentUser = createMockAdmin();
        User userToBeDemoted = createMockUser();
        userToBeDemoted.setBlocked(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBeDemoted);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.demoteOrganizer(currentUser, userToBeDemoted.getId()));
    }

    @Test
    public void demoteOrganizer_should_throw_when_userIsDeleted() {
        User currentUser = createMockAdmin();
        User userToBeDemoted = createMockUser();
        userToBeDemoted.setDeleted(true);

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBeDemoted);

        Assertions.assertThrows(EntityNotFoundException.class,
                () -> service.demoteOrganizer(currentUser, userToBeDemoted.getId()));
    }

    @Test
    public void demoteOrganizer_should_throw_when_userIsAlreadyUser() {
        User currentUser = createMockAdmin();
        User userToBeDemoted = createMockUser();

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBeDemoted);

        Assertions.assertThrows(DuplicateEntityException.class,
                () -> service.demoteOrganizer(currentUser, userToBeDemoted.getId()));
    }

    @Test
    public void demoteOrganizer_should_update() {
        User currentUser = createMockAdmin();
        User userToBeDemoted = createMockOrganiser();
        int roleId = Role.PHOTO_JUNKIE_ROLE_ID;

        Mockito.when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(userToBeDemoted);

        service.demoteOrganizer(currentUser, userToBeDemoted.getId());

        Assertions.assertEquals(roleId, userToBeDemoted.getRole().getId());
    }

    @Test
    public void changeUserPassword_should_throw_when_userIsBlocked() {
        User currentUser = createMockAdmin();
        currentUser.setBlocked(true);

        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.changeUserPassword(currentUser.getPassword(), currentUser.getPassword(), currentUser));
    }

    @Test
    public void changeUserPassword_should_throw_when_userIsDeleted() {
        User currentUser = createMockAdmin();
        currentUser.setDeleted(true);


        Assertions.assertThrows(UnauthorizedOperationException.class,
                () -> service.changeUserPassword(currentUser.getPassword(), currentUser.getPassword(), currentUser));
    }






}



